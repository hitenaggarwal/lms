public with sharing class LMS_Constants {
    
    public static final String CONTAINER_ACTIVE = 'active';
    public static final String CONTAINER_UPDATED = 'updated';
    public static final String CONTAINER_COMPLETED = 'completed';
   public static final String CONTAINER_OVERDUE= 'overdue';
}