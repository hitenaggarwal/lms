public without sharing class LMS_TrainingPlanEnrollment {
   @AuraEnabled
   public static void enrollLeaners (Id trainingPlanId,List<Id> contactIdList){
       
       try {
           List<Training_Plan_Enrol__c> tpeList = new List<Training_Plan_Enrol__c>();
           for(Id conId  :contactIdList ){
            Training_Plan_Enrol__c tpe  = new Training_Plan_Enrol__c();
            tpe.Training_Plan__c = trainingPlanId;
            tpe.Learner__c = conId;
            tpe.Enrolment_Date__c = Date.Today();
            tpe.Status__c = 'Not Started';
            tpeList.add(tpe);
           }
           insert tpeList;
        

        Set<Id> trainingCourses = new Set<Id>();

        for(Training_Plan_Course_Association__c tpa : [SELECT Id,Course_Catalogue__c,Schedule_Exists__c
                                                    FROM Training_Plan_Course_Association__c
                                                    WHERE Training_Plan__c=:trainingPlanId ]){
                                                        if(tpa.Schedule_Exists__c){
                                                            trainingCourses.add(tpa.Course_Catalogue__c);
                                                        }
                                                    }
          List<User_Enrollment__c> userEnrollmentList = new List<User_Enrollment__c>();                                          
       for(Course_Catalogue__c cc :[SELECT Id , 
                                        (SELECT Id ,Total_Confirmed__c,Available_Seats__c
                                        FROM Course_Schedules__r 
                                        WHERE End_Date__c >= Today 
                                        OR End_date__c = null LIMIT 1)
                                    FROM Course_Catalogue__c
                                    WHERE Id IN :trainingCourses ]){
                                        for(Course_Schedule__c cs : cc.Course_Schedules__r ){
                                            for(Id conId : contactIdList){
                                                User_Enrollment__c userEnroll = new User_Enrollment__c();
                                                userEnroll.Contact__c = conId;
                                                userEnroll.Course_Schedule__c =cs.Id;
                                                userEnroll.Status__c = 'Not Started';
                                                userEnroll.Enrolment_Status__c = 'Confirmed';
                                                if(cs.Available_Seats__c == cs.Total_Confirmed__c &&  cs.Available_Seats__c != null){
                                                    userEnroll.Enrolment_Status__c = 'Waitlist';
                                                }
                                                userEnrollmentList.add(userEnroll);
                                            }
                                        }
                                            
                                    }
        insert userEnrollmentList;
        List<User_Module_Association__c> lstUserModuleAssociations = new List<User_Module_Association__c>();
        for(Course_Catalogue__c cc : [SELECT Id , 
                                        (SELECT Id,Module_Name__c,Type__c 
                                        FROM Course_Modules__r)
                                        FROM Course_Catalogue__c
                                        WHERE Id IN :trainingCourses]){
                                            for(User_Enrollment__c userEnroll : userEnrollmentList){
                                                for(Course_Module__c module : cc.Course_Modules__r){
                                                    User_Module_Association__c uma = new User_Module_Association__c();
                                                    uma.User_Enrollment__c = userEnroll.Id;
                                                    uma.Course_Module__c = module.Id;
                                                    uma.Status__c = 'Not Started';
                                                    uma.Name = module.Module_Name__c;
                                                    lstUserModuleAssociations.add(uma);
                                                }
                                            }
                                        }
       insert lstUserModuleAssociations;
       } catch (Exception e) {
           throw new AuraHandledException(e.getMessage());
       }
   }

   @AuraEnabled
   public static void enrollLeaners (Id trainingPlanId,List<Training_Plan_Enrol__c> enrollmentList){
       
       try {
           
           for(Training_Plan_Enrol__c enrollment : enrollmentList){
                enrollment.Training_Plan__c = trainingPlanId;
                enrollment.Status__c = 'Not Started';
                enrollment.Enrolment_Date__c = Date.Today();
           }
           Insert enrollmentList;
           Set<Id> trainingCourses = new Set<Id>();

           for(Training_Plan_Course_Association__c tpa : [SELECT Id,Course_Catalogue__c,Schedule_Exists__c
                                                        FROM Training_Plan_Course_Association__c
                                                        WHERE Training_Plan__c=:trainingPlanId ]){
                                                            if(tpa.Schedule_Exists__c){
                                                                trainingCourses.add(tpa.Course_Catalogue__c);
                                                            }
                                                        }
          List<User_Enrollment__c> userEnrollmentList = new List<User_Enrollment__c>();                                          
       for(Course_Catalogue__c cc :[SELECT Id , 
                                        (SELECT Id ,Total_Confirmed__c,Available_Seats__c
                                        FROM Course_Schedules__r 
                                        WHERE End_Date__c >= Today 
                                        OR End_date__c = null LIMIT 1)
                                    FROM Course_Catalogue__c
                                    WHERE Id IN :trainingCourses ]){
                                        for(Course_Schedule__c cs : cc.Course_Schedules__r ){
                                            for(Training_Plan_Enrol__c enrollment : enrollmentList){
                                                User_Enrollment__c userEnroll = new User_Enrollment__c();
                                                userEnroll.Contact__c = enrollment.Learner__c;
                                                userEnroll.Course_Schedule__c =cs.Id;
                                                userEnroll.Status__c = 'Not Started';
                                                userEnroll.Enrolment_Status__c = 'Confirmed';
                                                if(cs.Available_Seats__c == cs.Total_Confirmed__c &&  cs.Available_Seats__c != null){
                                                    userEnroll.Enrolment_Status__c = 'Waitlist';
                                                }
                                                userEnrollmentList.add(userEnroll);
                                            }
                                        }
                                            
                                    }
        insert userEnrollmentList;
        List<User_Module_Association__c> lstUserModuleAssociations = new List<User_Module_Association__c>();
        for(Course_Catalogue__c cc : [SELECT Id , 
                                        (SELECT Id,Module_Name__c,Type__c 
                                        FROM Course_Modules__r)
                                        FROM Course_Catalogue__c
                                        WHERE Id IN :trainingCourses]){
                                            for(User_Enrollment__c userEnroll : userEnrollmentList){
                                                for(Course_Module__c module : cc.Course_Modules__r){
                                                    User_Module_Association__c uma = new User_Module_Association__c();
                                                    uma.User_Enrollment__c = userEnroll.Id;
                                                    uma.Course_Module__c = module.Id;
                                                    uma.Status__c = 'Not Started';
                                                    uma.Name = module.Module_Name__c;
                                                    lstUserModuleAssociations.add(uma);
                                                }
                                            }
                                        }
       insert lstUserModuleAssociations;
       } catch (Exception e) {
           throw new AuraHandledException(e.getMessage());
       }
   }

   @AuraEnabled
   public static void updateEnrollers (List<Training_Plan_Enrol__c> enrollmentList){
        if(!enrollmentList.isEmpty()){
            Update enrollmentList;
        }
   }
}