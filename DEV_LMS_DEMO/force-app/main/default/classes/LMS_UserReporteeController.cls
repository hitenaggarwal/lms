public without sharing class LMS_UserReporteeController {
    @AuraEnabled
    public static List<Contact> getMyReportees(){
        Id userId = UserInfo.getUserId();
        Id contactId = [SELECT Id, contactId 
                        FROM User 
                        WHERE Id = :userId].contactId;
        
        List<Contact> reporteesList = new List<Contact>();
        for(Contact con : [SELECT Id, Name, Supervisor__c 
                           FROM Contact
                           WHERE Supervisor__c = :contactId]) {
                            reporteesList.add(con);
                           }
        return reporteesList;
    }
}