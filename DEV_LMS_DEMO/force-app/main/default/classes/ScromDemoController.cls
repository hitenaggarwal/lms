public without sharing class ScromDemoController {
    
      
    @AuraEnabled
    public static void insertSuspendData(Id recordId,String suspendData){
        User_Module_Association__c ScromMedia = new User_Module_Association__c();
        ScromMedia.Id=recordId;
        ScromMedia.Suspended_Data__c=suspendData;
        
        update ScromMedia;

    }

    @AuraEnabled
    public static String getSuspendData(Id recordId){
        List<User_Module_Association__c> listOfScromMedia=[SELECT ID,Suspended_Data__c From User_Module_Association__c
                                                WHERE Id=:recordId];
        if(listOfScromMedia.size()>0){
            return listOfScromMedia[0].Suspended_Data__c;
        }
        return '';

    }
    @AuraEnabled
    public static void setModuleComplete(Id userModuleId){
        try {
            User_Module_Association__c userModule = new User_Module_Association__c();
            userModule.Id=userModuleId;
            userModule.Status__c='Completed';
            userModule.Completed_date__c=Date.today();

            
            update userModule;
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public class CustomException extends Exception {}
}