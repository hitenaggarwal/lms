public without sharing class LMS_CoursePreRequisitesViewController {

    @AuraEnabled
    public static Map < String, Object > fetchCourseRelations(String courseId) {
        Map < String, Object > response = new Map < String, Object > ();
        List<CourseRelationWrapper> lstCourseRelationWrapper = new List<CourseRelationWrapper>();
        try{
            List<Course_Relation__c> lstCourseRelation = new List<Course_Relation__c>();
            lstCourseRelation = [SELECT Id, Name, Course_Catalogue__c, Type__c, Related_Course__c, Related_Course__r.Name FROM Course_Relation__c 
            WHERE Course_Catalogue__c = :courseId AND Related_Course__c != null AND Type__c = 'Prerequisite' ORDER BY Related_Course__r.Name];
            if(!lstCourseRelation.isEmpty() && lstCourseRelation.size() > 0){
                for(Course_Relation__c cr : lstCourseRelation){
                    lstCourseRelationWrapper.add(new CourseRelationWrapper(cr));
                }
            }
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
        response.put('crWrapper', lstCourseRelationWrapper);
        return response;
    }

    public class CourseRelationWrapper{
        @AuraEnabled public Course_Relation__c courseRelation;
        @AuraEnabled public String relatedCourseName;
        @AuraEnabled public String relatedCourseUrl;
        public CourseRelationWrapper(Course_Relation__c courseRelation){
            this.courseRelation = courseRelation;
            relatedCourseName = '';
            relatedCourseUrl = '';
            if(courseRelation.Related_Course__c != null){
                relatedCourseName = courseRelation.Related_Course__r.Name;
                relatedCourseUrl = Site.getPathPrefix() + '/course-catalogue/' + courseRelation.Related_Course__c + '/' + relatedCourseName;
            }
        }
    }

    public class LMSException extends Exception{
        
    }

}