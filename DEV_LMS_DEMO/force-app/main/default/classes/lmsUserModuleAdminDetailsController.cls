public without sharing class lmsUserModuleAdminDetailsController {
    @AuraEnabled
    public static List<User_Module_Association__c>  getCourseDetails(String status){
        if(status == 'Completed'){
            return [SELECT Id,Name,Course_Module__r.Module_Name__c,User_Enrollment__r.Name,CreatedById,Status__c,Due_Date__c
                                              
                                            FROM User_Module_Association__c 
                                            WHERE Status__c = 'Completed' 
                                            order by Name];
        }
        if(status == 'Incomplete'){
            return [SELECT Id,Name,Course_Module__r.Module_Name__c,User_Enrollment__r.Name,CreatedById,Status__c,Due_Date__c
                                              
                                            FROM User_Module_Association__c 
                                            WHERE Status__c = 'In Progress'
                                            OR  Status__c = 'Not Started'
                                            order by Name];
        }
        if(status == 'Overdue'){
            return [SELECT Id,Name,Course_Module__r.Module_Name__c,User_Enrollment__r.Name,CreatedById,Status__c,Due_Date__c
                                              
                                            FROM User_Module_Association__c 
                                            WHERE Due_Date__c <: Date.Today() 
                                            order by Name];
        }

        return new List<User_Module_Association__c>();
        
        
        
                                            
    }
}