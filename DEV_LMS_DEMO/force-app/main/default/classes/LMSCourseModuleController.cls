/**
 * @description       : 
 * @author            : Manish Sogani
 * @group             : 
 * @last modified on  : 01-05-2021
 * @last modified by  : Manish Sogani
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   01-05-2021   Manish Sogani   Initial Version
**/
public with sharing class LMSCourseModuleController {
    @AuraEnabled
    public static Course_Module__c fetchCourseModule(String courseModuleId) {
        return [SELECT Name, Module_Name__c, Type__c, Status__c, Sequence__c, Audio_URL__c, Max_Attempts__c, Passing_Score__c, PDF_URL__c, Video_URL__c
                FROM Course_Module__c
                WHERE Id = :courseModuleId
                LIMIT 1];
    }
    @AuraEnabled
    public static Map < String, Object > fetchCourseModuleRecords(String courseCatalogRecordId){
        Map < String, Object > response = new Map < String, Object > ();
        List<CourseModuleWrapper> lstCourseModuleWrapper = new List<CourseModuleWrapper>();
        try{
            if(String.isNotBlank(courseCatalogRecordId) && String.isNotEmpty(courseCatalogRecordId)){

                Set<String> setCMAdditionalFields = new Set<String>();
                setCMAdditionalFields.add('Id');
                setCMAdditionalFields.add('Course_Catalogue__c');

                String soql = '';
                soql = prepareQuery('Course_Module__c', setCMAdditionalFields, true);
                soql += ' WHERE Course_Catalogue__c = \'' + courseCatalogRecordId + '\'';
                soql += ' ORDER BY Module_Name__c ';
                for(Course_Module__c cm : database.query(soql)){
                    lstCourseModuleWrapper.add(new CourseModuleWrapper(cm));
                }
            }
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
        response.put('cmWrapper', lstCourseModuleWrapper);
        return response;
    }

    @AuraEnabled
    public static String createCourseModuleRecord(Course_Module__c newRecord){
        newRecord.Status__c = 'Published';
        insert newRecord;
        return newRecord.Id;
    }

    @AuraEnabled
    public static String createCourseModuleVersionRecord(String moduleName){
        Course_Module__c courseModule = [SELECT Id, PDF_URL__c, Video_URL__c, Audio_URL__c FROM Course_Module__c WHERE Name = :moduleName LIMIT 1].get(0);
        Course_Module_Version__c courseModuleVersion = new Course_Module_Version__c();
        courseModuleVersion.Video_URL__c = courseModule.Video_URL__c;
        courseModuleVersion.Audio_URL__c = courseModule.Audio_URL__c;
        courseModuleVersion.Course_Module__c = courseModule.Id;
        courseModuleVersion.Pdf_URL__c = courseModule.PDF_URL__c;
        insert courseModuleVersion;
        return courseModuleVersion.Id;
    }

    @AuraEnabled
    public static void doDeleteRecord(String recordIdForDelete, String recordObjectForDelete){
        try {
            List<sObject> lstSObject = new List<sObject>();
            if(String.isNotBlank(recordIdForDelete) && String.isNotEmpty(recordIdForDelete) && 
            String.isNotBlank(recordObjectForDelete) && String.isNotEmpty(recordObjectForDelete)){
                String soql = 'SELECT Id ';
                soql += ' FROM ' + recordObjectForDelete;
                soql += ' WHERE Id = \'' + recordIdForDelete + '\'';
                lstSObject = database.query(soql);

                List<sObject> lstCVSObject = new List<sObject>();
                soql = 'SELECT Id FROM Course_Module_Version__c';
                soql += ' WHERE Course_Module__c = \'' + recordIdForDelete + '\'';
                lstCVSObject = database.query(soql);
                if(!lstCVSObject.isEmpty() && lstCVSObject.size() > 0){
                    delete lstCVSObject;
                }

                if(!lstSObject.isEmpty() && lstSObject.size() > 0){
                    delete lstSObject;
                }
            }
        } catch (Exception e) {
            throw new LMSException( e.getMessage() );
        }
    }

    public class CourseModuleWrapper{
        @AuraEnabled public Course_Module__c courseModule;
        @AuraEnabled public Boolean isQuiz;

        public CourseModuleWrapper(Course_Module__c courseModule){
            this.courseModule = courseModule;
            isQuiz = false;
            if(courseModule.Type__c == 'Quiz'){
                isQuiz = true;
            }
        }
    }

     //Prepare SOQL query based on Object Name
     public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Schema.sObjectType objType = globalDescription.get(objName);
        Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
        Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
}