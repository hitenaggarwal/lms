public without sharing class LMS_TrainingPlanDetailsController {
    public LMS_TrainingPlanDetailsController() {

    }
    @AuraEnabled
    public static List<Training_Plan__c> getTrainingPlan(Id trainingId) {
        List<Training_Plan__c> trainingPlanList = [SELECT Name, Role__c, Is_Active__c
                                                   FROM Training_Plan__c
                                                   WHERE Id = :trainingId
                                                   LIMIT 1];
        return trainingPlanList;                                       
    }

    @AuraEnabled
    public static void updateActiveStatus(Id trainingPlanId, Boolean activeStatus) {
        List<Training_Plan__c> trainingPlanList = [SELECT Name, Is_Active__c
                                                   FROM Training_Plan__c
                                                   WHERE Id = :trainingPlanId
                                                   LIMIT 1];
        trainingPlanList.get(0).Is_Active__c = activeStatus;
        update trainingPlanList;                                       
    }

    @AuraEnabled
    public static List<Training_Plan_Course_Association__c> getTrainingPlanCourses(Id trainingId) {
        List<Training_Plan_Course_Association__c> trainingPlanCourseList = new List<Training_Plan_Course_Association__c>();
        for(Training_Plan_Course_Association__c tpc : [SELECT Course_Catalogue__r.Id, Course_Catalogue__r.Name, Course_Catalogue__r.Duration__c, Course_Catalogue__r.Roles__c, Course_Catalogue__r.Summary__c 
                                                       FROM Training_Plan_Course_Association__c 
                                                       WHERE Training_Plan__c = :trainingId]) {
                                                            trainingPlanCourseList.add(tpc);
                                                       }
        return trainingPlanCourseList;                                         
    }

    @AuraEnabled
    public static List<Course_Catalogue__c> getRemainingCourses(Id trainingPlanId){
        Set<Id> trainingPlanCourseIds = new Set<Id>();
        List<Training_Plan_Course_Association__c> trainingPlanCourseList = getTrainingPlanCourses(trainingPlanId);
        List<Course_Catalogue__c> remainingCoursesList = new List<Course_Catalogue__c>();

        for(Training_Plan_Course_Association__c tpc : trainingPlanCourseList) {
            trainingPlanCourseIds.add(tpc.Course_Catalogue__r.Id);
        }

        for(Course_Catalogue__c course : [SELECT Id, Name, Duration__c, Roles__c
                                          FROM Course_Catalogue__c
                                          WHERE Status__c = 'Published'
                                          AND Id NOT IN :trainingPlanCourseIds]) {
                                            remainingCoursesList.add(course);
                                          }
        return remainingCoursesList;                               
    }

    @AuraEnabled
    public static void addCoursesToTrainingPlan(Id trainingPlanId, String coursesString){
        Set<Id> courseIds = (Set<Id>) JSON.deserialize(coursesString, Set<Id>.class);

        List<Training_Plan_Course_Association__c> trainingPlanCourseList = new List<Training_Plan_Course_Association__c>();
        for(Id courseId : courseIds) {
            Training_Plan_Course_Association__c tpca = new Training_Plan_Course_Association__c();
            tpca.Training_Plan__c = trainingPlanId;
            tpca.Course_Catalogue__c = courseId;
            trainingPlanCourseList.add(tpca);
        }
        
        insert trainingPlanCourseList;
    }


}