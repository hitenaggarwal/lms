public with sharing class LMS_CourseRoleApproval {
    public LMS_CourseRoleApproval() {

    }

    @AuraEnabled
        public static List<Course_Module_Role__c> getCourseRoles() {
            List<Course_Module_Role__c> courseList = new List<Course_Module_Role__c>();
            try{
                courseList = [SELECT Id,Name,Status__c,Role__c,CreatedDate,Course_Schedule__r.Course_Catalogue__r.Name FROM Course_Module_Role__c];
            } catch(exception e) {
                throw new AuraHandledException('Error  ---> '+e.getMessage());
            }
            return courseList;
        }

        @AuraEnabled
        public static String saveCourseRoleData(List<String> ids, String status) {
            try{
                List<Course_Module_Role__c> courseList = new List<Course_Module_Role__c>();
                for(String id : ids) {
                    Course_Module_Role__c cm = new Course_Module_Role__c();
                    cm.Id = id;
                    cm.Status__c = status;
                    courseList.add(cm);
                } 
                if(courseList.size()>0) {
                    update courseList;
                    return 'SUCCESS';
                }
                return '';
            } catch(exception e) {
                throw new AuraHandledException(e.getMessage());
            }
        }

}