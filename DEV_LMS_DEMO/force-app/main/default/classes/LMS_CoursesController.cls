public without sharing class LMS_CoursesController {

    private static User currentUser;

    @AuraEnabled
    public static CoursesWrapper fetchCourses(String searchObject){
        try {
            currentUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId()];

            Map<String,Object> queryParams = (Map<String,Object>)JSON.deserializeUntyped( searchObject );

            string filter = '';
    
            //Filter by Search
            if( String.isNotBlank( (String)queryParams.get('searchTerm') ) ){
                String filterValue = string.escapeSingleQuotes( (String)queryParams.get('searchTerm') ) + '%';
                filter +=  ' AND Course_Schedule__r.Course_Catalogue__r.Name LIKE \'' + filterValue + '\' ';
            }

            //Filter by Container
            if( String.isNotBlank( (String)queryParams.get('container') ) ){
                String filterValue = string.escapeSingleQuotes( (String)queryParams.get('container') );

                if(filterValue == LMS_Constants.CONTAINER_ACTIVE) {
                    filter +=  ' AND (Status__c IN (\'Not Started\', \'In progress\') ';
                    filter += ' OR Course_Schedule__r.Auto_Enroll__c = true ) ';
                }
                else if(filterValue == LMS_Constants.CONTAINER_UPDATED) {
                    filter += ' AND Course_Updated__c = true ';
                }
                else if(filterValue == LMS_Constants.CONTAINER_COMPLETED) {
                    filter += ' AND Status__c = \'Completed\' ';
                }
                else if(filterValue == LMS_Constants.CONTAINER_OVERDUE) {
                    filter +=  ' AND Status__c IN (\'Not Started\', \'In progress\') AND Due_Date__c != null AND Due_Date__c < TODAY AND Overdue__c = true ';
                }
            }
            System.debug('FILTER:'+ filter);
            String query = 'SELECT Id, Status__c,Enrolled_Date__c,Due_Date__c,Course_Schedule__c, Course_Updated__c, '+
                'Course_Schedule__r.Course_Catalogue__c, Course_Schedule__r.Course_Catalogue__r.Name, '+
                'Course_Schedule__r.Course_Catalogue__r.Description__c, Course_Schedule__r.Course_Catalogue__r.Roles__c , Course_Schedule__r.Auto_Enroll__c '+
                'FROM User_Enrollment__c '+
                'WHERE Contact__c = \'' +currentUser.ContactId + '\' ' + filter + ' ORDER BY Course_Schedule__r.Course_Catalogue__r.Name ASC ';
            
            List<User_Enrollment__c> enrollmentsList = (List<User_Enrollment__c>)Database.query( query );

            Map<String,Integer> enrollmentIdToTotalModulesMap = new Map<String,Integer>();
            Map<String,Integer> enrollmentIdToCompletedModulesMap = new Map<String,Integer>();

            for(User_Module_Association__c association : [
                SELECT Id, User_Enrollment__c, Status__c
                FROM User_Module_Association__c
                WHERE User_Enrollment__c IN :enrollmentsList
            ]) {
                Integer total = 1;
                if( enrollmentIdToTotalModulesMap.containsKey( association.User_Enrollment__c ) ) {
                    total = enrollmentIdToTotalModulesMap.get( association.User_Enrollment__c ) + 1;
                }
                enrollmentIdToTotalModulesMap.put( association.User_Enrollment__c, total );

                if( association.Status__c == 'Completed' ) {
                    Integer completed = 1;
                    if( enrollmentIdToCompletedModulesMap.containsKey( association.User_Enrollment__c ) ) {
                        completed = enrollmentIdToCompletedModulesMap.get( association.User_Enrollment__c ) + 1;
                    }
                    enrollmentIdToCompletedModulesMap.put( association.User_Enrollment__c, completed );
                }
            }

            List<CourseItemWrapper> courseItems = new List<CourseItemWrapper>();

            for(User_Enrollment__c enrollment : enrollmentsList) {
                CourseItemWrapper courseItem = new CourseItemWrapper( enrollment );

                if( enrollmentIdToTotalModulesMap.containsKey( enrollment.Id ) ) {
                    courseItem.totalModules = enrollmentIdToTotalModulesMap.get( enrollment.Id );
                }
                if( enrollmentIdToCompletedModulesMap.containsKey( enrollment.Id ) ) {
                    courseItem.completedModules = enrollmentIdToCompletedModulesMap.get( enrollment.Id );
                }
                
                courseItems.add( courseItem );
            }

            CoursesWrapper response = new CoursesWrapper( courseItems );
            return response;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class CoursesWrapper {
        @AuraEnabled public List<CourseItemWrapper> courseItems;

        public CoursesWrapper( List<CourseItemWrapper> courseItems ) {
            this.courseItems = courseItems;
        }
    }

    public class CourseItemWrapper {
        @AuraEnabled public String userEnrollmentId;
        @AuraEnabled public String roles;
        @AuraEnabled public String courseStatus;
        @AuraEnabled public Date courseDueDate;
        @AuraEnabled public Date courseEnrollmentDate;
        @AuraEnabled public String courseScheduleId;
        @AuraEnabled public String courseId;
        @AuraEnabled public String courseTitle;
        @AuraEnabled public String courseDescription;
        @AuraEnabled public String courseCatalogUrl;
        @AuraEnabled public Integer totalModules;
        @AuraEnabled public Integer completedModules;

        public CourseItemWrapper(User_Enrollment__c enrollment) {
            this.userEnrollmentId = enrollment.Id;
            this.courseStatus = enrollment.Status__c;
            this.courseEnrollmentDate = enrollment.Enrolled_Date__c;
            this.courseDueDate = enrollment.Due_Date__c;
            this.courseScheduleId = enrollment.Course_Schedule__c;
            this.courseId = enrollment.Course_Schedule__r?.Course_Catalogue__c;
            this.courseTitle = enrollment.Course_Schedule__r?.Course_Catalogue__r.Name;
            this.courseDescription = enrollment.Course_Schedule__r?.Course_Catalogue__r.Description__c;
            this.roles = enrollment.Course_Schedule__r.Course_Catalogue__r.Roles__c;
            this.courseCatalogUrl = Site.getPathPrefix() + '/course-catalogue/' + enrollment.Course_Schedule__r?.Course_Catalogue__c + '/' + enrollment.Course_Schedule__r?.Course_Catalogue__r.Name;
            // this.courseCatalogUrl = Site.getPathPrefix() + '/course-detail?id=' + enrollment.Id;
            this.totalModules = 0;
            this.completedModules = 0;
        }
    }
}