public with sharing class LMS_CourseData {
    
   

    
    @AuraEnabled
    public static LMS_Response viewcourseDetails(){
        System.debug('In Class');
        Id userId = UserInfo.getUserId();
        User u = [select id, contactId from User where id = : userId];
        Id getContactId = u.contactId;
          LMS_Response allUserCourses = new LMS_Response();
        //  List<User_Enrollment__c> pastCourses = new List<User_Enrollment__c> ();
        //  List<User_Enrollment__c> futureCourses = new List<User_Enrollment__c> ();
         System.debug('In user id');
        
            System.debug('contact Id not null');
            for(User_Enrollment__c userEnrollmentRecord :  [SELECT Id, Contact__c, Course_Schedule__r.Course_Catalogue__r.Name,Course_Schedule__r.Course_Catalogue__r.Id,Course_Schedule__r.Start_Date__c,Course_Schedule__r.End_Date__c , Status__c
                                                                FROM User_Enrollment__c
                                                                WHERE Contact__c =: getContactId
                                                                AND Course_Schedule__r.Session_Type__c = 'Instructor'
                                                                order by createdDate desc
                                                            ]){
                                                                if(userEnrollmentRecord.Course_Schedule__r.Start_Date__c >= Date.Today()){
                                                                    allUserCourses.futureCourses.add(userEnrollmentRecord);
                                                                    System.debug('In  future Leaves' + userEnrollmentRecord);

                                                                }else {
                                                                    System.debug('In  Past Leaves' + userEnrollmentRecord);
                                                                    allUserCourses.pastCourses.add(userEnrollmentRecord);
                                                                }
                                                            

                                                            }
                                                              

                                                                                                          
        
        return allUserCourses;
        
    } 
    public class LMS_Response{

        @AuraEnabled
        public List<User_Enrollment__c> pastCourses { get; set; }
        @AuraEnabled
        public List<User_Enrollment__c> futureCourses { get; set; }
        public LMS_Response(){
            this.pastCourses =new List<User_Enrollment__c>();
            this.futureCourses = new List<User_Enrollment__c>();
        }
    
}
    
}