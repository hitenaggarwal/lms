public without sharing class LMSIntroController {
   @AuraEnabled
   public static Boolean getFirstTimeUser(){
       try {
           for(User u : [SELECT Id ,ContactId , Contact.First_Time_User__c
                        FROM User
                        WHERE Id=:UserInfo.getUserId()]){
                            return u.Contact.First_Time_User__c;
                        }
                        return false;
       } catch (Exception e) {
           throw new AuraHandledException(e.getMessage());
       }
   }
   @AuraEnabled
   public static void updateFirstTimeUser(){
       try {
           Contact con = new Contact();
           for(User u : [SELECT Id ,ContactId , Contact.First_Time_User__c
                        FROM User
                        WHERE Id=:UserInfo.getUserId()]){
                            con.Id = u.ContactId;
                            con.First_Time_User__c = false;
                        }
            update con;
       } catch (Exception e) {
           throw new AuraHandledException(e.getMessage());
       }
   }
}