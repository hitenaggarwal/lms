public without sharing class LMS_CoursePreRequisites {
    public LMS_CoursePreRequisites() {

    }
    @AuraEnabled
    public static List<Course_Relation__c> fetchCourseRelations(String courseId) {
        try{
            return [SELECT Id,Name, Course_Catalogue__c, Type__c
                    FROM Course_Relation__c
                    WHERE Course_Catalogue__c = :courseId];
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
    }

    @AuraEnabled
    public static void deleteCourseRelations(String id) {
        try{
            List<Course_Relation__c> lstCourseRelation = [SELECT Id,Name FROM Course_Relation__c WHERE Id =:id];
            delete lstCourseRelation;
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
    }

    public class LMSException extends Exception{
        
    }
}