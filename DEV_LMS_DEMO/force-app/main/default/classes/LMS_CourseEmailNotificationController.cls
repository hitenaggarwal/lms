public with sharing class LMS_CourseEmailNotificationController {
    public LMS_CourseEmailNotificationController() {

    }

    @AuraEnabled
    public static Map<String, Object> doInit( String recordId, String attendeeStatusOption ){
        Map<String, Object> response = new Map<String, Object>();
        response.put('emails', new list<String>());
        response.put('showAttendeeStatusOptions', false);
        try {
            if( String.isNotBlank( recordId ) ){
                Id idValue = Id.valueOf( recordId );
                String objectAPIName = idValue.getSObjectType().getDescribe().getName();
                if( objectAPIName == 'Course_Schedule__c'){
                    response.put('showAttendeeStatusOptions', true);
                    list<String> emails = new list<String>();
                    list<String> attendeeStatus = new list<String>();
                    if( attendeeStatusOption == 'All' ){
                        attendeeStatus = new list<String>{'Confirmed', 'Waitlist'};
                    }else if( attendeeStatusOption == 'Confirmed' ){
                        attendeeStatus = new list<String>{'Confirmed'};
                    }else{
                        attendeeStatus = new list<String>{'Waitlist'};
                    }
                    for( User_Enrollment__c userEnrollment : [SELECT Id, Contact__c, Contact__r.Email FROM User_Enrollment__c 
                                                            WHERE Course_Schedule__c=:idValue AND Contact__c != null
                                                            AND Enrolment_Status__c IN:attendeeStatus]){
                        emails.add( userEnrollment.Contact__r.Email );
                    }
                    response.put( 'emails', emails );
                }
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> sendEmail( String recordId, String emailAddresses, String emailSubject, String emailBody ){
        Map<String, Object> response = new Map<String, Object>();
        try {
            list<String> emails = new list<String>();
            if( String.isBlank( emailAddresses ) ){
                throw new LMS_EmailException('Email address is required.');
            }else{
                for( String emailAddress : emailAddresses.split(',') ){
                    if( String.isNotBlank( emailAddress.trim() ) ){
                        if( !validateEmail( emailAddress.trim() ) ){
                            throw new LMS_EmailException('Invalid Email address.');
                        }else {
                            emails.add( emailAddress.trim() ); 
                        }
                    }else{
                        throw new LMS_EmailException('Email address is required.');
                    }
                }
            }

            if( String.isBlank( emailSubject ) ){
                throw new LMS_EmailException('Email Subject is required.');
            }

            if( String.isBlank( emailBody ) ){
                throw new LMS_EmailException('Email body is required.');
            }

            list<Messaging.SingleEmailMessage> messages = new list<Messaging.SingleEmailMessage>();

            for( String email : emails ){
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setToAddresses( new list<String>{ email } );
                message.setSubject( emailSubject );
                message.setHtmlBody( emailBody );
                messages.add( message );
            }

            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            /*Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
            mail.setTargetObjectIds(lstIds);
            mail.setSenderDisplayName('System Admin');
            mail.setTemplateId(et.id);
            Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });*/
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }

    private static Boolean validateEmail( String email ){
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: http://www.regular-expressions.info/email.html
        Pattern emailPattern = Pattern.compile(emailRegex);
        Matcher emailMatcher = emailPattern.matcher(email);

        if (!emailMatcher.matches()) 
            return false;
        return true;	
    }
    
    public class LMS_EmailException extends Exception{}
}