public without sharing class lmsUtility {
    @AuraEnabled
    public static void deleteRecord(Id recordId){
        
         Database.delete(recordId);
    }
}