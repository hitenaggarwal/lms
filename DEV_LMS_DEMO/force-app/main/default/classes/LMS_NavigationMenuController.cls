public without sharing class LMS_NavigationMenuController {
  
    @AuraEnabled
    public static Boolean isAdmin(){

        for(User  u : [SELECT Id,Contact.Is_Admin__c 
                        FROM  User
                        WHERE Id =:UserInfo.getUserId()]){
                            return u.Contact.Is_Admin__c;
                        }
    
        return false;
    }

   
}