public without sharing class LMSQuizQuestionsController {
    
    @AuraEnabled
    public static Map < String, Object > fetchQuizQuestionRecords(String courseModuleId){
        Map < String, Object > response = new Map < String, Object > ();
        List<QuestionWrapper> lstQuestionWrapper = new List<QuestionWrapper>();
        try{
            if(String.isNotBlank(courseModuleId) && String.isNotEmpty(courseModuleId)){
                Set<String> setAdditionalFields = new Set<String>();
                setAdditionalFields.add('Id');
                setAdditionalFields.add('Course_Module__c');
                setAdditionalFields.add('Category__c');
                setAdditionalFields.add('Question_Type__c');
                setAdditionalFields.add('Sequence__c');
                setAdditionalFields.add('Score__c');
                setAdditionalFields.add('Description__c');
                setAdditionalFields.add('Is_Active__c');

                String soql = '';
                soql = prepareQuery('Question__c', setAdditionalFields, false);
                soql += ' WHERE Course_Module__c = \'' + courseModuleId + '\'';
                soql += ' ORDER BY Sequence__c';
                for(Question__c q : database.query(soql)){
                    lstQuestionWrapper.add(new QuestionWrapper(q));
                }
            }
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
        response.put('quesWrapper', lstQuestionWrapper);
        return response;
    }

    @AuraEnabled
    public static void doDeleteRecord(String recordIdForDelete, String recordObjectForDelete){
        try {
            if(String.isNotBlank(recordIdForDelete) && String.isNotEmpty(recordIdForDelete) && 
            String.isNotBlank(recordObjectForDelete) && String.isNotEmpty(recordObjectForDelete)){
                String soql = 'SELECT Id ';
                soql += ' FROM ' + recordObjectForDelete;
                soql += ' WHERE Id = \'' + recordIdForDelete + '\'';

                List<sObject> lstSObject = new List<sObject>();
                lstSObject = database.query(soql);
                if(!lstSObject.isEmpty() && lstSObject.size() > 0){
                    delete lstSObject;
                }
            }
        } catch (Exception e) {
            throw new LMSException( e.getMessage() );
        }
    }

    public class QuestionWrapper{
        @AuraEnabled public Question__c question;

        public QuestionWrapper(Question__c question){
            this.question = question;
        }
    }

     //Prepare SOQL query based on Object Name
     public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
            Schema.sObjectType objType = globalDescription.get(objName);
            Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
            Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
            
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
}