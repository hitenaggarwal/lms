public without sharing class lmsCourseFacilityController {
    @AuraEnabled
    public static List<Course_Facility__c>  getCourseFaciltyDetails(Id recordId){
        return [SELECT Id, Facility__r.Name, Course_Catalogue__r.Name ,CreatedBy.Name
                FROM Course_Facility__c
                WHERE Course_Catalogue__c=: recordId
                order by Facility__r.Name];
                                            
    }

    @AuraEnabled
    public static List<Facility__c>  getRemainingFacility(Id recordId){
        Set<Id> facilityId = new Set<Id>();
        for (Course_Facility__c  courseFacility: [SELECT Facility__c FROM Course_Facility__c where Course_Catalogue__c =: recordId ]){
            
            facilityId.add(courseFacility.Facility__c) ;
               
        }
        List<Facility__c> facilityList = new List<Facility__c>();
        for(Facility__c facilty : [SELECT Id, Name
                                          FROM Facility__c
                                          WHERE  Id NOT IN :facilityId
                                          ORDER BY Name]) {
                                            facilityList.add(facilty);
                                          }
        

        return facilityList;
                                            
    }
    @AuraEnabled
    public static void  addCourseFacility(Id recordId,String facilityString){
        Set<Id>  facilityIds= (Set<Id>) JSON.deserialize(facilityString, Set<Id>.class);
        List<Course_Facility__c> courseFacilityList = new List<Course_Facility__c>();
        for(Id facilityId : facilityIds) {
            Course_Facility__c courseFacility = new Course_Facility__c();
            courseFacility.Facility__c = facilityId;
            courseFacility.Course_Catalogue__c = recordId;
            courseFacilityList.add(courseFacility);
        }
        
        insert courseFacilityList;
        
                                            
    }


    @AuraEnabled
    public static void doDeleteRecord(String recordIdForDelete, String recordObjectForDelete){
        try {
            List<sObject> lstSObject = new List<sObject>();
            if(String.isNotBlank(recordIdForDelete) && String.isNotEmpty(recordIdForDelete) && 
            String.isNotBlank(recordObjectForDelete) && String.isNotEmpty(recordObjectForDelete)){
                String soql = 'SELECT Id ';
                soql += ' FROM ' + recordObjectForDelete;
                soql += ' WHERE Id = \'' + recordIdForDelete + '\'';
                lstSObject = database.query(soql);

                List<sObject> lstCVSObject = new List<sObject>();
                soql = 'SELECT Id FROM Course_Module_Version__c';
                soql += ' WHERE Course_Module__c = \'' + recordIdForDelete + '\'';
                lstCVSObject = database.query(soql);
                if(!lstCVSObject.isEmpty() && lstCVSObject.size() > 0){
                    delete lstCVSObject;
                }

                if(!lstSObject.isEmpty() && lstSObject.size() > 0){
                    delete lstSObject;
                }
            }
        } catch (Exception e) {
            throw new LMSException( e.getMessage() );
        }
    }
    public class LMSException extends Exception{
        
    }

}