public with sharing class LMS_AllProfileController {
    @AuraEnabled
    public static Map<String, Object> getAllProfiles(){
        Map<String, Object> response = new Map<String, Object>();
        try {
            list<Option> profileOptions = new list<Option>();
            
            for( Contact profileCatalouge : [SELECT Id, Name FROM Contact] ){
                profileOptions.add(new Option(profileCatalouge.Name,profileCatalouge.Id));
            }
            response.put( 'profileOptions', profileOptions );
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }
    public class Option{
        @AuraEnabled public String label;
        @AuraEnabled public String value;

        public Option( String label, String value ){
            this.label = label;
            this.value = value;
        }
    }

    @AuraEnabled
    public static InformationWrapper getContact(String contactId){
        try{
        
        InformationWrapper wrapper = new InformationWrapper();
        Contact con = [SELECT Id,Birthdate,Total_Training_Hours__c, Email, Role__c, Title, Phone, MailingAddress, FirstName, LastName, Location__c,Department, Supervisor__c FROM Contact WHERE Id = : contactId];
        wrapper.birthDate = con.Birthdate;
        wrapper.location = con.Location__c;
        wrapper.department = con.Department;
        wrapper.supervisor = con.Supervisor__c;
        wrapper.firstName = con.FirstName;
        wrapper.lastName = con.LastName;
        wrapper.email = con.Email;
        wrapper.role = con.Role__c;
        wrapper.title = con.Title;
        wrapper.trainingDurationInHours = con.Total_Training_Hours__c;
        wrapper.phone = con.Phone;

        return wrapper;
    }catch(Exception ex){
        System.debug(ex.getMessage());
        return null;
    }
    
}
    
    public class InformationWrapper{
        
        @AuraEnabled public String location;
        @AuraEnabled public String department;
        @AuraEnabled public String supervisor;
        @AuraEnabled public String firstName;
        @AuraEnabled public String lastName;
        @AuraEnabled public Date birthDate;
        @AuraEnabled public String email;
        @AuraEnabled public String role;
        @AuraEnabled public String title;
        @AuraEnabled public String trainingDurationInHours;
        @AuraEnabled public String phone;
        
    }


    @AuraEnabled
    public static ProfileWrapper getProfileInfo(Id contactId){
        ProfileWrapper pw = new ProfileWrapper();
        List<Contact> conList = [SELECT Id,About_Me__c
                                 FROM Contact
                                 WHERE Id = :contactId
                                 LIMIT 1]; 
                                   
       List<ContentDocumentLink> cdlList = [SELECT ContentDocumentId, LinkedEntityId
                                            FROM ContentDocumentLink 
                                            WHERE LinkedEntityId = :contactId
                                            AND LinkedEntity.Type='Contact'
                                            ORDER BY Id dESC];
        if(!cdlList.isEmpty()){
            Id contentDocumentId = cdlList[0].ContentDocumentId;
            List<ContentVersion> cvList = [SELECT Id, FileExtension, Title 
                                           FROM ContentVersion
                                           WHERE ContentDocumentId = :contentDocumentId
                                           LIMIT 1]; 
            pw.contentVersionId = cvList[0].Id;
            pw.fileName = cvList[0].Title + '.' + cvList[0].FileExtension;
        }
          
                                        
        pw.aboutMe = conList[0].About_Me__c;
       
        return pw;
    }
    public class ProfileWrapper {
        @AuraEnabled  public String certificationId {get;set;}
        @AuraEnabled  public String certificationName {get;set;}
        @AuraEnabled  public Date certificationDate {get;set;}
        @AuraEnabled  public String issuingAuthority {get;set;}
        @AuraEnabled  public String aboutMe {get;set;}
        @AuraEnabled  public String contentVersionId {get;set;}
        @AuraEnabled  public String fileName {get;set;}
    }

    @AuraEnabled
    public static List<ProfileWrapper> getCertificationsList(String contactId){
        List<Certification__c> certicationList = [SELECT Id, Name, Certification_Date__c, Issuing_Authority__c
                                                  FROM Certification__c
                                                  WHERE Contact__c = :contactId
                                                  ORDER BY Certification_Date__c DESC];
         
        List<ProfileWrapper> certificationWrapperList = new List<ProfileWrapper>();
        for(Certification__c certification : certicationList) {
            ProfileWrapper cw = new ProfileWrapper();
            cw.certificationId = certification.Id;
            cw.certificationName = certification.Name;
            cw.certificationDate = certification.Certification_Date__c;
            cw.issuingAuthority = certification.Issuing_Authority__c;
            certificationWrapperList.add(cw);
        }      
        return certificationWrapperList;                             
    }

    @AuraEnabled
    public static List<EventWrapper> getAllEvents(Id contactId){
        List<Course_Schedule__c> courseScheduleList = [SELECT Id,Name, Start_Date__c ,End_Date__c
                                                        FROM Course_Schedule__c
                                                        WHERE Session_Type__c = 'Instructor'
                                                        AND Instructor__c =: contactId];
        List<EventWrapper> eventList= new List<EventWrapper>();
        for(Course_Schedule__c cs : courseScheduleList){
            EventWrapper ew = new EventWrapper();
            ew.courseScheduleId = cs.Id;
            ew.courseScheduleName = cs.Name;
            ew.courseScheduleStartDate = cs.Start_Date__c;
            ew.courseScheduleEndDate = cs.End_Date__c;
            eventList.add(ew);
        }
        return eventList;
    }

    public class EventWrapper {
        @AuraEnabled  public String courseScheduleId {get;set;}
        @AuraEnabled  public String courseScheduleName {get;set;}
        @AuraEnabled  public DateTime courseScheduleStartDate {get;set;}
        @AuraEnabled  public DateTime courseScheduleEndDate {get;set;}
    }


    @AuraEnabled
    public static List<ComplianceWrapper> getUserCompliance(Id contactId){
        List<User_Compliance_Status__c> userComplianceStatusList = [SELECT Id,Compliance__c,Name,Status__c
                                                                    FROM User_Compliance_Status__c
                                                                    WHERE Contact__c =: contactId];
        List<ComplianceWrapper> complianceWrapperList = new List<ComplianceWrapper>();
        for(User_Compliance_Status__c ucs : userComplianceStatusList){
            ComplianceWrapper cw = new ComplianceWrapper();
            cw.complianceId = ucs.Id;
            cw.complianceName = ucs.Compliance__c;
            cw.courseScheduleName = ucs.Name;
            cw.complianceStatus = ucs.Status__c;
            complianceWrapperList.add(cw);
        }
        return complianceWrapperList;
    }

    @AuraEnabled
    public static ComplianceWrapper getUserComplianceDetails(Id userComplianceId){
        List<User_Compliance_Status__c> userComplianceStatusList = [SELECT Id,Compliance__c,Name,Status__c
                                                                    FROM User_Compliance_Status__c
                                                                    WHERE Id =: userComplianceId];
        
        ComplianceWrapper cw = new ComplianceWrapper();
        cw.complianceId = userComplianceStatusList[0].Id;
        cw.complianceName = userComplianceStatusList[0].Compliance__c;
        cw.courseScheduleName = userComplianceStatusList[0].Name;
        cw.complianceStatus = userComplianceStatusList[0].Status__c;
        return cw;
    }
    public class ComplianceWrapper {
        @AuraEnabled  public Id complianceId {get;set;}
        @AuraEnabled  public String complianceName {get;set;}
        @AuraEnabled  public String courseScheduleName {get;set;}
        @AuraEnabled  public String complianceStatus {get;set;}
    }

    @AuraEnabled
    public static void upsertUserCompliance(String complianceString, Boolean isUpdate,Id contactId){
        ComplianceWrapper cw = (ComplianceWrapper)JSON.deserialize(complianceString, ComplianceWrapper.class);
        
        User_Compliance_Status__c compliance = new User_Compliance_Status__c();
        compliance.Contact__c = contactId;
        compliance.Compliance__c = cw.complianceName;
        compliance.Name = cw.courseScheduleName;
        compliance.Status__c = cw.complianceStatus;
        if(isUpdate == true) {
            compliance.Id = cw.complianceId;
            update compliance;
        } else {
            insert compliance;
        }
    }

    @AuraEnabled
    public static void deleteCertification(Id certificationId){
        List<Certification__c> certificationList = [SELECT Id
                                                    FROM Certification__c
                                                    WHERE Id =: certificationId];
        delete certificationList;
    }

    @AuraEnabled
    public static void deleteUserCompliance(Id userComplianceId){
        List<User_Compliance_Status__c> userComplianceStatusList = [SELECT Id
                                                                    FROM User_Compliance_Status__c
                                                                    WHERE Id =: userComplianceId];
        delete userComplianceStatusList;
    }

    @AuraEnabled
    public static void upsertCertification(String certificationString, Boolean isUpdate, Id contactId){
        ProfileWrapper cw = (ProfileWrapper)JSON.deserialize(certificationString, ProfileWrapper.class);
        
        Certification__c certification = new Certification__c();
        certification.Contact__c = contactId;
        certification.Name = cw.certificationName;
        certification.Certification_Date__c = cw.certificationDate;
        certification.Issuing_Authority__c = cw.issuingAuthority;
        if(isUpdate == true) {
            certification.Id = cw.certificationId;
            update certification;
        } else {
            insert certification;
        }
    }

    @AuraEnabled
    public static  List<UserEnrollmentWrapper> getUserEnrollmentList(Id contactId){
        List<User_Enrollment__c> userEnrollmentList = [SELECT Id, Completed_Date__c,Name, Course_Schedule__r.Course_Catalogue__r.Duration_In_Hours_and_Mintues__c 
                                                        FROM User_Enrollment__c 
                                                        WHERE Status__c = 'Completed' 
                                                        And Contact__c =: contactId ];
        List<UserEnrollmentWrapper> enrollmentWrapperList = new List<UserEnrollmentWrapper>();
        for(User_Enrollment__c ue : userEnrollmentList){
            UserEnrollmentWrapper uew = new UserEnrollmentWrapper();
            uew.enrollmentId = ue.Id;
            uew.enrollmentCompletedDate = ue.Completed_Date__c;
            uew.enrollmentDurationInHours = ue.Course_Schedule__r.Course_Catalogue__r.Duration_In_Hours_and_Mintues__c;
            uew.enrollmentName = ue.Name;
            enrollmentWrapperList.add(uew);
        }
        return enrollmentWrapperList;
    }

    public class UserEnrollmentWrapper {
        @AuraEnabled  public Id enrollmentId {get;set;}
        @AuraEnabled  public Date enrollmentCompletedDate {get;set;}
        @AuraEnabled  public String enrollmentDurationInHours {get;set;}
        @AuraEnabled  public String enrollmentName {get;set;}
    }
}