/**
 * @description				: Controller class For lmsCourseModules to handle Revisit
 * @author					: Gaurav Suthar
 * @group					: group
 * @created on				: 05 Jan 2021
 * @last modified on		: 06 Jan 2021
 * @last modified by		: Gaurav Suthar
 **/

public without sharing class LMS_QuizRevisit_Controller {
    public LMS_QuizRevisit_Controller() {

    }

    @AuraEnabled
    public static String resetQuiz(Id courseModuleId){
        List<LMS_QuizUserQuestion__c> userQuestionListToDelete = new List<LMS_QuizUserQuestion__c>();

        Id conId;
        for(User usr : [SELECT Id, ContactId FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1]) {
            conId = usr.ContactId;
        }

        List<User_Module_Association__c> umaToUpdateList = new List<User_Module_Association__c>();
        

        for(User_Module_Association__c uma : [SELECT Id, Status__c 
                                                FROM User_Module_Association__c 
                                                WHERE Course_Module__c =: courseModuleId
                                                AND User_Enrollment__r.Contact__c =: conId]) {
            uma.Status__c = 'In Progress';
            umaToUpdateList.add(uma);
        }
        
        if(umaToUpdateList.size() > 0) {
            update umaToUpdateList;
        }
        
        for(LMS_QuizUserQuestion__c userQuestion : [SELECT Id, Quiz_Question__c 
                                                    FROM LMS_QuizUserQuestion__c 
                                                    WHERE Quiz_Question__r.Course_Module__c =: courseModuleId
                                                    AND User__c =: UserInfo.getUserId()]) {

            userQuestionListToDelete.add(userQuestion);
        }

        if(userQuestionListToDelete.size() > 0) {
            delete userQuestionListToDelete;
            return 'Reset Successful';
        }
        else{
            return 'Reset Failed';
        }
    }

    @AuraEnabled
    public static String handleNumberOfLaunches(Id courseModuleId){

        Id conId;
        for(User usr : [SELECT Id, ContactId FROM User WHERE Id=:UserInfo.getUserId() LIMIT 1]) {
            conId = usr.ContactId;
        }
        Decimal sequenceNumber = 0;
        List<User_Module_Association__c> umaToUpdateList = new List<User_Module_Association__c>();
        
        for(User_Module_Association__c uma : [SELECT Id, Number_of_Launches__c ,Course_Module__r.Sequence__c,Course_Module__r.Course_Catalogue__r.Name,Status__c
                                                FROM User_Module_Association__c 
                                                WHERE Course_Module__c =: courseModuleId
                                                AND User_Enrollment__r.Contact__c =: conId]) {
            if(uma.Course_Module__r.Sequence__c == 1){
                
                    if(uma.Number_of_Launches__c == null) {
                        uma.Number_of_Launches__c = 0;
                    }
                    if(uma.Status__c != 'Completed'){
                        uma.Status__c = 'In progress'; 
                    }
                    uma.Number_of_Launches__c = uma.Number_of_Launches__c + 1;
                    umaToUpdateList.add(uma);
            }else{
                sequenceNumber = uma.Course_Module__r.Sequence__c - 1;
                String courseName = uma.Course_Module__r.Course_Catalogue__r.Name;
                for(User_Module_Association__c umaRecord : [SELECT Id, Number_of_Launches__c ,Course_Module__r.Sequence__c,Status__c
                                                      FROM User_Module_Association__c 
                                                      WHERE Course_Module__r.Sequence__c =: sequenceNumber
                                                      AND Course_Module__r.Course_Catalogue__r.Name =: courseName
                                                      AND User_Enrollment__r.Contact__c =: conId]){
                                                        if(umaRecord.Status__c =='Completed'){
                                                            if(uma.Number_of_Launches__c == null) {
                                                                uma.Number_of_Launches__c = 0;
                                                            }
                                                            uma.Number_of_Launches__c = uma.Number_of_Launches__c + 1;
                                                            if(uma.Status__c != 'Completed'){
                                                                uma.Status__c = 'In progress'; 
                                                            }
                                                            umaToUpdateList.add(uma);
                                                        }
                                                }
                                                
            }

               

            
            
        }
        
        if(umaToUpdateList.size() > 0) {
            update umaToUpdateList;
            return 'Number of Launches Updated';
        }
        else{
            return 'Not updated';
        }
    }
    @AuraEnabled
    public static void completeCourse(Id cumaId){
        try {
            User_Module_Association__c uma = new User_Module_Association__c(Id =cumaId,Status__c ='Completed',Completed_Date__c = Date.Today());
            update uma;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}