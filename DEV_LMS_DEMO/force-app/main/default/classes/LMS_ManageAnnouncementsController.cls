public without sharing class LMS_ManageAnnouncementsController {
    public LMS_ManageAnnouncementsController() {}

    @AuraEnabled(Cacheable=true)
    public static Map<String, Object> doInit(){
        Map<String, Object> response = new Map<String, Object>();
        try{
            response.put( 'announcements', getAnnouncements() );
            
            List<Schema.PicklistEntry> ple = Announcement__c.Status__c.getDescribe().getPickListValues();
            List<Option> statusOptions = new List<Option>();
            for( Schema.PicklistEntry pickListVal : ple){
                statusOptions.add(new Option(pickListVal.getLabel(),pickListVal.getValue()));
            }  
            response.put('statusOptions', statusOptions);

            ple = Announcement__c.Type__c.getDescribe().getPickListValues();
            List<Option> typeOptions = new List<Option>();
            for( Schema.PicklistEntry pickListVal : ple){
                typeOptions.add(new Option(pickListVal.getLabel(),pickListVal.getValue()));
            }  
            response.put('typeOptions', typeOptions);

            List<Option> courseCatalogueOptions = new List<Option>();
            for( Course_Catalogue__c courseCatalogue : [SELECT Id, Name FROM Course_Catalogue__c]){
                courseCatalogueOptions.add(new Option(courseCatalogue.Name, courseCatalogue.Id) );
            }  
            response.put( 'courseCatalogueOptions', courseCatalogueOptions );
        
        }catch( Exception ex ){
            throw new AuraHandledException(ex.getMessage());
        }
        return response;
    }

    @AuraEnabled
    public static list<Announcement__c> getAnnouncements(){
        return [SELECT Id, Name, Title__c, Announcement__c, Course_Catalogue__c, 
            Course_Catalogue__r.Name, Date__c, Expiry_Date__c, Status__c, Type__c 
            FROM Announcement__c];
    }

    @AuraEnabled
    public static Announcement__c getAnnouncement( String announcementId ){
        return [SELECT Id, Name, Title__c, Announcement__c, Course_Catalogue__c, 
            Course_Catalogue__r.Name, Date__c, Expiry_Date__c, Status__c, Type__c 
            FROM Announcement__c WHERE Id=:announcementId limit 1];
    }

    @AuraEnabled
    public static Map<String, Object> saveAnnouncement( String announcementJSON ){
        Map<String, Object> response = new Map<String, Object>();
        try {
            Announcement__c accouncement = (Announcement__c) JSON.deserialize( announcementJSON, Announcement__c.class );
            upsert accouncement;
            response.put( 'announcements', getAnnouncements() );
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> deleteAnnouncement( String announcementId ){
        Map<String, Object> response = new Map<String, Object>();
        try {
            delete new Announcement__c( Id = announcementId );
            response.put( 'announcements', getAnnouncements() );
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }

    public class Option{
        @AuraEnabled public String label;
        @AuraEnabled public String value;

        public Option( String label, String value ){
            this.label = label;
            this.value = value;
        }
    }
}