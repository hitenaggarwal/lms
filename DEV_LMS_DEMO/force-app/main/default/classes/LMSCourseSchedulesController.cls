/**
 * @description       : 
 * @author            : Manish Sogani
 * @group             : 
 * @last modified on  : 01-05-2021
 * @last modified by  : Manish Sogani
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   01-05-2021   Manish Sogani   Initial Version
**/
public with sharing class LMSCourseSchedulesController {
    
    @AuraEnabled
    public static Map < String, Object > fetchCourseScheduleRecords(String courseCatalogRecordId){
        Map < String, Object > response = new Map < String, Object > ();
        List<CourseScheduleWrapper> lstCourseScheduleWrapper = new List<CourseScheduleWrapper>();
        try{
            if(String.isNotBlank(courseCatalogRecordId) && String.isNotEmpty(courseCatalogRecordId)){

                Set<String> setAdditionalFields = new Set<String>();
                setAdditionalFields.add('Id');
                setAdditionalFields.add('Course_Catalogue__c');
                setAdditionalFields.add('Location__c');
                setAdditionalFields.add('Session_Type__c');
                setAdditionalFields.add('Start_Date__c');
                setAdditionalFields.add('End_Date__c');
                setAdditionalFields.add('Instructor__c');
                setAdditionalFields.add('Instructor__r.Name');
                setAdditionalFields.add('Status__c');
                setAdditionalFields.add('Auto_Enroll__c');

                String soql = '';
                soql = prepareQuery('Course_Schedule__c', setAdditionalFields, false);
                soql += ' WHERE Course_Catalogue__c = \'' + courseCatalogRecordId + '\'';
                soql += ' ORDER BY CreatedDate desc';
                for(Course_Schedule__c cs : database.query(soql)){
                    lstCourseScheduleWrapper.add(new CourseScheduleWrapper(cs));
                }
            }
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
        response.put('csWrapper', lstCourseScheduleWrapper);
        return response;
    }

    @AuraEnabled
    public static void doDeleteRecord(String recordIdForDelete, String recordObjectForDelete){
        try {
            if(String.isNotBlank(recordIdForDelete) && String.isNotEmpty(recordIdForDelete) && 
            String.isNotBlank(recordObjectForDelete) && String.isNotEmpty(recordObjectForDelete)){
                String soql = 'SELECT Id ';
                soql += ' FROM ' + recordObjectForDelete;
                soql += ' WHERE Id = \'' + recordIdForDelete + '\'';

                List<sObject> lstSObject = new List<sObject>();
                lstSObject = database.query(soql);
                if(!lstSObject.isEmpty() && lstSObject.size() > 0){
                    delete lstSObject;
                }
            }
        } catch (Exception e) {
            throw new LMSException( e.getMessage() );
        }
    }

    public class CourseScheduleWrapper{
        @AuraEnabled public Course_Schedule__c courseSchedule;
        @AuraEnabled public String courseScheduleInstructorName;

        public CourseScheduleWrapper(Course_Schedule__c courseSchedule){
            this.courseSchedule = courseSchedule;
            if(courseSchedule.Instructor__c != null){
                courseScheduleInstructorName = courseSchedule.Instructor__r.Name;
            }
        }
    }

     //Prepare SOQL query based on Object Name
     public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Schema.sObjectType objType = globalDescription.get(objName);
        Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
        Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
}