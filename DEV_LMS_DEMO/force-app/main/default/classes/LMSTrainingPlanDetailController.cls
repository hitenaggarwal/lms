public without sharing class LMSTrainingPlanDetailController {
    @AuraEnabled
    public static List<TrainingPlanCourse> getTrainingPlanCourses(Id recordId){
        List<TrainingPlanCourse> trainingPlanCourseList = new List<TrainingPlanCourse>();
        try {
            for(Training_Plan_Course_Association__c tpc : [SELECT Id, Course_Catalogue__c,Course_Catalogue__r.Name,Course_Catalogue__r.Description__c
                                                            FROM Training_Plan_Course_Association__c
                                                            WHERE  Training_Plan__c =:recordId]){
                                                                TrainingPlanCourse tpcWrapper = new TrainingPlanCourse();
                                                                tpcWrapper.courseName = tpc.Course_Catalogue__r.Name;
                                                                tpcWrapper.courseDescription = tpc.Course_Catalogue__r.Description__c;
                                                                tpcWrapper.courseId = tpc.Course_Catalogue__c;
                                                                trainingPlanCourseList.add(tpcWrapper );
                                                            }
                                                            return trainingPlanCourseList;
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    public class TrainingPlanCourse{
        @AuraEnabled public String courseName;
        @AuraEnabled public String courseDescription;
        @AuraEnabled public Id courseId;

    }
    @AuraEnabled
    public static Training_Plan__c getTrainingPlanDetails(Id recordId){
        try {
            for(Training_Plan__c tp : [SELECT Id , Name,Description__c
                                        FROM Training_Plan__c
                                        WHERE Id =:recordId]){
                                            return tp;
                                        }
                                        return new Training_Plan__c();
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String createTrainingPlanEnrollemnt(Training_Plan_Enrol__c newRecord, Id userId){
        List<User>userList = [SELECT Id,ContactId
                              FROM User
                              WHERE Id =: userId];

        Id contactId = userList.get(0).ContactId;
        Id trainingPlan = newRecord.Training_Plan__c;
        List<Training_Plan_Enrol__c> tpeList = [SELECT Id,Learner__c,Training_Plan__c
                                                FROM Training_Plan_Enrol__c
                                                WHERE Learner__c =:contactId
                                                AND Training_Plan__c =:trainingPlan
                                                ORDER BY Createddate DESC];
        if(!tpeList.isEmpty()){
            Training_Plan_Enrol__c existingRecord = new Training_Plan_Enrol__c();
            existingRecord = tpeList.get(0);
            existingRecord.Type__c = newRecord.Type__c;
            existingRecord.Comment__c = newRecord.Comment__c;
            existingRecord.New_Date__c = newRecord.New_Date__c;
            update existingRecord;
            return existingRecord.Id;
        }
        else{
            throw new AuraHandledException('Please Enroll First.');
        }
                                
    
    }
    @AuraEnabled
    public static String createTrainingPlanEnrollemntFeedback(Training_Plan_Enrol__c newRecord, Id userId){
        List<User>userList = [SELECT Id,ContactId
                              FROM User
                              WHERE Id =: userId];

        Id contactId = userList.get(0).ContactId;
        Id trainingPlan = newRecord.Training_Plan__c;
        List<Training_Plan_Enrol__c> tpeList = [SELECT Id,Learner__c,Training_Plan__c
                                                FROM Training_Plan_Enrol__c
                                                WHERE Learner__c =:contactId
                                                AND Training_Plan__c =:trainingPlan
                                                ORDER BY Createddate DESC];
        if(!tpeList.isEmpty()){
            Training_Plan_Enrol__c existingRecord = new Training_Plan_Enrol__c();
            existingRecord = tpeList.get(0);
            existingRecord.Proposed_Changes__c = newRecord.Proposed_Changes__c;
            update existingRecord;
            return existingRecord.Id;
        }
        else{
            throw new AuraHandledException('Please Enroll First.');
        }

}
}