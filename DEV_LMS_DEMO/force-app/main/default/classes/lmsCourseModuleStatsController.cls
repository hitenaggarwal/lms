public without sharing class lmsCourseModuleStatsController {
    @AuraEnabled
    public static CourseCountWrapper  getCourseCountDetails(){
        CourseCountWrapper lsmCourseCountWrapper = new CourseCountWrapper();
        for (AggregateResult ar : [SELECT count(Id),Status__c FROM Course_Module__c group by  Status__c]){
            if(ar.get('Status__c')=='Draft'){
                lsmCourseCountWrapper.draftCourseModule = Integer.valueOf( ar.get('expr0'));
            }
            if(ar.get('Status__c')=='Published'){
                lsmCourseCountWrapper.publishedCourseModule = Integer.valueOf( ar.get('expr0'));
            }
        }
        for (AggregateResult ar : [SELECT count(Id),Status__c FROM Course_Catalogue__c group by  Status__c]){
            if(ar.get('Status__c')=='Draft'){
                lsmCourseCountWrapper.draftCourseCatalogue = Integer.valueOf( ar.get('expr0'));
            }
            if(ar.get('Status__c')=='Published'){
                lsmCourseCountWrapper.publishedCourseCatalogue = Integer.valueOf( ar.get('expr0'));
                
            }	
            if(ar.get('Status__c')=='Archived'){
                lsmCourseCountWrapper.archivedCourseCatalogue = Integer.valueOf( ar.get('expr0'));
                
            }
        }
        for (User_Module_Association__c userModuleAssociationRecord : [SELECT Id,Status__c,Due_Date__c FROM User_Module_Association__c ]){
            
            if(userModuleAssociationRecord.Status__c == 'In Progress' || userModuleAssociationRecord.Status__c == 'Not Started'){      
                lsmCourseCountWrapper.incompleteUserModuleAssociation += 1; 
            }
            if(userModuleAssociationRecord.Status__c == 'Completed'){
                lsmCourseCountWrapper.completedUserModuleAssociation += 1; 
            }
            if(userModuleAssociationRecord.Due_Date__c < Date.today()){
                lsmCourseCountWrapper.overdueUserModuleAssociation += 1; 
            }
        }


        return lsmCourseCountWrapper;
                                            
    }

    public class CourseCountWrapper{
        @AuraEnabled public Integer draftCourseModule;
        @AuraEnabled public Integer publishedCourseModule;
        @AuraEnabled public Integer draftCourseCatalogue;
        @AuraEnabled public Integer publishedCourseCatalogue;
        @AuraEnabled public Integer archivedCourseCatalogue;
        @AuraEnabled public Integer incompleteUserModuleAssociation;
        @AuraEnabled public Integer completedUserModuleAssociation;
        @AuraEnabled public Integer overdueUserModuleAssociation;
        public CourseCountWrapper(){
            this.draftCourseModule = 0 ;
            this.publishedCourseModule = 0 ;
            this.draftCourseCatalogue = 0 ;
            this.publishedCourseCatalogue = 0 ;
            this.archivedCourseCatalogue = 0 ;
            this.incompleteUserModuleAssociation = 0 ;
            this.completedUserModuleAssociation = 0 ;
            this.overdueUserModuleAssociation = 0 ;
        }
    }
}