public without sharing class LmsModuleFacilitieController {
    @AuraEnabled
    public static List<Facility__c>  getCourseDetails(String status){
        return [SELECT Id,Name,CreatedBy.Name
                                              
                    FROM Facility__c  order by Name];
                                            
    }
}