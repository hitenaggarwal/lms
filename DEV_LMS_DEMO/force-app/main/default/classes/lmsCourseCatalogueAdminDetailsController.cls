public with sharing class lmsCourseCatalogueAdminDetailsController {
    @AuraEnabled
    public static List<Course_Catalogue__c>  getCourseDetails(String status){
        return [SELECT Id,Name,Status__c,Total_Enrollment__c,CreatedBy.Name
                                            FROM Course_Catalogue__c 
                                            WHERE Status__c =: status order by Name];
                                            
    }
}