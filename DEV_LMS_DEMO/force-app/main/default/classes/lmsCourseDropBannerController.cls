public without sharing class lmsCourseDropBannerController {
    
    @AuraEnabled
    public static void deleteUserModuleAndEnrollment(Id userId, Id courseId){

        List<User>userList = [SELECT Id,ContactId
                              FROM User
                              WHERE Id =: userId];
        Id contactId = userList.get(0).ContactId;
        List<User_Enrollment__c> enrollmentsList = new List<User_Enrollment__c>();

        enrollmentsList = [SELECT Id, Status__c, Course_Schedule__c, Course_Updated__c, Course_Schedule__r.Course_Catalogue__c, 
                            Course_Schedule__r.Course_Catalogue__r.Name,  Course_Schedule__r.Course_Catalogue__r.Description__c, 
                            Course_Schedule__r.Course_Catalogue__r.Roles__c, Course_Schedule__r.Course_Catalogue__r.Objective__c,
                            Course_Schedule__r.Course_Catalogue__r.Summary__c, Course_Schedule__r.Session_Type__c, Contact__r.Name, Completed_Date__c
                            FROM User_Enrollment__c 
                            WHERE Contact__c = :contactId
                            AND Course_Schedule__r.Course_Catalogue__c = :courseId];
        
        List<User_Module_Association__c> userModuleAssociationList = new List<User_Module_Association__c>();

        userModuleAssociationList = [SELECT User_Enrollment__c	
                                     FROM User_Module_Association__c
                                     WHERE User_Enrollment__c IN: enrollmentsList];
        
        if(!userModuleAssociationList.isEmpty()){
            delete userModuleAssociationList;
        }
        if(!enrollmentsList.isEmpty()){
            delete enrollmentsList;
        }

    }

}