public without sharing class LMSQuizQuestionResponsesController {
    
    @AuraEnabled
    public static Map < String, Object > fetchQuizQuestionResponseRecords(String questionId){
        Map < String, Object > response = new Map < String, Object > ();
        List<QuestionResponseWrapper> lstQuestionResponseWrapper = new List<QuestionResponseWrapper>();
        try{
            if(String.isNotBlank(questionId) && String.isNotEmpty(questionId)){
                Set<String> setAdditionalFields = new Set<String>();
                setAdditionalFields.add('Id');
                setAdditionalFields.add('Question__c');
                setAdditionalFields.add('Sequence__c');
                setAdditionalFields.add('Description__c');
                setAdditionalFields.add('Is_Correct__c');

                String soql = '';
                soql = prepareQuery('Question_Response__c', setAdditionalFields, false);
                soql += ' WHERE Question__c = \'' + questionId + '\'';
                soql += ' ORDER BY Sequence__c';
                for(Question_Response__c qr : database.query(soql)){
                    lstQuestionResponseWrapper.add(new QuestionResponseWrapper(qr));
                }
            }
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
        response.put('quesResWrapper', lstQuestionResponseWrapper);
        return response;
    }

    @AuraEnabled
    public static void doDeleteRecord(String recordIdForDelete, String recordObjectForDelete){
        try {
            if(String.isNotBlank(recordIdForDelete) && String.isNotEmpty(recordIdForDelete) && 
            String.isNotBlank(recordObjectForDelete) && String.isNotEmpty(recordObjectForDelete)){
                String soql = 'SELECT Id ';
                soql += ' FROM ' + recordObjectForDelete;
                soql += ' WHERE Id = \'' + recordIdForDelete + '\'';

                List<sObject> lstSObject = new List<sObject>();
                lstSObject = database.query(soql);
                if(!lstSObject.isEmpty() && lstSObject.size() > 0){
                    delete lstSObject;
                }
            }
        } catch (Exception e) {
            throw new LMSException( e.getMessage() );
        }
    }

    public class QuestionResponseWrapper{
        @AuraEnabled public Question_Response__c questionResponse;

        public QuestionResponseWrapper(Question_Response__c questionResponse){
            this.questionResponse = questionResponse;
        }
    }

     //Prepare SOQL query based on Object Name
     public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
            Schema.sObjectType objType = globalDescription.get(objName);
            Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
            Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap(); 
            
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
}