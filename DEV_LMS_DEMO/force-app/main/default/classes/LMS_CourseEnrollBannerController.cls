public without sharing class LMS_CourseEnrollBannerController {
    
    @AuraEnabled
    public static void doCourseEnroll(String courseId){
        try {
            User currentUser = [SELECT Id, ContactId, Contact.Role__c, Contact.Is_Admin__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
                
            if(currentUser != null){
                List<Course_Relation__c> lstCourseRelation = new List<Course_Relation__c>();
                lstCourseRelation = [SELECT Id, Name, Course_Catalogue__c, Type__c, Related_Course__c FROM Course_Relation__c 
                                        WHERE Course_Catalogue__c = :courseId AND Related_Course__c != null AND Type__c = 'Prerequisite'];
                if(!lstCourseRelation.isEmpty() && lstCourseRelation.size() > 0){
                    Set<String> setRelatedCourseIds = new Set<String>();
                    for(Course_Relation__c cr : lstCourseRelation){
                        setRelatedCourseIds.add(cr.Related_Course__c);
                    }
                    if(setRelatedCourseIds.size() > 0){
                        List<User_Enrollment__c> lstUserEnrollment = new List<User_Enrollment__c>();
                        lstUserEnrollment = [SELECT Id FROM User_Enrollment__c WHERE Contact__c = :currentUser.ContactId 
                                            AND Course_Schedule__r.Course_Catalogue__c IN :setRelatedCourseIds 
                                            AND Status__c = 'Completed'];
                        if(lstCourseRelation.size() != lstUserEnrollment.size()){
                            throw new LMSException( 'Please complete all Pre-Requisite Courses first to enroll.' );
                        }
                    }
                }
                

                List<Course_Schedule__c> courseScheduleList = new List<Course_Schedule__c>();
                courseScheduleList = [SELECT Id, Available_Seats__c, Total_Confirmed__c, Require_Supervisor_Approval__c 
                FROM Course_Schedule__c WHERE Course_Catalogue__c = :courseId AND Session_Type__c = 'Instructor' LIMIT 1];

                if(!courseScheduleList.isEmpty() && courseScheduleList.size() > 0){
                    User_Enrollment__c userEnroll = new User_Enrollment__c();
                    userEnroll.Contact__c = currentUser.ContactId;
                    userEnroll.Course_Schedule__c = courseScheduleList[0].Id;
                    userEnroll.Status__c = 'Not Started';
                    userEnroll.Enrolment_Status__c = 'Confirmed';
                    if(courseScheduleList[0].Available_Seats__c <= courseScheduleList[0].Total_Confirmed__c){
                        userEnroll.Enrolment_Status__c = 'Waitlist';
                    }
                    if(courseScheduleList[0].Require_Supervisor_Approval__c){
                        userEnroll.Enrolment_Status__c = 'Pending Approval';
                    }
                    insert userEnroll;

                    List<User_Module_Association__c> lstUserModuleAssociations = new List<User_Module_Association__c>();
                    for(Course_Module__c module : [ SELECT Id, Module_Name__c,Type__c FROM Course_Module__c 
                                                    WHERE Course_Catalogue__c = :courseId AND Status__c = 'Published' ORDER BY Sequence__c]) {
                        User_Module_Association__c uma = new User_Module_Association__c();
                        uma.User_Enrollment__c = userEnroll.Id;
                        uma.Course_Module__c = module.Id;
                        uma.Status__c = 'Not Started';
                        lstUserModuleAssociations.add(uma);
                    }
                    if(!lstUserModuleAssociations.isEmpty() && lstUserModuleAssociations.size() > 0){
                        insert lstUserModuleAssociations;
                    }
                }else{
                    throw new LMSException( 'No Course Schedule found.' );
                }
            }

        } catch (Exception e) {
            throw new LMSException( e.getMessage() );
        }
    }

    public class LMSException extends Exception{
        
    }
}