/**
 * @description       : 
 * @author            : Manish Sogani
 * @group             : 
 * @last modified on  : 01-05-2021
 * @last modified by  : Manish Sogani
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   01-05-2021   Manish Sogani   Initial Version
**/
public with sharing class LMSCourseModuleRolesController {
    
    @AuraEnabled
    public static Map < String, Object > fetchCourseRoleRecords(String courseScheduleRecordId){
        Map < String, Object > response = new Map < String, Object > ();
        List<CourseRoleWrapper> lstCourseRoleWrapper = new List<CourseRoleWrapper>();
        try{
            if(String.isNotBlank(courseScheduleRecordId) && String.isNotEmpty(courseScheduleRecordId)){

                Set<String> setAdditionalFields = new Set<String>();
                setAdditionalFields.add('Id');
                setAdditionalFields.add('Name');
                setAdditionalFields.add('Course_Schedule__c');
                setAdditionalFields.add('Role__c');
                setAdditionalFields.add('Contact__c');
                setAdditionalFields.add('Contact__r.Name');
                setAdditionalFields.add('Status__c');
                setAdditionalFields.add('Show_All_Data__c');
                setAdditionalFields.add('View_Exam_Result__c');

                String soql = '';
                soql = prepareQuery('Course_Module_Role__c', setAdditionalFields, false);
                soql += ' WHERE Course_Schedule__c = \'' + courseScheduleRecordId + '\'';
                soql += ' ORDER BY Name';
                for(Course_Module_Role__c cr : database.query(soql)){
                    lstCourseRoleWrapper.add(new CourseRoleWrapper(cr));
                }
            }
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
        response.put('crWrapper', lstCourseRoleWrapper);
        return response;
    }

    @AuraEnabled
    public static void doDeleteRecord(String recordIdForDelete, String recordObjectForDelete){
        try {
            if(String.isNotBlank(recordIdForDelete) && String.isNotEmpty(recordIdForDelete) && 
            String.isNotBlank(recordObjectForDelete) && String.isNotEmpty(recordObjectForDelete)){
                String soql = 'SELECT Id ';
                soql += ' FROM ' + recordObjectForDelete;
                soql += ' WHERE Id = \'' + recordIdForDelete + '\'';

                List<sObject> lstSObject = new List<sObject>();
                lstSObject = database.query(soql);
                if(!lstSObject.isEmpty() && lstSObject.size() > 0){
                    delete lstSObject;
                }
            }
        } catch (Exception e) {
            throw new LMSException( e.getMessage() );
        }
    }

    public class CourseRoleWrapper{
        @AuraEnabled public Course_Module_Role__c courseRole;
        @AuraEnabled public String courseRoleContactName;

        public CourseRoleWrapper(Course_Module_Role__c courseRole){
            this.courseRole = courseRole;
            if(courseRole.Contact__c != null){
                courseRoleContactName = courseRole.Contact__r.Name;
            }
        }
    }

     //Prepare SOQL query based on Object Name
     public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Schema.sObjectType objType = globalDescription.get(objName);
        Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
        Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
}