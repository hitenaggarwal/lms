public without sharing class LMSTrainingPlanController {
    @AuraEnabled
    public static List<TrainingPlanWrapper> fetchTrainingPlanWrapper(){
        List<TrainingPlanWrapper> trainingPlanWrapperList = new List<TrainingPlanWrapper>();
        Map<Id,List<Training_Plan_Course_Association__c>> trainingPlantoCourseListMap = new Map<Id,List<Training_Plan_Course_Association__c>>();
        Map<Id,Training_Plan_Enrol__c> trainingPlanToEnrolmentMap = new Map<Id,Training_Plan_Enrol__c>();
        Map<Id,Training_Plan__c> trainingPlanIdToRecordMap = new Map<Id,Training_Plan__c>();
        User currentUser = [SELECT Id, ContactId,Contact.Role__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
       
        if(currentUser != null && currentUser.ContactId != null){
            Set<Id> trainingPlanIdSet = new Set<Id>();
            String role = '\''+currentUser.Contact.Role__c+'\'';
            String planQuery = 'SELECT Id FROM Training_Plan__c WHERE Role__c includes('+role+')';
            List<Training_Plan__c> planList = (List<Training_Plan__c>)Database.query(planQuery);
            for(Training_Plan__c plan : planList){
                trainingPlanIdSet.add(plan.Id);
            }
            for(Training_Plan_Enrol__c enrolment : [SELECT Id,Training_Plan__c 
                                                    FROM Training_Plan_Enrol__c 
                                                    WHERE Learner__c =:currentUser.ContactId]){
                trainingPlanIdSet.add(enrolment.Training_Plan__c);
            }
            if(trainingPlanIdSet.size() > 0){
                for(Training_Plan__c plan : [SELECT Id,Name,Role__c,Description__c,(SELECT Id,Course_Catalogue__c,Course_Catalogue__r.Name,
                                            Course_Catalogue__r.Rating__c,Course_Catalogue__r.Total_Enrollment__c
                                            FROM Training_Plan_Course_Association__r) FROM Training_Plan__c
                                            WHERE Id IN:trainingPlanIdSet]){
                                    trainingPlanIdToRecordMap.put(plan.Id,plan);
                                    trainingPlantoCourseListMap.put(plan.Id,plan.Training_Plan_Course_Association__r);
                }
                for(Training_Plan_Enrol__c enrolment : [SELECT Id,Completed_Date__c,Due_Date__c,Enrolment_Date__c,
                                                        Learner__c,Status__c,Training_Plan__c
                                                        FROM Training_Plan_Enrol__c
                                                        WHERE Training_Plan__c IN:trainingPlanIdSet
                                                        AND Learner__c =:currentUser.ContactId]){
                                                            trainingPlanToEnrolmentMap.put(enrolment.Training_Plan__c,enrolment);
                                                        }
                for(Id trainingPlanId : trainingPlanIdSet){
                    if(trainingPlanToEnrolmentMap.containsKey(trainingPlanId)){
                        if(trainingPlantoCourseListMap.containsKey(trainingPlanId)){
                            List<Training_Plan_Course_Association__c> courseList = trainingPlantoCourseListMap.get(trainingPlanId);
                            List<TrainingPlanCourses> trainingCourseList = new List<TrainingPlanCourses>();
                            Training_Plan__c planRecord = trainingPlanIdToRecordMap.get(trainingPlanId);
                            for(Training_Plan_Course_Association__c course : courseList){
                                trainingCourseList.add(new TrainingPlanCourses(course.Course_Catalogue__r.Name,String.valueOf(course.Course_Catalogue__c),String.valueOf(course.Course_Catalogue__r.Rating__c),String.valueOf(course.Course_Catalogue__r.Total_Enrollment__c)));
                            }
                            Training_Plan_Enrol__c planEnrolment = trainingPlanToEnrolmentMap.get(trainingPlanId);
                            trainingPlanWrapperList.add(new TrainingPlanWrapper(planRecord.Name,String.valueOf(planRecord.Id),planEnrolment.Status__c,planEnrolment.Enrolment_Date__c,planEnrolment.Due_Date__c,planEnrolment.Completed_Date__c,trainingCourseList.size(),trainingCourseList,planRecord.Description__c));
                        }
                    }else{
                        if(trainingPlantoCourseListMap.containsKey(trainingPlanId)){
                            List<Training_Plan_Course_Association__c> courseList = trainingPlantoCourseListMap.get(trainingPlanId);
                            List<TrainingPlanCourses> trainingCourseList = new List<TrainingPlanCourses>();
                            Training_Plan__c planRecord = trainingPlanIdToRecordMap.get(trainingPlanId);
                            for(Training_Plan_Course_Association__c course : courseList){
                                trainingCourseList.add(new TrainingPlanCourses(course.Course_Catalogue__r.Name,String.valueOf(course.Course_Catalogue__c),String.valueOf(course.Course_Catalogue__r.Rating__c),String.valueOf(course.Course_Catalogue__r.Total_Enrollment__c)));
                            }
                            trainingPlanWrapperList.add(new TrainingPlanWrapper(planRecord.Name,String.valueOf(planRecord.Id),'Not Enrolled',null,null,null,trainingCourseList.size(),trainingCourseList,planRecord.Description__c));
                        }
                    }
                   // trainingPlanWrapperList
                }
            }
    } 
        return trainingPlanWrapperList;
    }

    @AuraEnabled
    public static List<Training_Plan__c> getTrainingPlans(){
        try {
            List<Training_Plan__c> trainingPlanList = new List<Training_Plan__c>();
            for(Training_Plan__c tp:[SELECT Id, Name, Duration_In_Days__c,Status__c,Is_Published__c FROM Training_Plan__c WHERE Is_Active__c = true]){
                trainingPlanList.add(tp);
            }
            return trainingPlanList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<Contact> getLearners(String trainingPlanId, Boolean isRegisterd){
        try {
            User user = [SELECT Id,ContactId FROM User WHERE Id =: UserInfo.getUserId()];
            Set<String> learnersIdSet = new Set<String>();
            List<Contact> allReporteesList = new List<Contact>();
            List<Contact> reporteesList = new List<Contact>();
            for(Contact con : [SELECT Id,Name FROM Contact WHERE Supervisor__c =: user.ContactId]){
                allReporteesList.add(con);
            }

            for(Training_Plan_Enrol__c tpe : [SELECT Learner__c FROM Training_Plan_Enrol__c WHERE Training_Plan__c =: trainingPlanId AND Learner__r.Supervisor__c =: user.ContactId]){
                learnersIdSet.add(tpe.Learner__c);
            }

            for(Contact con: allReporteesList){
                if(isRegisterd){
                    if(learnersIdSet.contains(con.Id)){
                        reporteesList.add(con);
                    } 
                }
                else{
                    if(!learnersIdSet.contains(con.Id)){
                        reporteesList.add(con);
                    }
                }   
            }

            return reporteesList;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<Training_Plan_Enrol__c> getEnrolledLearners(String trainingPlanId){
        try {
            User user = [SELECT Id,ContactId FROM User WHERE Id =: UserInfo.getUserId()];
            Set<String> learnersIdSet = new Set<String>();
            List<Contact> allReporteesList = new List<Contact>();
            List<Training_Plan_Enrol__c> enrolledLearnersList = new List<Training_Plan_Enrol__c>();
            for(Contact con : [SELECT Id,Name FROM Contact WHERE Supervisor__c =: user.ContactId]){
                allReporteesList.add(con);
            }
            Map<Id, Training_Plan_Enrol__c> learnersIdMap = new Map<Id, Training_Plan_Enrol__c>();
            for(Training_Plan_Enrol__c tpe : [SELECT Id, Learner__r.Name, Enrolment_Date__c, Status__c
                                              ,Certifying_Official__c,Supervisor__c,Training_Administrator__c,Coach__c,
                                              Certifying_Official__r.Name, Supervisor__r.Name,Training_Administrator__r.Name,
                                              Coach__r.Name,WebTA_hours__c
                                              FROM Training_Plan_Enrol__c WHERE Training_Plan__c =: trainingPlanId 
                                              AND Learner__r.Supervisor__c =: user.ContactId]){
                learnersIdMap.put(tpe.Learner__c, tpe);
            }

            for(Contact con: allReporteesList){
                if(learnersIdMap.keySet().contains(con.Id)){
                    enrolledLearnersList.add(learnersIdMap.get(con.Id));
                } 
            }

            return enrolledLearnersList;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<Training_Plan_Enrol__c> getTrainingPlanEnrollments(String trainingPlanId){
        try {
            User user = [SELECT Id,ContactId FROM User WHERE Id =: UserInfo.getUserId()];
            return [SELECT Id,Learner__r.Name,Proposed_Changes__c FROM Training_Plan_Enrol__c WHERE Training_Plan__c =: trainingPlanId AND Learner__r.Supervisor__c =: user.ContactId AND Proposed_Changes__c != null];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class TrainingPlanWrapper{
       @AuraEnabled public String planName;
       @AuraEnabled public String recId;
       @AuraEnabled public String description;
       @AuraEnabled public String enrolmentStatus;
       @AuraEnabled public Date enrolmentDate;
       @AuraEnabled public Date dueDate;
       @AuraEnabled public Date completedDate;
       @AuraEnabled public Integer totalCourses;
       @AuraEnabled public Boolean showEnrollButton;

       @AuraEnabled public List<TrainingPlanCourses> trainingPlanCourseList;

       public TrainingPlanWrapper(String name,String Id,String status,Date enrolmentDt,Date dueDt,Date completedDt,Integer couseCount,List<TrainingPlanCourses> courseList,String description){
            if(status == 'Not Enrolled'){
                showEnrollButton = true;
            }else{
                showEnrollButton = false;
            }
            trainingPlanCourseList = new List<TrainingPlanCourses>();
            planName = name;
            enrolmentStatus = status;
            enrolmentDate = enrolmentDt;
            dueDate = dueDt;
            completedDate = completedDt;
            totalCourses = couseCount;
            trainingPlanCourseList = courseList;
            recId = Id;
            this.description = description;

       }
    }
    public class TrainingPlanCourses{
        @AuraEnabled public String courseName;
        @AuraEnabled public String rating;
        @AuraEnabled public String moduleURL;
        @AuraEnabled public String totalEnrolments;
        public TrainingPlanCourses(String name,String url, String courseRating,String enrolment){
            courseName = name;
            moduleURL = url;
            rating = courseRating;
            totalEnrolments = enrolment;
        }
    }
}