public without sharing class LMS_Quiz_Controller   {

    @AuraEnabled
    public static Boolean validateQuiz(Id courseModuleId){
        Id courseUserModuleId ;
        Id ContactId;
        for(User u : [SELECT ContactId FROM User WHERE Id =:UserInfo.getUserId()]){
            ContactId = u.ContactId;
        }
        for(User_Module_Association__c cum : [SELECT Id ,Status__C
                                        FROM User_Module_Association__c
                                        WHERE Course_Module__c = :courseModuleId
                                        AND User_Enrollment__r.Contact__c =:ContactId]){
                                            if(cum.Status__c == 'Completed'){
                                              System.debug('Completed');
                                                return false;
                                            }
                                        }
        initiateQuiz(courseModuleId);                                
        return true;
    }

    @AuraEnabled
    public static void initiateQuiz(Id courseModuleId){
        Set<Integer> existingSequenceNumberSet = new Set<Integer>();
        Integer totalScore = 0;
        Id ContactId;
        for(User u : [SELECT ContactId FROM User WHERE Id =:UserInfo.getUserId()]){
            ContactId = u.ContactId;
        }

        Id courseUserModuleId ;
        Id courseUserEnrollmentId;
        for(User_Module_Association__c cum : [SELECT Id ,User_Enrollment__c
                                        FROM User_Module_Association__c
                                        WHERE Course_Module__c = :courseModuleId
                                        AND User_Enrollment__r.Contact__c =:ContactId]){
                                            courseUserModuleId = cum.Id;
                                               courseUserEnrollmentId = cum.User_Enrollment__c;
                                        }
        Set<Id> allQuestionIdSet = new Set<Id>();    
        for(Question__c q : [SELECT Id,Score__c FROM Question__c WHERE Course_Module__c =:courseModuleId]){
            allQuestionIdSet.add(q.Id);
            totalScore += Integer.valueOf(q.Score__c);
        }
                                    
        Set<Id> userQuestionIdSet = new Set<Id>();
        for(LMS_QuizUserQuestion__c lquq : [ SELECT Id,Quiz_Question__c,Sequence__c
                                             FROM LMS_QuizUserQuestion__c
                                             WHERE Course_User_Module__c =:courseUserModuleId
                                             AND User__c =:UserInfo.getUserId() ]){
                                                userQuestionIdSet.add(lquq.Quiz_Question__c);
                                                existingSequenceNumberSet.add(Integer.valueOf(lquq.Sequence__c));
                                             }
      
       
        List<LMS_QuizUserQuestion__c> userQuestionList = new List<LMS_QuizUserQuestion__c>();
        for(Id qId :allQuestionIdSet ){
            if(!userQuestionIdSet.contains(qId)){
                LMS_QuizUserQuestion__c lquq = new LMS_QuizUserQuestion__c();
                lquq.Quiz_Question__c = qId;
                lquq.Course_User_Module__c = courseUserModuleId;
                lquq.User__c = UserInfo.getUserId();
                lquq.Course_User_Enrollment__c = courseUserEnrollmentId;
                Integer randomSequenceNumber = generateRandonNo(existingSequenceNumberSet, allQuestionIdSet.size()-1);
                System.debug('randomNumber Generated'+randomSequenceNumber);
                
                existingSequenceNumberSet.add(randomSequenceNumber);
                System.debug('All Existing Questions'+existingSequenceNumberSet);
                lquq.Sequence__c = randomSequenceNumber;
                userQuestionList.add(lquq);
            }

        }
        if(!userQuestionList.isEmpty()){
            insert userQuestionList;
            update new User_Module_Association__c(Id=courseUserModuleId,Quiz_Score__c = totalScore);
        }
    }
    public class QuizResponseWrapper implements Comparable{
        @AuraEnabled
        public Integer sequenceNumber;
        @AuraEnabled
        public String questionText;
        @AuraEnabled
        public Id questionId;
        @AuraEnabled
        public Id userQuestionId;
        @AuraEnabled
        public Boolean isAnswered;
        @AuraEnabled
        public Boolean isSingleChoice;
        @AuraEnabled
        public List<Response> listOfResponses;
        
        public Integer compareTo(Object compareTo){
            QuizResponseWrapper qWrapper = (QuizResponseWrapper)compareTo;
            if (sequenceNumber> qWrapper.sequenceNumber) {
                // Set return value to a positive value.
                 return 1;
            } else if (sequenceNumber < qWrapper.sequenceNumber) {
                // Set return value to a negative value.
                 return -1;
            }
                return 0;
        }
       
    }
    public class Response{
        @AuraEnabled
        public String className;
        @AuraEnabled
        public String answerText;
        @AuraEnabled
        public Id answerId;
        @AuraEnabled
        public Boolean isCorrect;
        @AuraEnabled
        public Boolean isSelected;
    }

    @AuraEnabled(cacheable=true)    
    public static List<QuizResponseWrapper> getQuizResponses(Id courseModuleId,Boolean review){
        List<QuizResponseWrapper> QuizResponseWrapperList = new  List<QuizResponseWrapper> ();
        Map<Id,QuizResponseWrapper> questionToResponsesMap = new  Map<Id,QuizResponseWrapper>();
        Map<Id,Set<Id>> userQuestionToResponsesMap = new   Map<Id,Set<Id>>();
        Id courseUserModuleId ;
        Set<Id> allQuestionIdSet = new Set<Id>();  
        Id ContactId;
        for(User u : [SELECT ContactId FROM User WHERE Id =:UserInfo.getUserId()]){
            ContactId = u.ContactId;
        }

        for(User_Module_Association__c cum : [SELECT Id 
                                        FROM User_Module_Association__c
                                        WHERE Course_Module__c = :courseModuleId
                                        AND
                                        User_Enrollment__r.Contact__c =:ContactId]){
                                            courseUserModuleId = cum.Id;
                                        }

        for(Question__c q : [SELECT Id,Description__c , Question_Type__c,
                            (SELECT Id,Description__c,Is_Correct__c FROM Question_Responses__r order by Sequence__c) 
                            FROM Question__c 
                            WHERE Course_Module__c =:courseModuleId ]){
            QuizResponseWrapper qw = new QuizResponseWrapper();
            qw.questionId = q.Id;
            qw.questionText = q.Description__c;
            qw.isSingleChoice = q.Question_Type__c =='Single Choice' ? true:false;
            List<Response> listOfResponses = new List<Response>();
            for(Question_Response__c qr : q.Question_Responses__r){
                Response r = new Response();
                if(review != null && review){
                    if(qr.Is_Correct__c){
                        r.isCorrect = true;
                        r.className = 'correct';

                    }
                }
                r.answerId = qr.Id;
                r.answerText = qr.Description__c;
                r.isSelected = false;
                listOfResponses.add(r);
            }
            qw.listOfResponses= listOfResponses;
            questionToResponsesMap.put(q.Id,qw);
        }

        for(LMS_QuizUserQuestion__c quq  : [SELECT Quiz_Question__c,Sequence__c,
                                            (SELECT Id,QuestionResponse__c FROM LMS_QuizUserResponses__r  ) 
                                            FROM LMS_QuizUserQuestion__c
                                            WHERE User__c =:UserInfo.getUserId()
                                            AND Quiz_Question__c in:questionToResponsesMap.keySet()
                                            AND Course_User_Module__c =:courseUserModuleId ]){
                                                questionToResponsesMap.get(quq.Quiz_Question__c).userQuestionId = quq.Id;
                                                questionToResponsesMap.get(quq.Quiz_Question__c).sequenceNumber = Integer.valueOf(quq.Sequence__c);
                                                
                                                for(LMS_QuizUserResponse__c qur : quq.LMS_QuizUserResponses__r){
                                                    if(!userQuestionToResponsesMap.containsKey(quq.Quiz_Question__c)){
                                                        userQuestionToResponsesMap.put(quq.Quiz_Question__c,new Set<Id>());
                                                    }
                                                    userQuestionToResponsesMap.get(quq.Quiz_Question__c).add(qur.QuestionResponse__c);
                                                }
                                            }
        System.debug(userQuestionToResponsesMap);
        System.debug(questionToResponsesMap);
        List<QuizResponseWrapper> qwList = new List<QuizResponseWrapper>();

        for(Id questionId : questionToResponsesMap.keySet() ) {
           for(Response r : questionToResponsesMap.get(questionId).listOfResponses){
             if(userQuestionToResponsesMap.containsKey(questionId)){
              if(userQuestionToResponsesMap.get(questionId).contains(r.answerId)){
                r.isSelected = true;
                questionToResponsesMap.get(questionId).isAnswered = true;
            }
            }
            if(r.isSelected){
                if(r.isCorrect != null && r.isCorrect == true){
                    r.className = 'correct';
                }else{
                    r.className = 'wrong';
                }

            }
           }
           questionToResponsesMap.get(questionId).sequenceNumber +=1;
           qwList.add(questionToResponsesMap.get(questionId));
        }  
        qwList.sort();
        return qwList;                    
    } 
    @AuraEnabled
    public static void submitResponse(Object QuizResponse,Id courseModuleId ){
        List<QuizResponseWrapper> qurWrapperList = ( List<QuizResponseWrapper> ) JSON.deserialize(JSON.serialize(QuizResponse), List<QuizResponseWrapper>.class);
        System.debug(qurWrapperList);
        Id ContactId;
        for(User u : [SELECT ContactId FROM User WHERE Id =:UserInfo.getUserId()]){
            ContactId = u.ContactId;
        }
        List<LMS_QuizUserResponse__c> quizResponseList = new List<LMS_QuizUserResponse__c>();
        Set<Id> quizUserQuestionIdSet = new Set<Id>();
        Id courseUserModuleId ;
        for(User_Module_Association__c cum : [SELECT Id ,User_Enrollment__c
                                        FROM User_Module_Association__c
                                        WHERE Course_Module__c = :courseModuleId
                                        AND User_Enrollment__r.Contact__c =:contactId]){
                                            courseUserModuleId = cum.Id;
                                        }

        for(QuizResponseWrapper qrw : qurWrapperList ){
            quizUserQuestionIdSet.add(qrw.userQuestionId);
            for(Response res : qrw.listOfResponses){
                if(res.isSelected){
                    LMS_QuizUserResponse__c qur = new LMS_QuizUserResponse__c();
                    qur.Question__c = qrw.questionId;
                    qur.LMS_QuizUserQuestion__c = qrw.userQuestionId;
                    qur.QuestionResponse__c = res.answerId;
                    quizResponseList.add(qur);
                }
            }
        }
        if(!quizResponseList.isEmpty()){
            insert quizResponseList;
        }
        Integer totalScore = 0;


        for(LMS_QuizUserQuestion__c lquq : [SELECT Id, Quiz_Question__r.Score__c,Quiz_Question__r.Total_Correct_Questions__c,Quiz_Question__r.Question_Type__c,
                                            (SELECT Id,isCorrect__c FROM LMS_QuizUserResponses__r)
                                            FROM LMS_QuizUserQuestion__c
                                            WHERE Id in :quizUserQuestionIdSet ]){
                                                Integer userTotalCorrect = 0;
                                                for(LMS_QuizUserResponse__c lqur : lquq.LMS_QuizUserResponses__r){
                                                    if(lquq.Quiz_Question__r.Question_Type__c == 'Single Choice'){
                                                        if(lqur.isCorrect__C ){
                                                            userTotalCorrect ++;
                                                        }
                                                    }else{
                                                        userTotalCorrect ++;
                                                    }
                                                }
                                                if(userTotalCorrect == lquq.Quiz_Question__r.Total_Correct_Questions__c){
                                                    totalScore += Integer.valueOf(lquq.Quiz_Question__r.Score__c);
                                                }
                                            }
        System.debug(totalScore);

        if(totalScore >=0){
            update new User_Module_Association__c(Id=courseUserModuleId,User_Score__c = totalScore,Status__c = 'Completed',Completed_Date__c=Date.today());
            updateQuizResult(courseUserModuleId);
        }

    }

    public static void updateQuizResult(Id courseUserModuleId){
        List<User_Module_Association__c> umaListToUpdate = new List<User_Module_Association__c>();
        for(User_Module_Association__c uma : [SELECT Id, Course_Module__r.Passing_Score__c, Status__c, Type__c, User_Score__c
                                            FROM User_Module_Association__c 
                                            WHERE Id=:courseUserModuleId
                                            AND Type__c = 'Quiz'
                                            AND Status__c = 'Completed']){

            if(uma != null && uma.User_Score__c != null && uma.Course_Module__r.Passing_Score__c != null) {
                String quizResult = '';
                if(uma.User_Score__c >= uma.Course_Module__r.Passing_Score__c) {
                    quizResult = 'Pass';
                }
                else {
                    quizResult = 'Fail';
                }
                umaListToUpdate.add(new User_Module_Association__c(Id=uma.Id, Quiz_Result__c=quizResult));
            }   
        }     
        
        if(umaListToUpdate.size() > 0) {
            update umaListToUpdate;
        }
    }

    @AuraEnabled(cacheable=true)
    public static map<String,Integer> getScore( Id courseModuleId){
        Id ContactId;
        for(User u : [SELECT ContactId FROM User WHERE Id =:UserInfo.getUserId()]){
            ContactId = u.ContactId;
        }
        Map<String,Integer> responseMap = new Map<String,Integer>();
        for(User_Module_Association__c cum : [SELECT Id ,User_Score__c,Quiz_Score__c
                                        FROM User_Module_Association__c
                                        WHERE Course_Module__c = :courseModuleId
                                        AND User_Enrollment__r.Contact__c =:ContactId]){
                                            responseMap.put('UserScore', Integer.valueOf(cum.User_Score__c));
                                            responseMap.put('QuizScore', Integer.valueOf(cum.Quiz_Score__c));
                                        }
        return responseMap;
    }
  
    private static Integer generateRandonNo(Set<Integer> setOfExistingNos,Integer maxNo){
        Double random = Math.random();
        Integer randomNo = Math.round(random *maxNo);
        if(setOfExistingNos.contains(randomNo)){
            randomNo = generateRandonNo(setOfExistingNos,maxNo);
        }
        return randomNo;
    }


  
}




//LMS_Quiz_Controller.initiateQuiz('a1h0v0000012kb4AAA');
//System.debug(LMS_Quiz_Controller.getQuizResponses('a1h0v0000012kb4AAA'));