/**
 * @description       : 
 * @author            : Manish Sogani
 * @group             : 
 * @last modified on  : 12-31-2020
 * @last modified by  : Manish Sogani
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   12-31-2020   Manish Sogani   Initial Version
**/
public with sharing class LMS_MyDashboardEventsCtrl {
    
    public static final String CONTAINER_UPCOMING = 'upcoming';
    public static final String CONTAINER_REGISTERED = 'registered';

    @AuraEnabled
    public static List<EventsWrapper> fetchEventsWrapper(){
        try{
            List<EventsWrapper> lstEventsWrapper = new List<EventsWrapper>();
            
            User currentUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

            if(currentUser != null){
                lstEventsWrapper.add(new EventsWrapper(CONTAINER_UPCOMING, 2));
                lstEventsWrapper.add(new EventsWrapper(CONTAINER_REGISTERED, 3));

                if(!lstEventsWrapper.isEmpty() && lstEventsWrapper.size() > 0){
                    return lstEventsWrapper;
                }
            }
            
            return null;
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
    }

    public class EventsWrapper{
        @AuraEnabled public String eventType;
        @AuraEnabled public Integer eventCount;
        @AuraEnabled public String eventLabel;
        @AuraEnabled public String eventURL;
        public EventsWrapper(String eventType, Integer eventCount){
            this.eventType = eventType;
            this.eventCount = eventCount;

            String siteBaseURL = Site.getPathPrefix();
            if(eventType == CONTAINER_UPCOMING){
                eventLabel = 'Upcoming Events';
                eventURL = siteBaseURL + '/upcoming-events';

            }else if(eventType == CONTAINER_REGISTERED){
                eventLabel = 'Registered Events';
                eventURL = siteBaseURL + '/registered-events';

            }
        }
    }

    //Prepare SOQL query based on Object Name
    public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Schema.sObjectType objType = globalDescription.get(objName);
        Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
        Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
}