public with sharing class LMS_BookmarkManagerController {
    public LMS_BookmarkManagerController() {}

    @AuraEnabled
    public static Map<String, Object> fetchBookmarks( String recordId ){
        Map<String, Object> responses = new Map<String, Object>();
        responses.put('disableAddBookmark', true);
        responses.put('currentCourseBookmarked', false);
        responses.put('pathPrefix', Site.getPathPrefix());
        try {
            User usr = [SELECT Id, ContactId FROM User WHERE Id=:UserInfo.getUserId()];
            
            if( String.isNotBlank( recordId ) ){
                Id idValue = Id.valueOf( recordId );
                String objectAPIName = idValue.getSObjectType().getDescribe().getName();
                if( objectAPIName == 'Course_Catalogue__c'){
                    responses.put('disableAddBookmark', false);
                    list<Course_Bookmarks__c> bookmarks = [SELECT Id FROM Course_Bookmarks__c 
                                                            WHERE Course_Catalogue__c=:idValue AND Contact__c=:usr.ContactId 
                                                            limit 1];
                    if( bookmarks.size() > 0 ){
                        responses.put('currentCourseBookmarked', true);
                    }
                }
            }

            responses.put('bookmarks', [SELECT Id, Course_Catalogue__c, Course_Catalogue__r.Name FROM Course_Bookmarks__c
                                        WHERE Contact__c=:usr.ContactId]);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return responses;
    }

    @AuraEnabled
    public static Map<String, Object> toggleCourseBookmark( String recordId, Boolean bookmarkedState ){
        try {
            User usr = [SELECT Id, ContactId FROM User WHERE Id=:UserInfo.getUserId()];
            if( bookmarkedState != null && !bookmarkedState ){
                insert new Course_Bookmarks__c( Contact__c=usr.ContactId, Course_Catalogue__c=recordId);
            }else{
                list<Course_Bookmarks__c> bookmarks = [SELECT Id FROM Course_Bookmarks__c 
                                                        WHERE Course_Catalogue__c=:recordId AND Contact__c=:usr.ContactId 
                                                        limit 1];
                
                if( bookmarks.size() > 0 ){
                    delete bookmarks;
                }
            }

            return fetchBookmarks( recordId );
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}