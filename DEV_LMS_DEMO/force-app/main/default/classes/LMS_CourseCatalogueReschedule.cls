public without sharing class LMS_CourseCatalogueReschedule {

    @AuraEnabled
        public static List<Course_Schedule__c> getCourseSchedule(String id) {
            List<Course_Schedule__c> courseList = new List<Course_Schedule__c>();
            try{
                courseList = [SELECT Id,Start_Date__c,Status__c,Course_Catalogue__c,Name FROM Course_Schedule__c WHERE Course_Catalogue__c =: id ORDER BY Name ASC];
            } catch(exception e) {
                throw new AuraHandledException('Error  ---> '+e.getMessage());
            }
            return courseList;
        }

        @AuraEnabled
        public static List<Course_Catalogue__c> getCourseCatelouge() {
            List<Course_Catalogue__c> courseList = new List<Course_Catalogue__c>();
            try{
                courseList = [SELECT Id,Name FROM Course_Catalogue__c];
            } catch(exception e) {
                throw new AuraHandledException('Error  ---> '+e.getMessage());
            }
            return courseList;
        }

    @AuraEnabled
        public static List<User_Enrollment__c> getUserEnrollments(String id) {
            List<User_Enrollment__c> enrollmentList = new List<User_Enrollment__c>();
            try{
                enrollmentList = [SELECT Id,Course_Schedule__c,Name,Enrolled_Date__c,Enrolment_Status__c,Started_Date__c,Status__c,Due_Date__c FROM User_Enrollment__c WHERE Course_Schedule__c =: id ORDER BY Name ASC];
            } catch(exception e) {
                throw new AuraHandledException('Error  ---> '+e.getMessage());
            }
            return enrollmentList;
        }

    @AuraEnabled
        public static String saveEnrollment(String id,Date startDate) {
            try{
                User_Enrollment__c u = new User_Enrollment__c();
                User_Enrollment__c en = [SELECT Id,Course_Schedule__c,Name,Enrolled_Date__c,Enrolment_Status__c,Started_Date__c,Status__c,Due_Date__c FROM User_Enrollment__c WHERE id =: id];
                if(en.Due_Date__c != null && en.Started_Date__c != null) {
                    if(startDate != null) {
                        Integer days = en.Due_Date__c.daysBetween(en.Started_Date__c);
                        if(days <0) {
                            days = days*(-1);
                        }
                        u.Due_Date__c = startDate.addDays(days);
                    }  
                }
                u.Id = id;
                u.Started_Date__c = startDate;
                update u;
                return 'SUCCESS';
            } catch(exception e) {
                throw new AuraHandledException(e.getMessage());
            }
        }

    @AuraEnabled
        public static List<User_Enrollment__c> getUserEnrollments1(List<String> id) {
            List<User_Enrollment__c> enrollmentList = new List<User_Enrollment__c>();
            try{
                enrollmentList = [SELECT Id,Contact__r.Name,Course_Schedule__c,Course_Schedule__r.OJT_Training__c,Name,Enrolled_Date__c,Enrolment_Status__c,Started_Date__c,Status__c,Due_Date__c, Evaluation_Submitted__c, isConfirmedEnrollmentStatus__c FROM User_Enrollment__c WHERE Course_Schedule__c IN: id ORDER BY Name ASC];
            } catch(exception e) {
                throw new AuraHandledException('Error  ---> '+e.getMessage());
            }
            return enrollmentList;
        }

    @AuraEnabled
        public static void deleteEnrolledUser(String recordIdForDelete) {
            List<User_Enrollment__c> enrollmentList = new List<User_Enrollment__c>();
            try{
                enrollmentList = [SELECT Id FROM User_Enrollment__c WHERE Id = :recordIdForDelete LIMIT 1];
                delete enrollmentList;
            } catch(exception e) {
                throw new AuraHandledException('Error  ---> '+e.getMessage());
            }
        }
    
    @AuraEnabled
    public static void updateWaitListStatus(String courseScheduleId, String userEnrollId){
        try {
            Course_Schedule__c courseSchedule = [SELECT Id, Total_Waitlist__c
                                                 FROM Course_Schedule__c
                                                 WHERE Id = :courseScheduleId
                                                 LIMIT 1].get(0);

            if(courseSchedule.Total_Waitlist__c > 0) {
                List<User_Enrollment__c> userEnrollList = new List<User_Enrollment__c>();

                User_Enrollment__c getCurrentUser = [SELECT Id, Waitlist_Number__c, Enrolment_Status__c
                                                     FROM User_Enrollment__c
                                                     WHERE Id = :userEnrollId
                                                     LIMIT 1].get(0);
                getCurrentUser.Waitlist_Number__c = courseSchedule.Total_Waitlist__c;
                getCurrentUser.Enrolment_Status__c = 'Waitlist';
                userEnrollList.add(getCurrentUser);
                
                List<User_Enrollment__c> getOtherUsers = new List<User_Enrollment__c>();
                for(User_Enrollment__c enrolledUser : [SELECT Id, Waitlist_Number__c, Enrolment_Status__c
                                                       FROM User_Enrollment__c
                                                       WHERE Id != :userEnrollId
                                                       AND Course_Schedule__c = :courseScheduleId]) {
                    if(enrolledUser.Waitlist_Number__c != null && enrolledUser.Waitlist_Number__c != 0) {
                        enrolledUser.Waitlist_Number__c = enrolledUser.Waitlist_Number__c - 1;
                    }
                    if(enrolledUser.Waitlist_Number__c == 0) {
                        enrolledUser.Enrolment_Status__c = 'Confirmed';
                    }
                    userEnrollList.add(enrolledUser);
                }
                update userEnrollList;                                    
            }                                     
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static List<Course_Catalogue__c> enrollLearner( String courseId, String contact, String courseScheduleId){
        try {

            Course_Schedule__c currentSchedule;
            Set<String> setTimeRangeErroredRecordIds = new Set<String>();
            for(Course_Schedule__c cs : [SELECT Id,Start_Date__c,End_Date__c 
                                        FROM Course_Schedule__c 
                                        WHERE Id =:courseScheduleId  ]){
                                            currentSchedule = cs;

            }
            Set<Course_Schedule__c> existingCourseSchduleRecords = new Set<Course_Schedule__c>();
            for(User_Enrollment__c userEnrolment : [SELECT Id , Course_Schedule__r.Start_Date__c,Course_Schedule__r.End_Date__c
                                                    FROM User_Enrollment__c
                                                    WHERE Contact__c = :contact ]){
                                                        existingCourseSchduleRecords.add(userEnrolment.Course_Schedule__r); 
                                                    }
            for(Course_Schedule__c ecs : existingCourseSchduleRecords){
                if((currentSchedule.Start_Date__c > ecs.Start_Date__c && currentSchedule.Start_Date__c < ecs.End_Date__c) || 
                    (currentSchedule.End_Date__c > ecs.Start_Date__c && currentSchedule.End_Date__c < ecs.End_Date__c) || 
                    (ecs.Start_Date__c > currentSchedule.Start_Date__c && ecs.End_Date__c < currentSchedule.End_Date__c)){
                        setTimeRangeErroredRecordIds.add(currentSchedule.Id);
                    }
            } 
            if(setTimeRangeErroredRecordIds.size() > 0){
                return null;
            }

            Set<Id> prerequisiteCourseIdSet = new Set<Id>();
            for(Course_Relation__c cr : [SELECT Id, Related_Course__c
                                         FROM Course_Relation__c
                                         WHERE Course_Catalogue__c = :courseId
                                         AND Type__c = 'Prerequisite']) {
                                            prerequisiteCourseIdSet.add(cr.Related_Course__c);
                                         }
            System.debug('>>> 1 '+prerequisiteCourseIdSet);                             
            Map<Id, Course_Catalogue__c> courseCatalogueMap = new Map<Id, Course_Catalogue__c>();
            for(Course_Catalogue__c cc : [SELECT Id,Name,Roles__c,Status__c,Published_Date__c,Last_Updated_Date__c,Rating__c,Total_Enrollment__c 
                                          FROM Course_Catalogue__c 
                                          WHERE Id IN :prerequisiteCourseIdSet
                                          ORDER by Name]) {
                                            courseCatalogueMap.put(cc.Id, cc);
                                          }   
            System.debug('>>> 2 '+courseCatalogueMap);                                                      
            Map<Id, Course_Catalogue__c> scheduleToCourseMap = new Map<Id, Course_Catalogue__c>();
            for(Course_Schedule__c cs : [SELECT Id, Course_Catalogue__c
                                         FROM Course_Schedule__c
                                         WHERE Course_Catalogue__c IN :prerequisiteCourseIdSet]) {
                                            scheduleToCourseMap.put(cs.Id, courseCatalogueMap.get(cs.Course_Catalogue__c));
                                         }
            System.debug('>>> 3 '+scheduleToCourseMap);  
            Set<Id> courseIdSet = new Set<Id>();                            
            List<Course_Catalogue__c> coursesToCompleteList = new List<Course_Catalogue__c>();                         
            for(User_Enrollment__c enrolledUser : [SELECT Status__c, Course_Schedule__c
                                                   FROM User_Enrollment__c
                                                   WHERE Contact__c = :contact
                                                   AND Course_Schedule__c IN :scheduleToCourseMap.keySet()]) {
                                                       
                                                       if(enrolledUser.Status__c != 'Completed') {
                                                           if(!(courseIdSet.contains(scheduleToCourseMap.get(enrolledUser.Course_Schedule__c).Id))) {
                                                                coursesToCompleteList.add(scheduleToCourseMap.get(enrolledUser.Course_Schedule__c));
                                                                courseIdSet.add(scheduleToCourseMap.get(enrolledUser.Course_Schedule__c).Id);    
                                                           }
                                                       }
                                                       courseCatalogueMap.remove(scheduleToCourseMap.get(enrolledUser.Course_Schedule__c).Id); 
                                                   }
            System.debug('>>> 4 '+coursesToCompleteList);                                        
            for(Id courseCatalogueId : courseCatalogueMap.keySet()) {
                coursesToCompleteList.add(courseCatalogueMap.get(courseCatalogueId));
            }
            System.debug('>>> 5 '+coursesToCompleteList);    
            if(coursesToCompleteList.size() == 0) {
                Course_Schedule__c courseSchedule = [SELECT Id, Course_Catalogue__c, Available_Seats__c, Total_Confirmed__c, Start_Date__c 
                                                 FROM Course_Schedule__c
                                                 WHERE Id = :courseScheduleId
                                                 LIMIT 1].get(0);
                                                 
                DateTime startDateTime = courseSchedule.Start_Date__c;    
                Date startDate = date.newinstance(startDateTime.year(), startDateTime.month(), startDateTime.day());
                startDate = (startDate < System.today()) ? System.today() :  startDate;     

                User_Enrollment__c enrollUser = new User_Enrollment__c();
            
                enrollUser.Contact__c = contact;
                enrollUser.Course_Schedule__c = courseScheduleId;
                enrollUser.Started_Date__c =  startDate;
                enrollUser.Due_Date__c = startDate.addDays(10);
                if(courseSchedule.Available_Seats__c >= courseSchedule.Total_Confirmed__c) {
                    enrollUser.Enrolment_Status__c = 'Confirmed';
                } else {
                    enrollUser.Enrolment_Status__c = 'Waitlist';
                }
                insert enrollUser;

                Id courseCatalogueId = courseSchedule.Course_Catalogue__c;
                List<Course_Module__c> courseModuleList = [SELECT Id
                                                        FROM Course_Module__c
                                                        WHERE Course_Catalogue__c = :courseCatalogueId];

                List<User_Module_Association__c> userModuleAssociationList = new List<User_Module_Association__c>();                                           
                for(Course_Module__c courseModule : courseModuleList) {
                    User_Module_Association__c uma = new User_Module_Association__c();
                    uma.User_Enrollment__c = enrollUser.Id;
                    uma.Course_Module__c = courseModule.Id;
                    uma.Status__c = 'Not Started';
                    userModuleAssociationList.add(uma);
                }    
                insert userModuleAssociationList;     
            }             
            return coursesToCompleteList;                                                           
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String enrolledUserOnReservedSeats(Id courseScheduleId, Id accountId, Integer seats){

        List<Course_Schedule__c> courseScheduleList = new List<Course_Schedule__c>();
        List<User_Enrollment__c> userEnrollmentToInsert = new List<User_Enrollment__c>();
        Id courseCatalogueId;
        List<Course_Schedule__c> tempList = [SELECT Course_Catalogue__c
                                            FROM Course_Schedule__c
                                            WHERE Id =: courseScheduleId];
        if(!tempList.isEmpty()){
            courseCatalogueId = tempList.get(0).Course_Catalogue__c;
        }

        courseScheduleList = [SELECT Available_Seats__c
                              FROM Course_Schedule__c
                              WHERE ID =:courseScheduleId];
        Decimal availableSeats = courseScheduleList.get(0).Available_Seats__c;
        if(availableSeats < seats){
            return 'Only ' + String.valueOf(availableSeats) +' seats are available.';
        }

        else{
            List<Contact>defaultContactList = [SELECT Id,AccountId,Default_Contact__c
                                               FROM Contact
                                               WHERE AccountId =:accountId
                                               AND Default_Contact__c = true];

            for(Integer i=0; i<seats; i++){
                User_Enrollment__c uEnroll = new User_Enrollment__c();
                if(!defaultContactList.isEmpty()){
                    uEnroll.Contact__c = defaultContactList.get(0).Id;
                }
                    uEnroll.Course_Schedule__c = courseScheduleId;
                    uEnroll.Status__c = 'Not Started';
                    uEnroll.Enrolment_Status__c = 'Confirmed';
                userEnrollmentToInsert.add(uEnroll);
            }
            if(!userEnrollmentToInsert.isEmpty()){
                insert userEnrollmentToInsert;
                List<Course_Module__c> courseModuleList = [SELECT Id
                                                            FROM Course_Module__c
                                                            WHERE Course_Catalogue__c = :courseCatalogueId];

                List<User_Module_Association__c> userModuleAssociationList = new List<User_Module_Association__c>();  
                for(User_Enrollment__c uEnroll:userEnrollmentToInsert) {
                    for(Course_Module__c courseModule : courseModuleList) {
                    User_Module_Association__c uma = new User_Module_Association__c();
                    uma.User_Enrollment__c = uEnroll.Id;
                    uma.Course_Module__c = courseModule.Id;
                    uma.Status__c = 'Not Started';
                    userModuleAssociationList.add(uma);
                    }
            }    
                if(!userModuleAssociationList.isEmpty()){
                    insert userModuleAssociationList;
                    return 'Success';  
                }
                else {
                    throw new AuraHandledException('Insert Failed');
                }
                   
                
                }     
                
            
            else{
                throw new AuraHandledException('Insert Failed');
            }
        }
    }
    @AuraEnabled
    public static List<User_Enrollment__c> getReservedEnrolledUser(Id courseScheduleId, Id accountId){
        Id contactId;
        List<Contact>defaultContactList = [SELECT Id,AccountId,Default_Contact__c
                                               FROM Contact
                                               WHERE AccountId =:accountId
                                               AND Default_Contact__c = true];
        if(!defaultContactList.isEmpty()){
             contactId = defaultContactList.get(0).Id;
        }

        List<User_Enrollment__c> userEnrolledList = [SELECT Contact__r.Name, Contact__c
                                                    FROM User_Enrollment__c
                                                    WHERE Contact__c =:contactId
                                                    AND Course_Schedule__c =:courseScheduleId];

        return userEnrolledList;

    }

    @AuraEnabled
    public static void updateEnrollmentRecord(Map<Id,User_Enrollment__c>userEnrollMap){
        List<User_Enrollment__c> usr = userEnrollMap.values();

        if(!usr.isEmpty()){
            update usr;
            
        }
        else{
            throw new AuraHandledException('Update Failed');
        }
        
    }
}