/**
 * @description       : 
 * @author            : Manish Sogani
 * @group             : 
 * @last modified on  : 12-31-2020
 * @last modified by  : Manish Sogani
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   12-31-2020   Manish Sogani   Initial Version
**/
public with sharing class LMS_AnnouncementsCtrl {
    
    @AuraEnabled
    public static List<AnnouncementsWrapper> fetchAnnouncementsWrapper(){
        try {
            List<AnnouncementsWrapper> lstAnnouncementsWrapper = new List<AnnouncementsWrapper>();

            Set<String> setAdditionalFields = new Set<String>();
            setAdditionalFields.add('Id');
            
            String soql = '';
            soql = prepareQuery('Announcement__c', setAdditionalFields, true);
            soql += ' WHERE Date__c <= Today AND Expiry_Date__c >= Today AND Status__c = \'Published\'';
            
            User usr = [SELECT Id, ContactId FROM User WHERE Id=:UserInfo.getUserId()];

            set<Id> courseIds = new set<Id>();
            for( User_Enrollment__c userEnrollment : [SELECT Id, Course_Schedule__r.Course_Catalogue__c 
                                                        FROM User_Enrollment__c 
                                                        WHERE Contact__c=:usr.contactId AND Course_Schedule__c != null] ){
                courseIds.add( userEnrollment.Course_Schedule__r.Course_Catalogue__c );
            }
            if( !courseIds.isEmpty() ){
                soql += ' AND ( Type__c =\'General\' OR Course_Catalogue__c IN:courseIds ) ';
            }else{
                soql += ' AND Type__c =\'General\' ';
            }

            soql += ' ORDER BY Date__c ';

            for(Announcement__c ann : database.query(soql)){
                lstAnnouncementsWrapper.add(new AnnouncementsWrapper(ann));
            }

            if(!lstAnnouncementsWrapper.isEmpty() && lstAnnouncementsWrapper.size() > 0){
                return lstAnnouncementsWrapper;
            }
            return null;
        } catch (Exception ex) {
            throw new LMSException( ex.getMessage() );
        }
    }

    public class AnnouncementsWrapper{
        @AuraEnabled public Announcement__c ann;
        @AuraEnabled public Boolean isGeneralNotification;
        @AuraEnabled public Boolean isCourseNotification;
        public AnnouncementsWrapper(Announcement__c ann){
            this.ann = ann;
            isGeneralNotification = false;
            isCourseNotification = false;
            if(ann.Type__c == 'General'){
                isGeneralNotification = true;
            }else if(ann.Type__c == 'Course'){
                isCourseNotification = true;
            }
        }
    }

    //Prepare SOQL query based on Object Name
    public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Schema.sObjectType objType = globalDescription.get(objName);
        Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
        Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
}