/**
 * @description       : 
 * @author            : Manish Sogani
 * @group             : 
 * @last modified on  : 12-30-2020
 * @last modified by  : Manish Sogani
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   12-30-2020   Manish Sogani   Initial Version
**/
public with sharing class LMS_AllCoursesCtrl {
  
    @AuraEnabled
    public static List<CourseCatalogWrapper> loadAllCourses(Boolean isFromSearch, String searchedValue){
        try{
            List<CourseCatalogWrapper> lstCourseCatalogWrapper = new List<CourseCatalogWrapper>();
            Map<Id,String> catalogToSessionTypeMap = new Map<Id,String>();

            User currentUser = [SELECT Id, ContactId, Contact.Role__c, Contact.Is_Admin__c FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
                
            if(currentUser != null){
                if(!currentUser.Contact.Is_Admin__c){
                    if(currentUser.Contact.Role__c == null){
                        currentUser.Contact.Role__c = '';
                        throw new LMSException( 'No role assigned. Please contact system admin.' ); 
                    }
                }
                
                Set<String> setCSAdditionalFields = new Set<String>();
                setCSAdditionalFields.add('Id');
                
                String cssoql = '';
                cssoql = prepareQuery('Course_Schedule__c', setCSAdditionalFields, true);
                cssoql += ' WHERE Status__c = \'Published\' AND (End_Date__c >= Today OR End_Date__c = null ) ';
                cssoql += ' ORDER BY Course_Catalogue__r.Name ';
                Set<String> courseCatalogueIds = new Set<String>();
                for(Course_Schedule__c cs : database.query(cssoql)){
                    courseCatalogueIds.add(cs.Course_Catalogue__c);
                    catalogToSessionTypeMap.put(cs.Course_Catalogue__c,cs.Session_Type__c);
                }
                if(courseCatalogueIds.size() > 0){
                    Set<String> setCCAdditionalFields = new Set<String>();
                    setCCAdditionalFields.add('Id');
                    
                    String ccsoql = '';
                    ccsoql = prepareQuery('Course_Catalogue__c', setCCAdditionalFields, true);
                    ccsoql += ' WHERE Id IN :courseCatalogueIds ';
                    if(!currentUser.Contact.Is_Admin__c){
                        ccsoql += ' AND Roles__c includes (\'' + String.escapeSingleQuotes(currentUser.Contact.Role__c) + '\')';
                    }
                    if(isFromSearch){
                        ccsoql += ' AND Name Like \'%' + String.escapeSingleQuotes(searchedValue) + '%\'';
                    }
                    ccsoql += ' ORDER By Name ';
                    for(Course_Catalogue__c cc : database.query(ccsoql)){
                        lstCourseCatalogWrapper.add(new CourseCatalogWrapper(cc,catalogToSessionTypeMap.get(cc.Id)));
                    }
                    if(!lstCourseCatalogWrapper.isEmpty() && lstCourseCatalogWrapper.size() > 0){
                        return lstCourseCatalogWrapper;
                    }
                }
            }
            return null;

        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
    }

    public class CourseCatalogWrapper{
        @AuraEnabled public Course_Catalogue__c courseCatalog;
        @AuraEnabled public String courseCatalogUrl;
        @AuraEnabled public String sessionType;
        public CourseCatalogWrapper(Course_Catalogue__c courseCatalog,String sessionType ){
            this.courseCatalog = courseCatalog;
            this.sessionType = sessionType;
            courseCatalogUrl = Site.getPathPrefix() + '/course-catalogue/' + courseCatalog.Id + '/' + courseCatalog.Name;
        }
    }

    //Prepare SOQL query based on Object Name
    public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Schema.sObjectType objType = globalDescription.get(objName);
        Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
        Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
}