public with sharing class lmsCourseManagementController {
    @AuraEnabled
    public static List<CourseCatalogWrapper>  getCourseDetails(){
        List<CourseCatalogWrapper> lstCourseCatalogWrapper = new List<CourseCatalogWrapper>();
        for(Course_Catalogue__c course: [SELECT Id,Name,Roles__c,Status__c,
                                              Published_Date__c,Last_Updated_Date__c,Rating__c,
                                              Total_Enrollment__c 
                                            FROM Course_Catalogue__c order by Name]){
                                                CourseCatalogWrapper courseCatalogRecord = New CourseCatalogWrapper(course);
                                                courseCatalogRecord.courseCatalog = course;
                                                lstCourseCatalogWrapper.add(courseCatalogRecord);
                                            }
                                            return lstCourseCatalogWrapper;
                                            
    }

    public class CourseCatalogWrapper{
        @AuraEnabled public Course_Catalogue__c courseCatalog;
        @AuraEnabled public String courseCatalogUrl;
        public CourseCatalogWrapper(Course_Catalogue__c courseCatalog){
            this.courseCatalog = courseCatalog;
            courseCatalogUrl = Site.getPathPrefix() + '/course-catalogue/' + courseCatalog.Id + '/' + courseCatalog.Name;
        }
    }
}