public without sharing class LMS_CourseDetailsController {

    private static User currentUser;

    @AuraEnabled
    public static CourseWrapper fetchCourseDetails(String courseId){
        try {
            currentUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId()];

            List<Course_Catalogue__c> courseList = [
                SELECT Id, Status__c, Name, Description__c, Roles__c, Objective__c, 
                Summary__c, (SELECT Id, Available_Seats__c, Total_Confirmed__c, Require_Supervisor_Approval__c 
                             FROM Course_Schedules__r WHERE Session_Type__c = 'Instructor' LIMIT 1)
                FROM Course_Catalogue__c 
                WHERE Id = :courseId
            ];

            if( courseList.isEmpty() ) {
                throw new LMS_Exception('No Course found.');
            }

            List<User_Enrollment__c> enrollmentsList = [
                SELECT Id, Enrolment_Status__c, Status__c, Course_Schedule__c, Course_Updated__c, Course_Schedule__r.Course_Catalogue__c, 
                    Course_Schedule__r.Course_Catalogue__r.Name,  Course_Schedule__r.Course_Catalogue__r.Description__c, 
                    Course_Schedule__r.Course_Catalogue__r.Roles__c, Course_Schedule__r.Course_Catalogue__r.Objective__c,
                    Course_Schedule__r.Course_Catalogue__r.Summary__c, Course_Schedule__r.Session_Type__c, Contact__r.Name, Completed_Date__c
                FROM User_Enrollment__c 
                WHERE Contact__c = :currentUser.ContactId 
                AND Course_Schedule__r.Course_Catalogue__c = :courseList[0].Id
            ];

            CourseWrapper course;

            if( enrollmentsList.isEmpty() ) {

                course = new CourseWrapper( courseList[0] );

                for(Course_Module__c module : [
                    SELECT Id, Module_Name__c,Type__c
                    FROM Course_Module__c
                    WHERE Course_Catalogue__c = :courseId
                    AND Status__c = 'Published'
                    ORDER BY Sequence__c
                ]) {
                    course.modulesList.add( new ModuleWrapper( module ) );
                }

            } else {

                course = new CourseWrapper( enrollmentsList[0] );

                // Course Modules
                for(User_Module_Association__c association : [
                    SELECT Id, User_Enrollment__c, Status__c, Course_Module__c,Course_Module__r.Type__c,
                    Course_Module__r.Video_URL__c,Course_Module__r.SCORM_URL__c,
                     Course_Module__r.Module_Name__c,
                        Completed_Date__c
                    FROM User_Module_Association__c
                    WHERE User_Enrollment__c = :enrollmentsList[0].Id
                    ORDER BY Course_Module__r.Sequence__c
                ]) {
                    if( association.Status__c == 'Completed' ) {
                        course.completedModules++;
                    }
                    course.modulesList.add( new ModuleWrapper( association ) );
                }
            }
            course.totalModules = course.modulesList.size();

            //START - Logic for Submit For Approval
            course.isShowSubmitForApprovalButton = false;
            if(course.isOnJobTraining){
                if(course.totalModules != 0 && course.totalModules == course.completedModules){
                    course.isShowSubmitForApprovalButton = true;
                }
            }
            //END - Logic for Submit For Approval

            // Course Resources
            for(Course_Reference__c resource : [
                SELECT Id, Name, Course_Catalogue__c, Type__c, URL__c
                FROM Course_Reference__c
                WHERE Course_Catalogue__c = :courseList[0].Id
            ]) {
                ResourceWrapper resourceWrp = new ResourceWrapper( resource );
                resourceWrp.disableActions = !course.isEnrolledIntoCourse;
                course.resourcesList.add( resourceWrp );
            }

            return course;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Integer fetchLaunchDetails(String courseId){
        Set<Id> moduleIdSet = new Map<Id, Course_Module__c>([SELECT Id FROM Course_Module__c WHERE Course_Catalogue__c=:courseId]).keySet();
        
        Integer launchCount = 0;

        if(moduleIdSet.size() > 0) {
            for(User_Module_Association__c uma : [SELECT Number_of_Launches__c 
                                                    FROM User_Module_Association__c 
                                                    WHERE Course_Module__c IN : moduleIdSet ]) {
                if(uma.Number_of_Launches__c == null) {
                    uma.Number_of_Launches__c = 0;
                }
                launchCount = launchCount + Integer.valueOf(uma.Number_of_Launches__c);
            }
        }
        return launchCount;                                                
    }

    @AuraEnabled
    public static String fetchHelpText(String courseId){
        Course_Catalogue__c courseCatalogue = [SELECT Help_Text__c FROM Course_Catalogue__c WHERE Id=:courseId LIMIT 1];
        return String.valueOf(courseCatalogue.Help_Text__c);
    }

    @AuraEnabled
    public static void doSubmitForApproval(String courseId){
        try {
            User currentUser = [SELECT Id, ContactId, Contact.Role__c, Contact.Is_Admin__c FROM User 
                                WHERE Id = :UserInfo.getUserId() LIMIT 1];
            
            if(currentUser != null){
                User_Enrollment__c usrEnroll = [SELECT Id FROM User_Enrollment__c 
                                                WHERE Contact__c = :currentUser.ContactId AND 
                                                Course_Schedule__r.Course_Catalogue__c = :courseId LIMIT 1];
                usrEnroll.Status__c = 'Pending Approval';
                update usrEnroll;
            }
        }catch (Exception e) {
            throw new LMSException(e.getMessage());
        }
    }

    public class CourseWrapper {
        @AuraEnabled public String userEnrollmentId;
        @AuraEnabled public String roles;
        @AuraEnabled public String courseStatus;
        @AuraEnabled public String courseScheduleId;
        @AuraEnabled public String courseId;
        @AuraEnabled public String courseTitle;
        @AuraEnabled public String courseDescription;
        @AuraEnabled public String courseSummary;
        @AuraEnabled public String courseObjective;
        @AuraEnabled public String contactName;
        @AuraEnabled public Date completedDate;
        @AuraEnabled public Boolean courseIsCompleted;
        @AuraEnabled public Boolean isEnrolledIntoCourse;
        @AuraEnabled public Boolean isDropFromCourse;
        @AuraEnabled public List<ModuleWrapper> modulesList;
        @AuraEnabled public Integer totalModules;
        @AuraEnabled public Integer completedModules;
        @AuraEnabled public Boolean isShowSubmitForApprovalButton;
        @AuraEnabled public Boolean isOnJobTraining;
        @AuraEnabled public List<ResourceWrapper> resourcesList;
        @AuraEnabled public Boolean isShowModules; 

        public CourseWrapper(User_Enrollment__c enrollment) {
            this.userEnrollmentId = enrollment.Id;
            this.courseStatus = enrollment.Enrolment_Status__c;
            this.courseScheduleId = enrollment.Course_Schedule__c;
            this.courseId = enrollment.Course_Schedule__r?.Course_Catalogue__c;
            this.courseTitle = enrollment.Course_Schedule__r?.Course_Catalogue__r.Name;
            this.courseDescription = enrollment.Course_Schedule__r?.Course_Catalogue__r.Description__c;
            this.courseSummary = enrollment.Course_Schedule__r?.Course_Catalogue__r.Description__c;
            this.courseObjective = enrollment.Course_Schedule__r?.Course_Catalogue__r.Objective__c;
            this.contactName = enrollment.Contact__r?.Name;
            this.completedDate = enrollment.Completed_Date__c;
            this.roles = enrollment.Course_Schedule__r.Course_Catalogue__r.Roles__c;
            this.courseIsCompleted = enrollment.Status__c == 'Completed'; 
            this.isEnrolledIntoCourse = false;
            this.isDropFromCourse = false;
            if(enrollment.Course_Schedule__c != null && enrollment.Course_Schedule__r.Session_Type__c == 'Instructor'){
                if(!this.courseIsCompleted){
                    this.isDropFromCourse = true;
                }
            }

            this.isOnJobTraining = false;
            if(enrollment.Course_Schedule__c != null && enrollment.Course_Schedule__r.Session_Type__c == 'On Job Training'){
                this.isOnJobTraining = true;
            }
            this.modulesList = new List<ModuleWrapper>();
            this.totalModules = 0;
            this.completedModules = 0;
            this.resourcesList = new List<ResourceWrapper>();
            isShowModules = false;
            if(enrollment.Enrolment_Status__c == 'Confirmed'){
                isShowModules = true;
            }            
        }

        public CourseWrapper(Course_Catalogue__c course) {
            this.courseStatus = 'Not Enrolled';
            this.courseId = course.Id;
            this.courseTitle = course.Name;
            this.courseDescription = course.Description__c;
            this.courseSummary = course.Description__c;
            this.courseObjective = course.Objective__c;
            this.roles = course.Roles__c;
            this.courseIsCompleted = false; 
            this.isEnrolledIntoCourse = false;
            this.isDropFromCourse = false;
            if(!course.Course_Schedules__r.isEmpty() && course.Course_Schedules__r.size() > 0){
                this.isEnrolledIntoCourse = true;
            }
            this.isOnJobTraining = false;
            this.modulesList = new List<ModuleWrapper>();
            this.totalModules = 0;
            this.completedModules = 0;
            this.resourcesList = new List<ResourceWrapper>();
            isShowModules = true;            
        }
    }

    public class ModuleWrapper {
        @AuraEnabled public String umaId;
        @AuraEnabled public String moduleType;
        @AuraEnabled public String moduleId;
        @AuraEnabled public String moduleVideoUrl;
        @AuraEnabled public String userEnrollmentId;
        @AuraEnabled public String moduleName;
        @AuraEnabled public String moduleStatus;
        @AuraEnabled public Boolean moduleIsCompleted;
        @AuraEnabled public Date completionDate;
        @AuraEnabled public Boolean disableActions;
        @AuraEnabled public String scormURL;
        

        public ModuleWrapper(User_Module_Association__c uma) {
            this.umaId = uma.Id;
            this.moduleId = uma.Course_Module__c;
            this.userEnrollmentId = uma.User_Enrollment__c;
            this.moduleName = uma.Course_Module__r.Module_Name__c;
            this.moduleStatus = uma.Status__c;
            this.completionDate = uma.Completed_Date__c;
            this.moduleIsCompleted = uma.Status__c == 'Completed'; 
            this.disableActions = false;
            this.moduleType = uma.Course_Module__r.Type__c;
            this.moduleVideoUrl = uma.Course_Module__r.Video_URL__c;
            this.scormURL = uma.Course_Module__r.SCORM_URL__c;
        }

        public ModuleWrapper(Course_Module__c module) {
            this.moduleId = module.Id;
            this.moduleName = module.Module_Name__c;
            this.moduleStatus = 'Not Started';
            this.disableActions = true;
            this.moduleType = module.Type__c;
            

        }
    }

    public class ResourceWrapper {
        @AuraEnabled public String recourceId;
        @AuraEnabled public String resourceName;
        @AuraEnabled public String courseId;
        @AuraEnabled public String resourceType;
        @AuraEnabled public String resourceUrl;
        @AuraEnabled public Boolean isHyperlink;
        @AuraEnabled public Boolean isFile;
        @AuraEnabled public Boolean disableActions;

        public ResourceWrapper(Course_Reference__c resource) {
            this.recourceId = resource.Id;
            this.resourceName = resource.Name;
            this.courseId = resource.Course_Catalogue__c;
            this.resourceType = resource.Type__c;
            this.resourceUrl = resource.URL__c;
            this.isHyperlink = resource.Type__c == 'Hyperlink';
            this.isFile = resource.Type__c == 'File';
        }
    }

    public class LMSException extends Exception{
        
    }
}