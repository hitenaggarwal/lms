public class LMS_CourseScheduleTriggerHandler {
    
    public static void beforeInsert(List<Course_Schedule__c> newList){
        preventConflict(newList, null);
    }
    
    public static void beforeUpdate(List<Course_Schedule__c> newList, Map<Id, Course_Schedule__c> oldMap){
        preventConflict(newList, oldMap);
    }

    public static void afterUpdate(List<Course_Schedule__c> newList, Map<Id, Course_Schedule__c> oldMap){
        createAutoEnrollRecords(newList, oldMap);
    }
    
    public static void preventConflict(List<Course_Schedule__c> newList, Map<Id, Course_Schedule__c> oldMap){
        Set<String> setInstructorIds = new Set<String>();
        for(Course_Schedule__c cs : newList){
            if(cs.Instructor__c != null){
                if( (oldMap == null || oldMap.get(cs.Id).Instructor__c != cs.Instructor__c)  && cs.Instructor__c != null ){
                    setInstructorIds.add(cs.Instructor__c);
                }
            }
        }
        if(setInstructorIds.size() > 0){
            Set<Course_Schedule__c> existingCourseSchduleRecords = new Set<Course_Schedule__c>();
            for(Course_Schedule__c cs : [SELECT Id, Start_Date__c, End_Date__c FROM Course_Schedule__c WHERE Instructor__c IN :setInstructorIds]){
                existingCourseSchduleRecords.add(cs);
            }
            if(!existingCourseSchduleRecords.isEmpty() && existingCourseSchduleRecords.size() > 0){
                Set<String> setTimeRangeErroredRecordIds = new Set<String>();
                for(Course_Schedule__c cs : newList){
                    if(cs.Start_Date__c < System.now()){
                        cs.addError('Please ensure that the Start Date/Time is greater than Current Date/Time.');
                    }else if(cs.Start_Date__c > cs.End_Date__c){
                        cs.addError('Please ensure that the End Date/Time is greater than or equal to the Start Date/Time.');
                    }else{
                        for(Course_Schedule__c ecs : existingCourseSchduleRecords){
                            if((cs.Start_Date__c > ecs.Start_Date__c && cs.Start_Date__c < ecs.End_Date__c) || 
                               (cs.End_Date__c > ecs.Start_Date__c && cs.End_Date__c < ecs.End_Date__c) || 
                               (ecs.Start_Date__c > cs.Start_Date__c && ecs.End_Date__c < cs.End_Date__c)){
                                   setTimeRangeErroredRecordIds.add(cs.Id);
                               }
                        } 
                    }
                }
                if(setTimeRangeErroredRecordIds.size() > 0){
                    for(Course_Schedule__c cs : newList){
                        if(setTimeRangeErroredRecordIds.contains(cs.Id)){
                            cs.addError('Not Allowed. Already course schdeule record(s) exist in given date/time range for the instructor.');
                        }
                    }
                }
            }
        }
    }


    public static void createAutoEnrollRecords(List<Course_Schedule__c> newList, Map<Id, Course_Schedule__c> oldMap){
        
        Id courseCatalogueId;
        List<Course_Schedule__c>autoEnrollCourseScheduleList = new List<Course_Schedule__c>();
        List<String>courseRoleList = new  List<String>();
        for(Course_Schedule__c cs:newList){
            if(cs.Auto_Enroll__c == true){
                autoEnrollCourseScheduleList.add(cs);
                String s = cs.Course_Catalogue__r.Roles__c;
                List<String> temp = s.split(';');
                courseRoleList.addALL(temp);
                courseCatalogueId = cs.Course_Catalogue__c;
            }
        }
        System.assert(false,courseRoleList);
        if(!autoEnrollCourseScheduleList.isEmpty()){
            List<Contact> contactList = new List<Contact>();
            contactList = [SELECT Id
                        FROM Contact
                        WHERE Role__c IN: courseRoleList];

        List<User_Enrollment__c>userEnrollmentToInsert = new List<User_Enrollment__c>();

        for(Course_Schedule__c cs:newList){
            for(Contact con:contactList){
                User_Enrollment__c ue = new User_Enrollment__c();
                ue.Contact__c = con.Id;
                ue.Course_Schedule__c = cs.Id;
                ue.Status__c = 'Not Started';
                userEnrollmentToInsert.add(ue);
            }
        }
        if(!userEnrollmentToInsert.isEmpty()){
            insert userEnrollmentToInsert;
        }
        List<Course_Module__c> courseModuleList = [SELECT Id
                                                    FROM Course_Module__c
                                                    WHERE Course_Catalogue__c = :courseCatalogueId];

        List<User_Module_Association__c> userModuleAssociationList = new List<User_Module_Association__c>();
       
        for(User_Enrollment__c uEnroll : userEnrollmentToInsert){
            for(Course_Module__c module :courseModuleList){
                User_Module_Association__c userModule = new User_Module_Association__c();
                userModule.Course_Module__c = module.Id;
                userModule.User_Enrollment__c = uEnroll.Id;
                userModule.Status__c = 'Not Started';
                userModuleAssociationList.add(userModule);
            }
        }
        if(!userModuleAssociationList.isEmpty()){
            insert userModuleAssociationList;
        }
       

    }

    }
    
}