public without sharing class LMS_ManageTaskComponentController {

    @AuraEnabled
    public static List<User_Enrollment__c> getAllUserEnrollment(Id recordId){
        
        List<User_Enrollment__c>userEnrollmentList = [SELECT Course_Schedule__r.Session_Type__c,Status__c,
                                                      Contact__r.Name,Due_Date__c,Started_Date__c,Enrolment_Status__c
                                                      FROM User_Enrollment__c
                                                      WHERE Course_Schedule__c =:recordId];
        
        return userEnrollmentList;
    }

    @AuraEnabled
    public static List<User_Module_Association__c> getUserModuleAssociation(Id recordId){
        List<User_Module_Association__c>userModuleList = [SELECT Started_Date__c,Completed_Date__c,Due_Date__c,Course_Module__r.Module_Name__c,
                                                        Sequence__c,Status__c, Course_Module__r.Course_Catalogue__r.Name,Is_Completed__c
                                                        FROM User_Module_Association__c
                                                        WHERE Type__c = 'Performance Task'
                                                        AND User_Enrollment__c =: recordId
                                                        ORDER BY Sequence__c  ASC];
        return userModuleList;
    }


    @AuraEnabled
    public static void updateUserModuleRecord(Map<Id,User_Module_Association__c> updatedMap){
        List<User_Module_Association__c> userModuleToUpdate = updatedMap.values();                        
        if(!userModuleToUpdate.isEmpty()){
            update userModuleToUpdate;
        }
        else{
            throw new AuraHandledException('Update Failed, Please Try Again!!');
        }

    }


    @AuraEnabled
    public static void deleteUserModuleRecord(Id recordId){
        List<User_Module_Association__c>ulist = new List<User_Module_Association__c>();

        ulist = [SELECT ID
                FROM User_Module_Association__c
                WHERE ID =: recordId];

        if(!ulist.isEmpty()){
            delete ulist;
        }
        else{
            throw new AuraHandledException('Deletion Failed, Please Try Again!!');
        }
    }


}