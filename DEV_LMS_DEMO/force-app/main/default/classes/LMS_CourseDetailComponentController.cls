public without sharing class LMS_CourseDetailComponentController {
    
    @AuraEnabled
    public static void updateOverviewField(String recordId, String summary){
        Course_Catalogue__c course = new Course_Catalogue__c();
        course.Id = recordId;
        course.Summary__c = summary;
        update course;
    }

    @AuraEnabled
    public static void updateObjectiveField(String recordId, String objective){
        Course_Catalogue__c course = new Course_Catalogue__c();
        course.Id = recordId;
        course.Objective__c = objective;
        update course;
    }

    @AuraEnabled
    public static List<Course_Module__c> getAllModules(String recordId){
    List<Course_Module__c>moduleList = new List<Course_Module__c>();
    moduleList = [SELECT Module_Name__c,Status__c,Type__c,Max_Attempts__c,
                 Passing_Score__c,Sequence__c
                 FROM Course_Module__c
                 WHERE Course_Catalogue__c =:recordId];
    
    return moduleList;
 }

 @AuraEnabled
 public static List<User_Enrollment__c> getCourseEnrollment(String recordId){
 List<User_Enrollment__c>enrollmentList = new List<User_Enrollment__c>();
 enrollmentList = [SELECT Course_Schedule__r.Start_Date__c , Course_Schedule__r.End_Date__c ,
                Contact__r.Name, Evaluation_Submitted__c
                FROM  User_Enrollment__c
                WHERE Course_Schedule__r.Course_Catalogue__c =:recordId ];
 
 return enrollmentList;
}



}