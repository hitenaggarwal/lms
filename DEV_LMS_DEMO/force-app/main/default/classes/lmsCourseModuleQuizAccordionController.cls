public without sharing class lmsCourseModuleQuizAccordionController {

    @AuraEnabled
    public static  List<ModuleWrapper>  courseModuleQuiz(String recordId){
    Map<Id, List<User_Module_Association__c>> moduleToUserAssociation = new Map<Id, List<User_Module_Association__c>>();

        List<Course_Module__c>courseModuleList = [SELECT Id,Module_Name__c, Type__c
                                                  FROM Course_Module__c
                                                  WHERE Type__c = 'Quiz'
                                                 AND Course_Catalogue__c =: recordId];

        for(User_Module_Association__c usr: [SELECT Quiz_Result__c,Quiz_Score__c,User_Score__c,Course_Module__r.Passing_Score__c,User_Enrollment__r.Contact__r.Name
                                              FROM User_Module_Association__c
                                              WHERE Course_Module__c IN : courseModuleList]){

                                                if(!moduleToUserAssociation.containsKey(usr.Course_Module__c)){
                                                  moduleToUserAssociation.put(usr.Course_Module__c, new List<User_Module_Association__c>());
                                                }
                                                moduleToUserAssociation.get(usr.Course_Module__c).add(usr);

                                                
                                              }

        List<ModuleWrapper> responseWrapperList = new  List<ModuleWrapper>();

        for(Course_Module__c cm :courseModuleList ){
          ModuleWrapper mw = new ModuleWrapper();
          mw.courseModule = cm;
          List<User_Module_Association__c> userModuleList = new  List<User_Module_Association__c>();
          for(User_Module_Association__c uma :moduleToUserAssociation.get(cm.Id) ){
            userModuleList.add(uma);
          }
          mw.userModuleList = userModuleList;

          responseWrapperList.add(mw);
          
        }
       
        return responseWrapperList;
            
    }


    public class ModuleWrapper{
      
      @AuraEnabled public Course_Module__c courseModule;
      @AuraEnabled public List<User_Module_Association__c> userModuleList;
      
    }


}