public without sharing class LMS_AttendanceManagerController {
    public LMS_AttendanceManagerController() {

    }

    @AuraEnabled(Cacheable=true)
    public static Map<String, Object> getCourseCatalogues(){
        Map<String, Object> response = new Map<String, Object>();
        try {
            list<Option> courseCatalogueOptions = new list<Option>();
            // More filters will be added later to this query.
            for( Course_Catalogue__c courseCatalogue : [SELECT Id, Name FROM Course_Catalogue__c] ){
                courseCatalogueOptions.add( new Option( courseCatalogue.Name, courseCatalogue.Id ) );
            }
            response.put( 'courseCatalogueOptions', courseCatalogueOptions );

            List<Schema.PicklistEntry> ple = User_Course_Attendance__c.Attendance__c.getDescribe().getPickListValues();
            List<Option> attendanceOptions = new List<Option>();
            for( Schema.PicklistEntry pickListVal : ple){
                attendanceOptions.add(new Option(pickListVal.getLabel(),pickListVal.getValue()));
            }  
            response.put('attendanceOptions',attendanceOptions); 
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }

    @AuraEnabled(Cacheable=true)
    public static Map<String, Object> getCourseSchedules( String courseCatalogueId ){
        Map<String, Object> response = new Map<String, Object>();
        try {
            list<Option> courseScheduleOptions = new list<Option>();
            // More filters will be added later to this query.
            for( Course_Schedule__c courseSchedule : [SELECT Id, Name FROM Course_Schedule__c WHERE Course_Catalogue__c=:courseCatalogueId] ){
                courseScheduleOptions.add( new Option( courseSchedule.Name, courseSchedule.Id ) );
            }
            response.put( 'courseScheduleOptions', courseScheduleOptions );
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> getCourseEnrollments( String courseScheduleId, Date attendanceDate ){
        Map<String, Object> response = new Map<String, Object>();
        try {

            if( attendanceDate == null ){
                attendanceDate = Date.Today();
            }

            list<UserEnrollment> courseEnrollments = new list<UserEnrollment>();
            // More filters will be added later to this query.
            for( User_Enrollment__c userEnrollment : [SELECT Id, Name, Contact__c, Contact__r.Name, Status__c, 
                                                        ( SELECT Id, Name, Attendance__c, Date__c, User_Enrollment__c 
                                                            FROM User_Course_Attendance__r
                                                            WHERE Date__c=:attendanceDate limit 1) 
                                                        FROM User_Enrollment__c 
                                                        WHERE Course_Schedule__c=:courseScheduleId 
                                                        AND Completed_Date__c = null AND Contact__c != null] ){
                User_Course_Attendance__c userAttendance;
                if( userEnrollment.User_Course_Attendance__r == null || userEnrollment.User_Course_Attendance__r.size() == 0 ){
                    userAttendance = new User_Course_Attendance__c( Date__c = Date.Today(), User_Enrollment__c=userEnrollment.Id, Attendance__c='' );
                }else{
                    userAttendance = userEnrollment.User_Course_Attendance__r.get(0);
                }
                courseEnrollments.add( new UserEnrollment( userEnrollment.Id, userEnrollment.Contact__r.Name, userEnrollment.Status__c, userAttendance ) );
            }
            response.put( 'courseEnrollments', courseEnrollments );

            System.debug('Attendance: '+[SELECT Id, Name, Attendance__c, Date__c, User_Enrollment__c 
            FROM User_Course_Attendance__c
            WHERE Date__c=TODAY]);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> saveAttendance( String jsonUserEnrollments){
        Map<String, Object> response = new Map<String, Object>();
        try {
            list<UserEnrollment> Wrappers = (list<UserEnrollment>) JSON.deserialize(jsonUserEnrollments, list<UserEnrollment>.class);
            
            list<User_Course_Attendance__c> attendance = new list<User_Course_Attendance__c>();
            for( UserEnrollment wrapper : Wrappers ){
                System.debug('userAttendance.Attendance__c: '+wrapper.userAttendance.Attendance__c);
                attendance.add( wrapper.userAttendance );
            }

            upsert attendance;
            response.put( 'courseEnrollments', Wrappers );
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return response;
    }

    public class Option{
        @AuraEnabled public String label;
        @AuraEnabled public String value;

        public Option( String label, String value ){
            this.label = label;
            this.value = value;
        }
    }

    public class UserEnrollment{
        @AuraEnabled public String enrollmentId;
        @AuraEnabled public String contactName;
        @AuraEnabled public String enrollmentStatus;
        @AuraEnabled public User_Course_Attendance__c userAttendance;
        
        public UserEnrollment( String enrollmentId, String contactName, String enrollmentStatus, User_Course_Attendance__c userAttendance ){
            this.enrollmentId = enrollmentId;
            this.contactName = contactName;
            this.enrollmentStatus = enrollmentStatus;
            this.userAttendance = userAttendance;
        } 
    } 
}