/**
 * @description       : 
 * @author            : Manish Sogani
 * @group             : 
 * @last modified on  : 12-31-2020
 * @last modified by  : Manish Sogani
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   12-31-2020   Manish Sogani   Initial Version
**/
public with sharing class LMS_MyDashboardCoursesCtrl {

    @AuraEnabled
    public static List<CoursesWrapper> fetchCoursesWrapper(){
        try{
            List<CoursesWrapper> lstCoursesWrapper = new List<CoursesWrapper>();
            
            User currentUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];

            Map<String, List<User_Enrollment__c>> mapCourseStatusAndListUserEnrollment = new Map<String, List<User_Enrollment__c>>();
            mapCourseStatusAndListUserEnrollment.put(LMS_Constants.CONTAINER_ACTIVE, new List<User_Enrollment__c>());
            mapCourseStatusAndListUserEnrollment.put(LMS_Constants.CONTAINER_UPDATED, new List<User_Enrollment__c>());
            mapCourseStatusAndListUserEnrollment.put(LMS_Constants.CONTAINER_COMPLETED, new List<User_Enrollment__c>());
            mapCourseStatusAndListUserEnrollment.put(LMS_Constants.CONTAINER_OVERDUE, new List<User_Enrollment__c>());
            
            if(currentUser != null && currentUser.ContactId != null){
                Set<String> setUEAdditionalFields = new Set<String>();
                setUEAdditionalFields.add('Id');
                String uesoql = '';
                uesoql = prepareQuery('User_Enrollment__c', setUEAdditionalFields, true);
                uesoql += ' WHERE Contact__c = \'' + currentUser.ContactId + '\'';

                for(User_Enrollment__c enrollment : database.query(uesoql)) {
                    String courseStatus = null;
                    if(enrollment.Status__c == 'Not Started' || enrollment.Status__c == 'In progress') {
                        courseStatus = LMS_Constants.CONTAINER_ACTIVE;
                        mapCourseStatusAndListUserEnrollment.get(courseStatus).add(enrollment);
                        if(enrollment.Due_Date__c < Date.today()){
                            mapCourseStatusAndListUserEnrollment.get(LMS_Constants.CONTAINER_OVERDUE).add(enrollment);
                        }
                    }
                    if(enrollment.Course_Updated__c == true) {
                        courseStatus = LMS_Constants.CONTAINER_UPDATED;
                        mapCourseStatusAndListUserEnrollment.get(courseStatus).add(enrollment);
                    }
                    if(enrollment.Status__c == 'Completed') {
                        courseStatus = LMS_Constants.CONTAINER_COMPLETED;
                        mapCourseStatusAndListUserEnrollment.get(courseStatus).add(enrollment);
                    }
                }
            }
            
            for(String cs : mapCourseStatusAndListUserEnrollment.keySet()){
                List<User_Enrollment__c> lstUserEnroll = mapCourseStatusAndListUserEnrollment.get(cs);
                lstCoursesWrapper.add(new CoursesWrapper(cs, lstUserEnroll.size()));
            }
            if(!lstCoursesWrapper.isEmpty() && lstCoursesWrapper.size() > 0){
                return lstCoursesWrapper;
            }

            
            return null;
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
    }

    public class CoursesWrapper{
        @AuraEnabled public String courseStatus;
        @AuraEnabled public Integer courseCount;
        @AuraEnabled public String courseLabel;
        @AuraEnabled public String courseURL;
        public CoursesWrapper(String courseStatus, Integer courseCount){
            this.courseStatus = courseStatus;
            this.courseCount = courseCount;

            String siteBaseURL = Site.getPathPrefix();
            if(courseStatus == LMS_Constants.CONTAINER_ACTIVE){
                courseLabel = 'Active Courses';
                courseURL = siteBaseURL + '/active-courses';

            }else if(courseStatus == LMS_Constants.CONTAINER_UPDATED){
                courseLabel = 'Updated Courses';
                courseURL = siteBaseURL + '/updated-courses';

            }else if(courseStatus == LMS_Constants.CONTAINER_COMPLETED){
                courseLabel = 'Completed Courses';
                courseURL = siteBaseURL + '/completed-courses';
            }else if(courseStatus == LMS_Constants.CONTAINER_OVERDUE){
                courseLabel = 'Overdue Courses';
                courseURL = siteBaseURL + '/overdue-courses';
            }
        }
    }

    //Prepare SOQL query based on Object Name
    public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Schema.sObjectType objType = globalDescription.get(objName);
        Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
        Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
}