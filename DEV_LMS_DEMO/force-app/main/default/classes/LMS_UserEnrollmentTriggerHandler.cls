public class LMS_UserEnrollmentTriggerHandler {
    public static void updateConfirmedAndWaitlistEnroll(List<User_Enrollment__c> newList, Map<Id, User_Enrollment__c> oldMap) {
        Map<String, Integer> scheduleToConfirmedUserEnrolledMap = new Map<String, Integer>();
        Map<String, Integer> scheduleToWaitlistUserEnrolledMap = new Map<String, Integer>();
        if(newList != NULL) {
            for(User_Enrollment__c userEnroll : newList) {
                if(oldMap == NULL || ((userEnroll.Course_Schedule__c != oldMap.get(userEnroll.Id).Course_Schedule__c) || 
                                      (userEnroll.Enrolment_Status__c != oldMap.get(userEnroll.Id).Enrolment_Status__c))) {
                        if(userEnroll.Course_Schedule__c != NULL) {
                            scheduleToConfirmedUserEnrolledMap.put(userEnroll.Course_Schedule__c, 0);
                            scheduleToWaitlistUserEnrolledMap.put(userEnroll.Course_Schedule__c, 0);
                        }
                        if(oldMap!=NULL && oldMap.get(userEnroll.Id).Course_Schedule__c != NULL) {
                            scheduleToConfirmedUserEnrolledMap.put(oldMap.get(userEnroll.Id).Course_Schedule__c, 0);
                            scheduleToWaitlistUserEnrolledMap.put(oldMap.get(userEnroll.Id).Course_Schedule__c, 0);
                        }
                    }
            }
        }
        if(newList == NULL && oldMap!=NULL) {
            for(Id userEnrollId : oldMap.keySet()) {
                if(oldMap.get(userEnrollId).Course_Schedule__c != NULL) {
                    scheduleToConfirmedUserEnrolledMap.put(oldMap.get(userEnrollId).Course_Schedule__c, 0);
                    scheduleToWaitlistUserEnrolledMap.put(oldMap.get(userEnrollId).Course_Schedule__c, 0);
                }
            }
        }
        
        for(AggregateResult result : [SELECT Count(Id), Course_Schedule__c
                                      FROM User_Enrollment__c
                                      WHERE Course_Schedule__c IN :scheduleToConfirmedUserEnrolledMap.keySet()
                                      AND Enrolment_Status__c = 'Confirmed'
                                      GROUP BY Course_Schedule__c]) {
                                          scheduleToConfirmedUserEnrolledMap.put(String.valueOf(result.get('Course_Schedule__c')), Integer.valueOf(result.get('expr0')));
                                          
                                      } 
        
        for(AggregateResult result : [SELECT Count(Id), Course_Schedule__c
                                      FROM User_Enrollment__c
                                      WHERE Course_Schedule__c IN :scheduleToWaitlistUserEnrolledMap.keySet()
                                      AND Enrolment_Status__c = 'Waitlist'
                                      GROUP BY Course_Schedule__c]) {
                                          scheduleToWaitlistUserEnrolledMap.put(String.valueOf(result.get('Course_Schedule__c')), Integer.valueOf(result.get('expr0')));
                                      }                               
        Map<Id, Course_Schedule__c> confirmCourseScheduleMap = new Map<Id, Course_Schedule__c>();
        for(Course_Schedule__c courseSchedule : [SELECT Id, Available_Seats__c, Total_Confirmed__c, Total_Waitlist__c, Instructor__c
                                                 FROM Course_Schedule__c
                                                 WHERE Id IN :scheduleToConfirmedUserEnrolledMap.keySet()]) {
                                                     confirmCourseScheduleMap.put(courseSchedule.Id, courseSchedule);
                                                 }
        Map<Id, Course_Schedule__c> waitlistCourseScheduleMap = new Map<Id, Course_Schedule__c>();
        for(Course_Schedule__c courseSchedule : [SELECT Id, Available_Seats__c, Total_Confirmed__c, Total_Waitlist__c, Instructor__c
                                                 FROM Course_Schedule__c
                                                 WHERE Id IN :scheduleToWaitlistUserEnrolledMap.keySet()]) {
                                                     waitlistCourseScheduleMap.put(courseSchedule.Id, courseSchedule);
                                                 }
        List<Course_Schedule__c> courseScheduleList = new List<Course_Schedule__c>();
        Map<Id, Course_Schedule__c> courseScheduleMap = new Map<Id, Course_Schedule__c>();
        for(Id courseScheduleId : scheduleToConfirmedUserEnrolledMap.keySet()) {
            Course_Schedule__c courseSchedule = new Course_Schedule__c();
            courseSchedule.Id = courseScheduleId;
            
            Integer totalCount = scheduleToConfirmedUserEnrolledMap.get(courseScheduleId);
            Course_Schedule__c existingCourseSchedule = confirmCourseScheduleMap.get(courseScheduleId);
            courseSchedule.Instructor__c = existingCourseSchedule.Instructor__c;
            if(existingCourseSchedule.Available_Seats__c != null) {
                if(existingCourseSchedule.Available_Seats__c == existingCourseSchedule.Total_Confirmed__c) {
                    if(totalCount - existingCourseSchedule.Available_Seats__c > 0) {
                        courseSchedule.Total_Waitlist__c = totalCount - existingCourseSchedule.Available_Seats__c;
                    } else {
                        courseSchedule.Total_Waitlist__c = 0;
                        courseSchedule.Total_Confirmed__c = totalCount;
                    }
                } else {
                    if(existingCourseSchedule.Available_Seats__c < totalCount) {
                        courseSchedule.Total_Confirmed__c = existingCourseSchedule.Available_Seats__c;
                        courseSchedule.Total_Waitlist__c = totalCount - existingCourseSchedule.Available_Seats__c;
                    } else {
                        courseSchedule.Total_Confirmed__c = totalCount;
                    }
                }
                courseScheduleList.add(courseSchedule);
                courseScheduleMap.put(courseSchedule.Id, courseSchedule);
            }
            
        }
        update courseScheduleList;
        List<Course_Schedule__c> newCourseScheduleList = new List<Course_Schedule__c>();
        for(Id courseScheduleId : scheduleToWaitlistUserEnrolledMap.keySet()) {
            Course_Schedule__c courseSchedule = new Course_Schedule__c();
            if(!(courseScheduleMap.containsKey(courseScheduleId))) {
                courseSchedule.Id = courseScheduleId;
            } else {
                courseSchedule = courseScheduleMap.get(courseScheduleId);
            }
            
            Integer totalCount = scheduleToWaitlistUserEnrolledMap.get(courseScheduleId);
            Course_Schedule__c existingCourseSchedule = waitlistCourseScheduleMap.get(courseScheduleId);
            courseSchedule.Instructor__c = existingCourseSchedule.Instructor__c;
            if(courseSchedule.Total_Waitlist__c != null) {
                courseSchedule.Total_Waitlist__c =  courseSchedule.Total_Waitlist__c + totalCount;
                newCourseScheduleList.add(courseSchedule);
            }
            
        }
        update newCourseScheduleList;
    }
    
    // If User Enrolmnet Status is completed , get all course whcih are completed by users and sum up their duration in minute and add them to the contact record. 
   
    public static void updateEnrolledContactTrainingDuration(List<User_Enrollment__c> newList, Map<Id, User_Enrollment__c> oldMap){
        Set<Id> contactIds = new Set<Id>();
        Map<Id,Integer> contactIdAndTrainingMinutes = new Map<Id,Integer>();
        List<Contact> contactList = new List<Contact>();
        
        if(newList == null && oldMap != null){
            for(User_Enrollment__c usrEnroll : oldMap.values()){
                if((usrEnroll.Status__c == 'Completed')){
                    contactIds.add(usrEnroll.Contact__c);
            	}
        	}
        }
        if(newList !=null){
            for(User_Enrollment__c usrEnroll : newList){
                if((oldMap == null && usrEnroll.Status__c == 'Completed') || (oldMap != null && usrEnroll.Status__c == 'Completed' && oldMap.get(usrEnroll.Id).Status__c != 'Completed') ||  (oldMap != null && usrEnroll.Course_Schedule__c != oldMap.get(usrEnroll.Id).Course_Schedule__c && oldMap.get(usrEnroll.Id).Status__c == 'Completed')){
                    contactIds.add(usrEnroll.Contact__c);
            	}
        	}
        }

            
            for(User_Enrollment__c usrEnrol: [SELECT Contact__c, Course_Schedule__r.Course_Catalogue__r.Duration_In_Minutes__c
                                              FROM User_Enrollment__c 
                                              WHERE Contact__c IN : contactIds
                                              AND Course_Schedule__c != null 
                                              AND Course_Schedule__r.Course_Catalogue__r.Duration_In_Minutes__c != null]){
                                                  if(!contactIdAndTrainingMinutes.containsKey(usrEnrol.Contact__c)){
                                                      contactIdAndTrainingMinutes.put(usrEnrol.Contact__c, 0);                                                      
                                                  }
                                                  contactIdAndTrainingMinutes.put(usrEnrol.Contact__c,contactIdAndTrainingMinutes.get(usrEnrol.Contact__c)+Integer.valueOf(usrEnrol.Course_Schedule__r.Course_Catalogue__r.Duration_In_Minutes__c));             				
            }
            
            if(!contactIdAndTrainingMinutes.isEmpty()){
               
                for(Id con : contactIdAndTrainingMinutes.keySet()){
                    Contact cont = new Contact();
                    cont.Id = con;
                    cont.Training_Time_In_Minutes__c  = contactIdAndTrainingMinutes.get(con);
                    contactList.add(cont);
                }
            }
            if(!contactList.isEmpty()){
                update contactList;
            }
        
    } 
    
    public static void preventConflict(List<User_Enrollment__c> newList, Map<Id, User_Enrollment__c> oldMap) {
        //Set<Id> userEnrollIdSet = new Set<Id>();
        Map<Id, Id> userEnrollMap = new Map<Id, Id>();
        for(User_Enrollment__c userEnroll : newList) {
            if(userEnroll.Contact__c != null) {
                if(oldMap == NULL || (userEnroll.Course_Schedule__c != oldMap.get(userEnroll.Id).Course_Schedule__c)) {
                    //userEnrollIdSet.add(userEnroll.Contact__c);
                    userEnrollMap.put(userEnroll.Contact__c, userEnroll.Course_Schedule__c);
                } 
            }
        }
        Map<Id, List<String>> userToExistingSchedulesMap = new Map<Id, List<String>>();
        for(User_Enrollment__c enrolledUser : [SELECT Id, Course_Schedule__c
                                               FROM User_Enrollment__c
                                               WHERE Contact__c IN :userEnrollMap.keySet()]) {
                                                   List<String> csList = new List<String>();
                                                   if(userToExistingSchedulesMap.containsKey(enrolledUser.Id)) {
                                                       csList = userToExistingSchedulesMap.get(enrolledUser.Id);
                                                   }
                                                   csList.add(enrolledUser.Course_Schedule__c);
                                                   userToExistingSchedulesMap.put(enrolledUser.Id, csList);
                                               }
        Set<Id> courseScheduleIds = new Set<Id>(userEnrollMap.values());
        /*Map<Id, Course_Schedule__c> contactToCourseScheduleMap = new Map<Id, Course_Schedule__c>();
        for(Course_Schedule__c cs : [SELECT Id, Start_Date__c, End_Date__c
                                     FROM Course_Schedule__c
                                     WHERE Id IN: courseScheduleIds]) {
                                         contactToCourseScheduleMap.put(cs)
                                     }*/
        
    }
    
    
    
}