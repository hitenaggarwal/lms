public class LMS_CertificateDownloadController {
    public User_Enrollment__c enrollment{get; set;}
    public Boolean cmpLoaded{get; set;}
    
    public LMS_CertificateDownloadController(){
        cmpLoaded = false;
        Id enrollmentId = ApexPages.currentPage().getParameters().get('enrollmentId');
        
        List<User_Enrollment__c> enrollmentsList = [
            SELECT Id, Status__c, Course_Schedule__c, Course_Updated__c, Course_Schedule__r.Course_Catalogue__c, 
                Course_Schedule__r.Course_Catalogue__r.Name,  Course_Schedule__r.Course_Catalogue__r.Description__c, 
                Course_Schedule__r.Course_Catalogue__r.Roles__c, Course_Schedule__r.Course_Catalogue__r.Objective__c,
                Course_Schedule__r.Course_Catalogue__r.Summary__c, Contact__r.Name, Completed_Date__c
            FROM User_Enrollment__c 
            WHERE ID = :enrollmentId
            AND Status__c = 'Completed'
        ];

        if( enrollmentsList.isEmpty() ) {
            throw new LMS_Exception('No completed course found.');
        }

        enrollment = enrollmentsList[0];
        cmpLoaded = true;
    }
}