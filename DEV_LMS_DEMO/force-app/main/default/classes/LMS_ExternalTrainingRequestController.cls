public with sharing class LMS_ExternalTrainingRequestController {
    public class TrainingRequestWrapper {
        @AuraEnabled public String recordId;
        @AuraEnabled public String trainingName;
        @AuraEnabled public String businessJustification;
        @AuraEnabled public Date requestDate;
        @AuraEnabled public String vendor;
        @AuraEnabled public Decimal cost;
        @AuraEnabled public String status;
    }
    
    @AuraEnabled
    public static List<External_Training_Request__c> getTrainingRequests(){
        Id userId = UserInfo.getUserId();
        List<External_Training_Request__c> trainingRequestList = new List<External_Training_Request__c>();
        for(External_Training_Request__c trainingRequest : [SELECT Id, Name, Business_Justification__c, Date__c, Vendor__c, Cost__c, Status__c
                                                             FROM External_Training_Request__c
                                                             WHERE CreatedById = :userId]) {
                                                                trainingRequestList.add(trainingRequest);
                                                             }
        return trainingRequestList;
    }

    @AuraEnabled
    public static Id createExternalTrainingRequest() {
        External_Training_Request__c trainingReq = new External_Training_Request__c();
        trainingReq.Name = 'ETR';
        insert trainingReq;
        return trainingReq.Id;
    }

    @AuraEnabled
    public static void updateExternalTrainingRequest(String trainingRequest){
        TrainingRequestWrapper trainingRequestWrapper = new TrainingRequestWrapper();
        trainingRequestWrapper = (TrainingRequestWrapper) JSON.deserialize(trainingRequest, TrainingRequestWrapper.class);

        Id userId = UserInfo.getUserId();
        List<User> userList = [Select ContactId from User where Id = :userId];
        Id contactId = userList[0].ContactId;
        
        List<External_Training_Request__c> trainingReqList = new List<External_Training_Request__c>();
        External_Training_Request__c trainingReq = new External_Training_Request__c();
        trainingReq.Id = trainingRequestWrapper.recordId;
        trainingReq.Name = trainingRequestWrapper.trainingName;
        trainingReq.Contact__c = contactId;
        trainingReq.Business_Justification__c = trainingRequestWrapper.businessJustification;
        trainingReq.Date__c = trainingRequestWrapper.requestDate;
        trainingReq.Vendor__c = trainingRequestWrapper.vendor;
        trainingReq.Cost__c = trainingRequestWrapper.cost;
        trainingReq.Status__c = 'Pending';
        trainingReqList.add(trainingReq);
        update trainingReqList;
    }

    @AuraEnabled
    public static void deleteExternalTrainingRequest(Id trainingReqId){
        List<External_Training_Request__c> trainingReqList = [SELECT Id
                                                              FROM External_Training_Request__c
                                                              WHERE Id = :trainingReqId
                                                              LIMIT 1];
        delete trainingReqList;                                                      
    }
   
}