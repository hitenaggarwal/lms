/**
 * @description				: Controller class for lmsSubmitEvaluation (lwc)
 * @author					: Gaurav Suthar
 * @group					: 
 * @created on				: 06 Jan 2021
 * @last modified on		: 06 Jan 2021
 * @last modified by		: Gaurav Suthar
 **/

public without sharing class LMS_Evaluation_Controller {
    public LMS_Evaluation_Controller() {

    }

    @AuraEnabled
    public static String updateUserEnrollment(String userEnrollmentId, String notes, Decimal grades, Boolean status) {
        
        if(userEnrollmentId != null && userEnrollmentId != '' && notes != '') {
            User_Enrollment__c userEnrollment = new User_Enrollment__c();
            userEnrollment.Id = userEnrollmentId;
            userEnrollment.Evaluation_Notes__c = notes;
            userEnrollment.Grade_Percentage__c = grades;
            userEnrollment.Evaluation_Submitted__c = status;
            update userEnrollment;
            return String.valueOf(userEnrollment.Evaluation_Submitted__c);
        }
        else {
            return 'User Enrollment Id is required.';
        }
    }
}