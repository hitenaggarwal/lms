/**
 * @description             : Controller class for lmsNewCourse
 * @author                  : Gaurav Suthar
 * @group                   : group
 * @created on              : 02 Jan 2021
 * @last modified on        : 01-04-2021
 * @last modified by        : Manish Sogani
 **/

public with sharing class LMSCourseCatalogueController {
    
    @AuraEnabled
    public static Course_Catalogue__c fetchCourseCatalogue(String courseCatalogRecordId){
        Course_Catalogue__c ccRecord = new Course_Catalogue__c();
        Set<String> setCCAdditionalFields = new Set<String>();
        setCCAdditionalFields.add('Id');
        setCCAdditionalFields.add('Name');
        setCCAdditionalFields.add('Roles__c');
        setCCAdditionalFields.add('Description__c');
        setCCAdditionalFields.add('Objective__c');
        setCCAdditionalFields.add('Recurring__c');
        setCCAdditionalFields.add('Recurring_Duration_In_Days__c');
        setCCAdditionalFields.add('Reoccurence__c');
        setCCAdditionalFields.add('Recurring_Month__c');
        setCCAdditionalFields.add('Recurring_Year__c');
        setCCAdditionalFields.add('Competency__c');
        setCCAdditionalFields.add('On_Job_Training__c');
        setCCAdditionalFields.add('Require_Supervisor_Approval__c');
        
        try{
            if(String.isNotBlank(courseCatalogRecordId) && String.isNotEmpty(courseCatalogRecordId)){
                
                String ccsoql = '';
                ccsoql = prepareQuery('Course_Catalogue__c', setCCAdditionalFields, false);
                ccsoql += ' WHERE Id = \'' + courseCatalogRecordId + '\'';
                for(Course_Catalogue__c cc : database.query(ccsoql)){
                    ccRecord = cc;
                }
            }
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
        /*for(String s : setCCAdditionalFields){
            if(ccRecord.get(s) == null && s != 'Id'){
                if(s == 'Recurring__c' || s == 'Require_Supervisor_Approval__c'){
                    ccRecord.put(s, false);
                }else{
                    ccRecord.put(s, '');
                }
            }
        }*/
        return ccRecord;
    }

    @AuraEnabled
    public static String createNewCourseRecord(String courseRecord){
        try{
            Course_Catalogue__c ccRecord = (Course_Catalogue__c)JSON.deserialize(courseRecord, Course_Catalogue__c.class);
            upsert ccRecord;
            return ccRecord.Id;
        }catch(Exception ex){
            throw new LMSException( ex.getMessage() );
        }
    }

    //Prepare SOQL query based on Object Name
    public static String prepareQuery(String objName, Set<String> setAdditionalFields, Boolean allFields){
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Schema.sObjectType objType = globalDescription.get(objName);
        Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
        Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  
        Set<String> setFields = new Set<String>();
        setFields = setAdditionalFields;
        
        String strQuery = '';
        strQuery = 'SELECT ';
        if(allFields){
            for(Schema.SObjectField field : mapFieldList.values()){  
                Schema.DescribeFieldResult fieldResult = field.getDescribe(); 
                if(fieldResult.isAccessible()){
                    setFields.add(fieldResult.getName());
                }  
            }
        }
        for(String fAPI : setFields){
            strQuery += fAPI + ', ';
        }
        strQuery = strQuery.substring(0, strQuery.lastIndexOf(','));
        strQuery += ' FROM ' + objName + ' ';
        return strQuery;
    }

    public class LMSException extends Exception{
        
    }
    
}