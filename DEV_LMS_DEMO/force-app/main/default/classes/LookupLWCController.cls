public without sharing class LookupLWCController {
    @AuraEnabled(cacheable=true)
    public static List<Sobject> searchRecords(String searchKey, String objectApiName,List<Id> selectedIds ,Map<String,List<String>> filters){
        String key ='%'+ searchKey + '%';
        
        String query = 'SELECT Id,Name FROM '+ objectApiName+' WHERE Name LIKE :key AND Id NOT IN :selectedIds';
        if(filters != null){
            for(String filterName :filters.keySet()){
                query+=' AND '+ filterName+ ' IN (\''+ String.join(filters.get(filterName), '\',\'') + '\')';
            }
        }
        query +=' LIMIT 10';
        return Database.query(query);
    }
}