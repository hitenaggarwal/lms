public with sharing class LMS_ProfileController {
    @AuraEnabled
    public static InformationWrapper getContact(){
        try{
        Id contactId = [SELECT Id, contactId FROM User WHERE Id =:UserInfo.getUserId()].contactId;
        InformationWrapper wrapper = new InformationWrapper();
        Contact con = [SELECT Id,Birthdate, Email, Total_Training_Hours__c,Role__c, Title, Phone, MailingAddress, FirstName, LastName, Location__c,Department, Supervisor__c FROM Contact WHERE Id = : contactId];
        wrapper.birthDate = con.Birthdate;
        wrapper.location = con.Location__c;
        wrapper.department = con.Department;
        wrapper.supervisor = con.Supervisor__c;
        wrapper.firstName = con.FirstName;
        wrapper.lastName = con.LastName;
        wrapper.email = con.Email;
        wrapper.role = con.Role__c;
        wrapper.title = con.Title;
        //wrapper.mailingAddress = con.MailingAddress;
        wrapper.phone = con.Phone;
        wrapper.trainingDurationInHours = con.Total_Training_Hours__c;
        return wrapper;
    }catch(Exception ex){
        System.debug(ex.getMessage());
        return null;
    }
    
}
    
    public class InformationWrapper{
        
        @AuraEnabled public String location;
        @AuraEnabled public String department;
        @AuraEnabled public String supervisor;
        @AuraEnabled public String firstName;
        @AuraEnabled public String lastName;
        @AuraEnabled public Date birthDate;
        @AuraEnabled public String email;
        @AuraEnabled public String role;
        @AuraEnabled public String title;
        @AuraEnabled public String trainingDurationInHours;
        //@AuraEnabled public Address mailingAddress;
        @AuraEnabled public String phone;
        
    }
}