public without sharing class lmsApprovalsController {

    @AuraEnabled
    public static ApprovalsWrapper getApprovals(){
        try {
            List<String> statusList = new List<String>{'Approved','Pending','Rejected'};
            User user = [SELECT Id,ContactId FROM User WHERE Id =: UserInfo.getUserId()];
            ApprovalsWrapper approvalWrapper = new ApprovalsWrapper();
            approvalWrapper.internalApprovalsList = [SELECT Id,Name,Status__c,Contact__r.Name FROM User_Compliance_Status__c WHERE Approval_Required__c = true AND Status__c IN: statusList AND Contact__r.Supervisor__c =: user.ContactId];
            approvalWrapper.externalApprovalsList = [SELECT Id,Name,Status__c,Contact__r.Name FROM External_Training_Request__c WHERE Status__c IN: statusList AND Contact__r.Supervisor__c =: user.ContactId];
            return approvalWrapper;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void approveRejectApprovals(List<String> internalApprovals, List<String> externalApprovals, Boolean isApprove){
        try {
            List<SObject> approvalsUpdate = new List<SObject>();
            //List<External_Training_Request__c> externalApprovalsUpdate = new List<External_Training_Request__c>();
            if(internalApprovals.size() > 0){
                for(User_Compliance_Status__c ucs : [SELECT Id, Status__c FROM User_Compliance_Status__c WHERE Id IN: internalApprovals]){
                    if(isApprove){
                        ucs.Status__c = 'Approved';
                    }
                    else{
                        ucs.Status__c = 'Rejected';
                    }
                    approvalsUpdate.add(ucs);
                }
            }
            if(externalApprovals.size() > 0){
                for(External_Training_Request__c etr : [SELECT Id, Status__c FROM External_Training_Request__c WHERE Id IN: externalApprovals]){
                    if(isApprove){
                        etr.Status__c = 'Approved';
                    }
                    else{
                        etr.Status__c = 'Rejected';
                    }
                    approvalsUpdate.add(etr);
                }
            }
            if(approvalsUpdate.size()>0)
                update approvalsUpdate;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class ApprovalsWrapper{
        @AuraEnabled public List<User_Compliance_Status__c> internalApprovalsList = new List<User_Compliance_Status__c>();
        @AuraEnabled public List<External_Training_Request__c> externalApprovalsList = new List<External_Training_Request__c>();
    }
}