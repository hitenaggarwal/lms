public without sharing class LMS_UserCertificationController {
    @AuraEnabled
    public static Id getContactId(){
        return [SELECT Id, contactId FROM User WHERE Id =:UserInfo.getUserId()].contactId;
    }

    @AuraEnabled
    public static ProfileWrapper getProfileInfo(Id contactId){
        ProfileWrapper pw = new ProfileWrapper();
        List<Contact> conList = [SELECT Id, About_Me__c
                                 FROM Contact
                                 WHERE Id = :contactId
                                 LIMIT 1]; 
                                   
        List<ContentDocumentLink> cdlList = [SELECT ContentDocumentId, LinkedEntityId
                                            FROM ContentDocumentLink 
                                            WHERE LinkedEntityId = :contactId
                                            AND LinkedEntity.Type='Contact'
                                            ORDER BY Id dESC];
        Id contentDocumentId = cdlList[0].ContentDocumentId;
        List<ContentVersion> cvList = [SELECT Id, FileExtension, Title 
                                       FROM ContentVersion
                                       WHERE ContentDocumentId = :contentDocumentId
                                       LIMIT 1];   
                                        
        pw.aboutMe = conList[0].About_Me__c;
        pw.contentVersionId = cvList[0].Id;
        pw.fileName = cvList[0].Title + '.' + cvList[0].FileExtension;
        return pw;
    }

    @AuraEnabled
    public static void updateAboutMe(Id contactId, String aboutMe){
        Contact con = [SELECT Id, About_Me__c
                       FROM Contact
                       WHERE Id = :contactId];
        con.About_Me__c = aboutMe;
        update con;    
    }

    @AuraEnabled
    public static List<ProfileWrapper> getCertificationsList(){
        Id contactId = [SELECT Id, contactId FROM User WHERE Id =:UserInfo.getUserId()].contactId;
        List<Certification__c> certicationList = [SELECT Id, Name, Certification_Date__c, Issuing_Authority__c
                                                  FROM Certification__c
                                                  WHERE Contact__c = :contactId
                                                  ORDER BY Certification_Date__c DESC];
         
        List<ProfileWrapper> certificationWrapperList = new List<ProfileWrapper>();
        for(Certification__c certification : certicationList) {
            ProfileWrapper cw = new ProfileWrapper();
            cw.certificationId = certification.Id;
            cw.certificationName = certification.Name;
            cw.certificationDate = certification.Certification_Date__c;
            cw.issuingAuthority = certification.Issuing_Authority__c;
            certificationWrapperList.add(cw);
        }      
        return certificationWrapperList;                             
    }

    @AuraEnabled
    public static ProfileWrapper getCertificationDetail(Id certificateId){
        Certification__c certification = [SELECT Id, Name, Certification_Date__c, Issuing_Authority__c
                                                  FROM Certification__c
                                                  WHERE Id = :certificateId
                                                  LIMIT 1].get(0);
         
        ProfileWrapper cw = new ProfileWrapper();
        cw.certificationId = certification.Id;
        cw.certificationName = certification.Name;
        cw.certificationDate = certification.Certification_Date__c;
        cw.issuingAuthority = certification.Issuing_Authority__c;
              
        return cw;                             
    }

    @AuraEnabled
    public static void upsertCertification(String certificationString, Boolean isUpdate){
        Id contactId = [SELECT Id, contactId FROM User WHERE Id =:UserInfo.getUserId()].contactId;
        ProfileWrapper cw = (ProfileWrapper)JSON.deserialize(certificationString, ProfileWrapper.class);
        
        Certification__c certification = new Certification__c();
        certification.Contact__c = contactId;
        certification.Name = cw.certificationName;
        certification.Certification_Date__c = cw.certificationDate;
        certification.Issuing_Authority__c = cw.issuingAuthority;
        if(isUpdate == true) {
            certification.Id = cw.certificationId;
            update certification;
        } else {
            insert certification;
        }
    }

    public class ProfileWrapper {
        @AuraEnabled  public String certificationId {get;set;}
        @AuraEnabled  public String certificationName {get;set;}
        @AuraEnabled  public Date certificationDate {get;set;}
        @AuraEnabled  public String issuingAuthority {get;set;}
        @AuraEnabled  public String aboutMe {get;set;}
        @AuraEnabled  public String contentVersionId {get;set;}
        @AuraEnabled  public String fileName {get;set;}
    }
}