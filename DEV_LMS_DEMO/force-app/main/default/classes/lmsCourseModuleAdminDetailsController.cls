public with sharing class lmsCourseModuleAdminDetailsController {
    @AuraEnabled
    public static List<Course_Module__c>  getCourseDetails(String status){
        return [SELECT Id,Name,Course_Catalogue__r.Name,CreatedBy.Name,Status__c,Module_Name__c
                                              
                                            FROM Course_Module__c 
                                            WHERE Status__c =: status order by Module_Name__c];
                                            
    }
}