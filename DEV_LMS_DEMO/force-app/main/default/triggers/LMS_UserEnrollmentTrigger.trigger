trigger LMS_UserEnrollmentTrigger on User_Enrollment__c (after insert, after update, after delete) {
    //if(Trigger.isInsert || Trigger.isUpdate) {
        LMS_UserEnrollmentTriggerHandler.updateConfirmedAndWaitlistEnroll(Trigger.New, Trigger.OldMap);
    //}
    if(Trigger.isInsert && Trigger.isAfter ){
        LMS_UserEnrollmentTriggerHandler.updateEnrolledContactTrainingDuration(Trigger.New, null);
    }
    if(Trigger.isUpdate && Trigger.isAfter){
        LMS_UserEnrollmentTriggerHandler.updateEnrolledContactTrainingDuration(Trigger.New, Trigger.OldMap);
    }
    if(Trigger.isDelete && Trigger.isAfter){
        LMS_UserEnrollmentTriggerHandler.updateEnrolledContactTrainingDuration(null, Trigger.OldMap);
    }
}