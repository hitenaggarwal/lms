trigger LMS_CourseScheduleTrigger on Course_Schedule__c (after insert, before insert, before update) {
    
    if(Trigger.isBefore && Trigger.isInsert){
        LMS_CourseScheduleTriggerHandler.beforeInsert(trigger.new);
    }
    else if(Trigger.isBefore && Trigger.isUpdate){
        LMS_CourseScheduleTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
    }   
    else if(Trigger.isAfter && Trigger.isInsert){
        LMS_CourseScheduleTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);
    }
}