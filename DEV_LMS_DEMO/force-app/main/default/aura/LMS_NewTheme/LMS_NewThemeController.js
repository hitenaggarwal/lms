({
  gotoHome: function (component, event, helper) {
    window.location.href = './';
    
  },
  doInit: function (component, event, helper) {
    // var action = component.get("c.isAdmin");
    // action.setCallback(this, function (response) {
    //   if (response.getState() == "SUCCESS") {
    //     if (response.getReturnValue() == null) {
    //       component.set('v.isAdmin', false);
    //     }
    //   }
    // });
    // $A.enqueueAction(action);

  },
  afterScriptsLoaded: function (component, event, helper) {
    console.log('Scripts Loaded');
    var action = component.get("c.getFirstTimeUser");
    action.setCallback(this, function (response) {
      if (response.getState() == "SUCCESS") {
        if (response.getReturnValue()) {
          setTimeout(function () { introJs(".introduction-farm").start(); }, 3000);
          var updateAction = component.get("c.updateFirstTimeUser");
          updateAction.setCallback(this, function (response) { });
          $A.enqueueAction(updateAction);
        }
      }
    });
    $A.enqueueAction(action);
  }
})