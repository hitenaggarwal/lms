({
    myAction: function (component, event, helper) {

    },
    navigateToPage: function (component, event, helper) {
        //location.href = './'+event.currentTarget.dataset.id;

        let navService = component.find("navService");

        // Sets the route to [Org url]/[Community uri]/[pageName]
        let pageReference = {

            type: "standard__namedPage", // community page. See https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/components_navigation_page_definitions.htm
            attributes: {
                pageName: event.currentTarget.dataset.id // pageName must be lower case
            }
        }

        navService.navigate(pageReference);

    },
    afterScriptsLoaded: function (component, event, helper) {
        console.log('Scripts Loaded');
        var action = component.get("c.getFirstTimeUser");
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                if (response.getReturnValue()) {
                    setTimeout(function () { introJs(".introduction-farm").start(); }, 3000);
                    var updateAction = component.get("c.updateFirstTimeUser");
                    updateAction.setCallback(this, function (response) { });
                    $A.enqueueAction(updateAction);
                }
            }
        });
        $A.enqueueAction(action);
    },
    navigateToReportsPage: function (component, event, helper) {
        let navService = component.find("navService");

        // Sets the route to [Org url]/[Community uri]/[pageName]
        let pageReference = {
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Report',
                actionName: 'list'
            },
            state: {
                filterName: 'everything'
            }
        }

        navService.navigate(pageReference);
    }


})