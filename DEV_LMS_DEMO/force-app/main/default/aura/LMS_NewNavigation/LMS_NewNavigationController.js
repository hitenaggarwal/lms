({


    doInit: function(component, event, helper) {
        //debugger;
        console.log('---doInit called---');
        var action = component.get("c.isAdmin");
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set('v.isAdmin', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);

        var _menuItems = component.get("v.menuItems");
        console.log('====_menuItems====' + JSON.stringify(_menuItems));
        var i = 0;
        for (i = 0; i < _menuItems.length; i++) {
            if ((_menuItems[i].label) == 'Home') {
                _menuItems[i].iconName = 'icon icon-home';
            } else if ((_menuItems[i].label) == 'Courses') {
                _menuItems[i].iconName = 'icon icon-books-46';
            } else if((_menuItems[i].label) == 'External Training Request'){
                _menuItems[i].iconName = 'icon icon-settings-gear';
            }
            /*else if ((_menuItems[i].label) == 'Hub') {
                _menuItems[i].iconName = 'icon icon-b-chat';
            } else if ((_menuItems[i].label) == 'Shoutout Zone') {
                _menuItems[i].iconName = 'icon icon-megaphone';
            } else if ((_menuItems[i].label) == 'MTX Family') {
                _menuItems[i].iconName = 'icon icon-eco-home';
            } else if ((_menuItems[i].label) == 'Document Center') {
                _menuItems[i].iconName = 'icon icon-archive-paper';
            } else if ((_menuItems[i].label) == 'Gamification') {
                _menuItems[i].iconName = 'icon icon-olympic-flame';
            } else if ((_menuItems[i].label) == 'Store') {
                _menuItems[i].iconName = 'icon icon-shop';
            } else if ((_menuItems[i].label) == 'Admin Settings') {
                _menuItems[i].iconName = 'icon icon-settings-gear';
            } else if ((_menuItems[i].label) == 'Client Success') {
                _menuItems[i].iconName = 'icon icon-users-wm';
            } else if ((_menuItems[i].label) == 'Intake Request') {
                _menuItems[i].iconName = 'icon icon-b-chat';
            } else if ((_menuItems[i].label) == 'Expensify') {
                _menuItems[i].iconName = 'icon icon-b-chat';
            } else if ((_menuItems[i].label) == 'Asset Management') {
                _menuItems[i].iconName = 'icon icon-b-chat';
            } else if ((_menuItems[i].label) == 'Collaboration Hub') {
                _menuItems[i].iconName = 'icon icon-b-chat';
            } else if ((_menuItems[i].label) == 'Help and Support') {
                _menuItems[i].iconName = 'icon icon-users-wm';
            } else if ((_menuItems[i].label) == 'Help & Support') {
                _menuItems[i].iconName = 'icon icon-users-wm';
            } else {
                _menuItems[i].iconName = 'icon icon-users-wm';
            }*/





            /*if((_menuItems[i].label).indexOf('Dashboard') > -1){
               _menuItems[i].iconName = 'index';
            }else if(_menuItems[i].label.indexOf('Collaboration') > -1){
               _menuItems[i].iconName = 'collaboration-hub';
            }else if(_menuItems[i].label.indexOf('Directory') > -1){
               _menuItems[i].iconName = 'company-directory';
            }else if(_menuItems[i].label.indexOf('Document') > -1){
               _menuItems[i].iconName = 'document-management';
            }else if(_menuItems[i].label.indexOf('Calendar') > -1) {
               _menuItems[i].iconName = 'calendar';
            }else if(_menuItems[i].label.indexOf('Team') > -1){
               _menuItems[i].iconName = 'teams';
            }else if(_menuItems[i].label.indexOf('My Learning') > -1){
               _menuItems[i].iconName = 'icon icon-books-46';
            }else{
                _menuItems[i].iconName = 'index';
            }*/
        }
        //component.set("v.menuItems", _menuItems);

        //Seeting the Navigation Menu Item as Active
        let tabNames = {
            Home: 'Home',
            Hub: 'Hub',
            MTX_Family: 'MTX Family',
            Document_Center: 'Document Center',
            My_Learning: 'My Learning',
            Expensify: 'Expensify',
            Gamification: 'Gamification',
            Asset_Management: 'Asset Management',
            Store: 'Store',
            Admin_Settings: 'Admin Settings',
            Client_Success: 'Client Success',
            Intake_Request: 'Intake Request'
        };

        setTimeout(function() {
            let href = window.location.href;
            //Checking for Home tab
            if (href.length - href.lastIndexOf("/connect/s/") == 11) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Home) {
                        item.classList.add('active');
                    }
                })
            }

            //Checking for Hub
            else if (href.indexOf('collaboration-hub') != -1 || href.indexOf('collaboration-groups') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Hub) {
                        item.classList.add('active');
                    }
                })
            }

            //Checking for MTX Family
            else if (href.indexOf('employee-directory') != -1 || href.indexOf('org-chart') != -1 ||
                href.indexOf('client-appreciations') != -1 || href.indexOf('recognition') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.MTX_Family) {
                        item.classList.add('active');
                    }
                })
            }

            //Checking for Document Center
            else if (href.indexOf('policies') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Document_Center) {
                        item.classList.add('active');
                    }
                })
            }

            //Checking for My Learning
            else if (href.indexOf('my-course') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.My_Learning) {
                        item.classList.add('active');
                    }
                })
            }

            //Checking for My Expensify
            else if (href.indexOf('my-course') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Expensify) {
                        item.classList.add('active');
                    }
                })
            }

            //Checking for Asset Management
            else if (href.indexOf('my-course') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Asset_Management) {
                        item.classList.add('active');
                    }
                })
            }

            //Checking for Gamification
            else if (href.indexOf('gamification') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Gamification) {
                        item.classList.add('active');
                    }
                })
            }

            //Checking for Store
            else if (href.indexOf('store') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Store) {
                        item.classList.add('active');
                    }
                })
            }

            //Checking for Admin Settings
            else if (href.indexOf('manage-content') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Admin_Settings) {
                        item.classList.add('active');
                    }
                })
            } else if (href.indexOf('client-success') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Client_Success) {
                        item.classList.add('active');
                    }
                })
            } else if (href.indexOf('intake-request') != -1) {
                let sidebars = document.querySelectorAll('.mxc-theme-item');
                sidebars.forEach(item => {
                    if (item.innerText == tabNames.Intake_Request) {
                        item.classList.add('active');
                    }
                })
            }
        }, 50);
        

    },

    navigateToPage: function(component, event, helper) {
        var id = event.currentTarget.dataset.menuItemId;
        if (id) {
            component.getSuper().navigate(id);
        }
        if(id == 0 || id == 8){
            let sidebars = document.querySelectorAll('.mxc-theme-item');
            sidebars.forEach(item => {
                item.classList.remove('active');
            })
            event.target.classList.add('active');
        }
    },

    handleClick: function(component, event, helper) {
        console.log('--handleClick called---');

        let sidebars = document.querySelectorAll('.mxc-theme-item');
        sidebars.forEach(item => {
            item.classList.remove('active');
        });
        var hasDropdownCls = component.get("v.hasDropdownCls");
        if(hasDropdownCls.indexOf('active') > -1){
            hasDropdownCls = hasDropdownCls.replace("active", "");
        }else{
            hasDropdownCls = hasDropdownCls + ' active';
        }
        component.set("v.hasDropdownCls", hasDropdownCls);

        /*let sidebars = document.querySelectorAll('.has-dropdown');
        let sidebarMain = document.querySelectorAll('.active');
        sidebarMain.forEach(item => {
            item.classList.remove('active');
        })
            
        sidebars.forEach(item => {
            item.classList.remove('active');
        })
        event.target.classList.add('active');*/
    },
    buttonToggle: function(component, event, helper) {
        let sidebars = document.querySelectorAll('.mxc-theme-sidebar');
        sidebars.forEach(sidebar => {
            sidebar.classList.toggle('mxc-theme-sidebar-is-closed')
        })
    }

})