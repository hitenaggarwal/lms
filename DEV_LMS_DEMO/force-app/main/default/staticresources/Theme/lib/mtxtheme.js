// all of theme interactions which require js interaction
window.addEventListener('DOMContentLoaded', () => {
  // TODO remove this delay glitch
  setTimeout(() => {
    // code for enabling sidebar behaviour
    const sidebars = document.querySelectorAll('.mxc-theme-sidebar');

    // do it for all sidebar instances
    sidebars.forEach(sidebar => {
      // find all the instances of the sidebar's items
      const items = sidebar.querySelectorAll(
        '.mxc-theme-sidebar-list > .mxc-theme-item'
      );
      let activeItem = sidebar.querySelector(
        '.mxc-theme-sidebar-list > .mxc-theme-item.active'
      );
      let toggleButton = sidebar.querySelector('.mxc-theme-sidebar-toggle');

      // add event listeners for click
      items.forEach(item => {
        item.addEventListener('click', ({ target }) => {
          // if this is not the active item
          if (!target.classList.contains('active')) {
            // remove active item's class and assign this element to it
            activeItem && activeItem.classList.remove('active');
            activeItem = target;
            activeItem.classList.add('active');
          } else {
            activeItem.classList.remove('active');
            activeItem = null;
          }
        });
      });

      // add event listner for toggle button
      toggleButton &&
        toggleButton.addEventListener('click', ({ target }) => {
          sidebar.classList.toggle('mxc-theme-sidebar-is-closed');
        });
    });

    // script for carousel
    const carousels = document.querySelectorAll('.mxc-theme-carousel');

    // for each of the carousels
    carousels.forEach(carousel => {
      let currentPage = 0;
      const carouselWindow = carousel.querySelector(
        '.mxc-theme-carousel-window'
      );
      // calculate the number of pages
      const maxPages = carouselWindow.childNodes.length;

      // create the page selector elements
      const fragment = document.createDocumentFragment();
      const pages = document.createElement('div');
      pages.classList.add('mxc-theme-carousel-pages');
      const listPages = [];

      for (let index = 0; index < maxPages; index++) {
        const page = document.createElement('div');
        page.classList.add('mxc-theme-carousel-page');
        // add active to the first page by default
        if (!index) {
          page.classList.add('mxc-theme-active');
        }
        pages.appendChild(page);
        listPages.push(page);
      }

      // add the page to the fragment
      fragment.appendChild(pages);

      // add the fragment to the carousel
      carousel.appendChild(fragment);

      const nextSlide = () => {
        if (currentPage < maxPages - 1) {
          // unset the previous page's active
          let page = listPages[currentPage];
          page.classList.remove('mxc-theme-active');
          currentPage++;
          page = listPages[currentPage];
          page.classList.add('mxc-theme-active');
          // set page transform
          carouselWindow.style = `transform: translateX(-${currentPage *
            100}%)`;
        } else {
          // unset the previous page's active
          let page = listPages[currentPage];
          page.classList.remove('mxc-theme-active');
          currentPage = 0;
          page = listPages[currentPage];
          page.classList.add('mxc-theme-active');
          // set page transform
          carouselWindow.style = `transform: translateX(-${currentPage *
            100}%)`;
        }
      };

      // after every 4 seconds go to the next slide
      setInterval(() => {
        nextSlide();
      }, 4000);

      // add click events to the page dots
      for (let index = 0; index < listPages.length; index++) {
        const element = listPages[index];
        // eslint-disable-next-line no-loop-func
        element.addEventListener('click', () => {
          // only if the two don't match
          if (index !== currentPage) {
            // unset the previous page's active
            let page = listPages[currentPage];
            page.classList.remove('mxc-theme-active');
            currentPage = index;
            page = listPages[currentPage];
            page.classList.add('mxc-theme-active');
            // set page transform
            carouselWindow.style = `transform: translateX(-${currentPage *
              100}%)`;
          }
        });
      }
    });
  }, 500);
});
