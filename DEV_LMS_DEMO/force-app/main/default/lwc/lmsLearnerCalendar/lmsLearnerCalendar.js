import { LightningElement, track, wire, api } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import FullCalendarJS from '@salesforce/resourceUrl/FullCalendarJS';
import viewcourseDetails from '@salesforce/apex/LMS_CourseData.viewcourseDetails';

import pubsub from 'c/pubsubMod';

export default class LmsLearnerCalendar extends LightningElement {

  @api showPopup;
  @api managertype;



  eventFired(event) {

    this.data = event;
    console.log('Event from Calendar Caught in same component');
    this.showPopup = true;
  }
  connectedCallback() {
    this.data = 'callback';
    this.eventFiredCallback = this.eventFired.bind(this);
    this.register();
  }
  @api
  register() {
    this.data += ' - register';
    pubsub.register('uniqueEventId', this.eventFiredCallback);
  }

  fullCalendarJsInitialised = false;
  eventArray = [];

  renderedCallback() {

    // Performs this operation only on first render
    if (this.fullCalendarJsInitialised) {
      return;
    }
    this.fullCalendarJsInitialised = true;

    // Executes all loadScript and loadStyle promises
    // and only resolves them once all promises are done
    Promise.all([
      loadScript(this, FullCalendarJS + "/FullCalendarJS/jquery.min.js"),
      loadScript(this, FullCalendarJS + "/FullCalendarJS/moment.min.js"),
      loadScript(this, FullCalendarJS + "/FullCalendarJS/fullcalendar.min.js"),
      loadStyle(this, FullCalendarJS + "/FullCalendarJS/fullcalendar.min.css"),
      // loadStyle(this, FullCalendarJS + '/fullcalendar.print.min.css')
    ])
      .then(() => {
        this.fetchEvents();

        // Initialise the calendar configuration

      })
      .catch(error => {
        // eslint-disable-next-line no-console
        console.error({
          message: 'Error occured on FullCalendarJS',
          error
        });
      })
  }

  /**
   * @description Initialise the calendar configuration
   *              This is where we configure the available options for the calendar.
   *              This is also where we load the Events data.
   */
  initialiseFullCalendarJs() {

    const ele = this.template.querySelector('div.fullcalendarjs');



    // eslint-disable-next-line no-undef
    $(ele).fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      displayEventTime: true,
      defaultDate: new Date(),
      // defaultDate: new Date(), // default day is today
      navLinks: true, // can click day/week names to navigate views
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      events: this.eventArray,
      dayClick: function (date, jsEvent, view) {
        console.log('test');
        pubsub.fire('uniqueEventId', 'evt.detail');
      },
      eventClick: function (info) {
        console.log('event Clicked ');

      }
    });
  }



  fetchEvents() {


    console.log('in fetch events');
    
    viewcourseDetails().then((data)=>{
      console.log('>>>>data'+data);
        
     
          data.futureCourses.map(course=>{
            var temp = '/'+course.Course_Schedule__r.Course_Catalogue__r.Id;
            course.Course_Schedule__r.Course_Catalogue__r.Id = temp;
            console.log('>>>>>Id'+course.Course_Schedule__r.Course_Catalogue__r.Id);
          console.log('course futureCourses'+course);
          
           this.eventArray.push({title:course.Course_Schedule__r.Course_Catalogue__r.Name,start : course.Course_Schedule__r.Start_Date__c,end:course.Course_Schedule__r.End_Date__c ,color:'#FFA500',url:course.Course_Schedule__r.Course_Catalogue__r.Id});
         })
         data.pastCourses.map(course=>{
          console.log('course pastCourses detailsa'+course.Course_Schedule__r.Name+'>>>>'+course.Started_Date__c+'>>>>'+course.Status__c);
          var temp = '/'+course.Course_Schedule__r.Course_Catalogue__r.Id;
          course.Course_Schedule__r.Course_Catalogue__r.Id = temp;
          console.log('>>>>>Id'+course.Course_Schedule__r.Course_Catalogue__r.Id);
           if(course.Status__c === 'Not Started'){
            console.log('Not Started');
            this.eventArray.push({title:course.Course_Schedule__r.Course_Catalogue__r.Name , start : course.Course_Schedule__r.Start_Date__c, end:course.Course_Schedule__r.End_Date__c  , color:'#FF0000',url:course.Course_Schedule__r.Course_Catalogue__r.Id});

           }
           if(course.Status__c === 'In progress'){
            console.log('In progress');
            this.eventArray.push({title:course.Course_Schedule__r.Course_Catalogue__r.Name , start : course.Course_Schedule__r.Start_Date__c, end:course.Course_Schedule__r.End_Date__c  , color:'#008000',url:course.Course_Schedule__r.Course_Catalogue__r.Id});

           }
           if(course.Status__c === 'Completed'){
            console.log('Completed');
            this.eventArray.push({title:course.Course_Schedule__r.Course_Catalogue__r.Name , start : course.Course_Schedule__r.Start_Date__c, end:course.Course_Schedule__r.End_Date__c  , color:'#696969',url:course.Course_Schedule__r.Course_Catalogue__r.Id});

           }
           
         })
         
     
        this.initialiseFullCalendarJs();
 
       }).catch(error => {
        window.console.log(' Error Occured in my method', error)
      })
    

    
    
    // this.eventArray.push({ title: 'Course Name1', start: new Date(), end: new Date(new Date().getTime() + 30*60000), color: '#FF0000',url:'/a004x0000035A1WAAU' });
    // this.eventArray.push({ title: 'Course Name1', start: new Date(new Date().getTime() + 10*60000), end: new Date(new Date().getTime() + 40*60000), color: '#FF0000',url:'/a004x0000035A1WAAU' });
    // this.initialiseFullCalendarJs();
  }
}