import { LightningElement, api, track, wire } from 'lwc';
import fetchCourses from '@salesforce/apex/LMS_CoursesController.fetchCourses';
import { showAjaxErrorMessage } from 'c/lmsUtils';
import { NavigationMixin } from 'lightning/navigation';

const CONTAINER_ACTIVE = 'active';
const CONTAINER_UPDATED = 'updated';
const CONTAINER_COMPLETED = 'completed';
const CONTAINER_OVERDUE = 'overdue';
export default class LmsCourses extends NavigationMixin(LightningElement) {

    @api container = CONTAINER_ACTIVE;

    @track searchObject = {
        searchTerm: ''
    }
    @track courses = [];

    showSpinner = false;
    cmpLoaded = false;
    heading;
    subheading;
    noCourseFoundTitle;


    connectedCallback() {
        this.searchObject.container = this.container;
        
        this.loadCourses();

        this.showHeadings();
    }

    showHeadings() {
        if( this.isActiveContainer ) {
            this.heading = 'My Active Courses';
            this.subheading = 'Courses that you have enrolled in or currently have in progress.';
            this.noCourseFoundTitle = 'You have no active courses at this moment.';
        } else if( this.isUpdatedContainer ) {
            this.heading = 'My Updated Courses';
            this.subheading = 'Courses that are updated.';
            this.noCourseFoundTitle = 'You have no updated courses at this moment.';
        } else if( this.isCompletedContainer ) {
            this.heading = 'My Completed Courses';
            this.subheading = 'Courses that you have completed.';
            this.noCourseFoundTitle = 'You have no completed courses at this moment.';
        }
        else if( this.isOverdueContainer ) {
            this.heading = 'My Overdue Courses';
            this.subheading = 'Courses that are overdue.';
            this.noCourseFoundTitle = 'You have no overdue courses at this moment.';
        }
    }

    loadCourses() {
        this.showSpinner = true;
        fetchCourses({
            searchObject: JSON.stringify(this.searchObject)
        })
            .then(data => {
                this.courses = data.courseItems;
                console.log('coursedata >>'+ this.courses );
                console.log('size of coursedata'+this.courses.length);
            })
            .catch(error => {
                console.log('error>>' + error);
                showAjaxErrorMessage(this, error);
            })
            .finally(() => {
                this.showSpinner = false;
                this.cmpLoaded = true;
            });
    }

    handleViewCatalog() {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: 'Course_Catalog__c'
            }
        });
    }

    get isActiveContainer() {
        return this.container === CONTAINER_ACTIVE;
    }

    get isUpdatedContainer() {
        return this.container === CONTAINER_UPDATED;
    }

    get isCompletedContainer() {
        return this.container === CONTAINER_COMPLETED;
    }
    get isOverdueContainer() {
        return this.container === CONTAINER_OVERDUE;
    }
    get noCoursesFound() {
        return !this.courses || this.courses.length === 0;
    }
}