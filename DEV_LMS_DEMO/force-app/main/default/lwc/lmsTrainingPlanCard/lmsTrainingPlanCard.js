import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';


export default class LmsTrainingPlanCard extends NavigationMixin(LightningElement) {

  @api trainingPlan;
  navigateToTrainingPlan() {
    this[NavigationMixin.Navigate]({
      type: 'standard__recordPage',
      attributes: {
        recordId: this.trainingPlan.recId,
        objectApiName: 'Training_Plan__c',
        actionName: 'view'
      }

    });

  }
}