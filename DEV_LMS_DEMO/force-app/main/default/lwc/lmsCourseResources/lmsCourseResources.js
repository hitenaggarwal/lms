import { LightningElement, api } from 'lwc';

export default class LmsCourseResources extends LightningElement {
    @api resources = [];

    handleDownload(event) {
        alert('Download resource: '+event.target.dataset.recourceId);
    }

    handleVisit(event) {
        alert('Visit resource: '+event.target.dataset.recourceId);
    }

    get noResourcesFound() {
        return !this.resources || this.resources.length === 0;
    }
}