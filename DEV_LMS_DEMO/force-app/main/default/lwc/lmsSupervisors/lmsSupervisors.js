import { LightningElement, track } from "lwc";
import getMyReportees from "@salesforce/apex/LMS_UserReporteeController.getMyReportees";

export default class LmsSupervisors extends LightningElement {
  isModalOpen = false;
  reporteeRecordId;
  @track reporteesList = [];

  connectedCallback() {
    this.fetchReportees();
  }

  fetchReportees() {
    getMyReportees()
      .then((result) => {
        console.log(result);
        console.log(JSON.stringify(result));
        this.reporteesList = result;
      })
      .catch((error) => console.log(error));
  }

  openChangeSupervisorModal(event) {
    this.isModalOpen = true;
    this.reporteeRecordId = event.currentTarget.dataset.id.substring(0, 15);
    console.log("recordId" + this.reporteeRecordId);
  }
  closeChangeSupervisorModal() {
    this.isModalOpen = false;
  }
  handleSubmit(event) {
    event.preventDefault(); // stop the form from submitting
    this.template.querySelector("lightning-record-edit-form").submit();
    console.log("here");
    this.isModalOpen = false;
  }
}