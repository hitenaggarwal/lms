import { LightningElement, api } from 'lwc';

export default class LmsActiveCourseCard extends LightningElement {
    @api course;
}