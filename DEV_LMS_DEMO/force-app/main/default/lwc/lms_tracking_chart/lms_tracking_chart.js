import { LightningElement, api } from "lwc";
import jquerymin from "@salesforce/resourceUrl/jquerymin";
import easypiechart from "@salesforce/resourceUrl/easypiechart";
import { loadScript } from "lightning/platformResourceLoader";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class Lms_tracking_chart extends LightningElement {
  @api
  percentage;
  @api
  heading;

  connectedCallback() {
    Promise.all([loadScript(this, jquerymin), loadScript(this, easypiechart)])
      .then(() => {
        this.template.querySelector(".chart").innerHTML = this.percentage + "%";
        let chart = (window.chart = new EasyPieChart(
          this.template.querySelector("div.chart"),
          {
            easing: "easeOutElastic",
            delay: 3000,
            size: 120,
            barColor: "#28a6ff",
            trackColor: "#a5bfd2",
            scaleColor: false,
            lineWidth: 8,
            trackWidth: 6,
            lineCap: "butt"
          }
        ));
      })
      .catch((error) => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Error loading Script",
            message: error,
            variant: "error"
          })
        );
      });
  }
}