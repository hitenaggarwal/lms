import {
    LightningElement,
    api
} from "lwc";

export default class LmsRatingCmp extends LightningElement {
    static courseRating;

    @api name;

    rating(event) {
        if (event.target.name === "Course") {
            LmsRatingCmp.courseRating = event.target.value;
        }
    }
    @api
    getvalues() {
        return LmsRatingCmp.courseRating;
    }
}