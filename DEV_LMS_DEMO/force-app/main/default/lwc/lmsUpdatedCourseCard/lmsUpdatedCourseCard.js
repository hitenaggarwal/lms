import { LightningElement, api } from 'lwc';

export default class LmsUpdatedCourseCard extends LightningElement {
    @api course;
}