import { LightningElement, api } from 'lwc';
import updateUserEnrollment from "@salesforce/apex/LMS_Evaluation_Controller.updateUserEnrollment";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class LmsSubmitEvaluation extends LightningElement {
    
    @api userEnrollmentId;
    
    @api showSubmitModal;

    connectedCallback() {
        console.log('User Enrollment Id : ' + this.userEnrollmentId);
    }

    handleInputs(event) {
        if(event.target.name === "notes") {
            this.notes = event.target.value;
        }
        else if(event.target.name === "grades") {
            this.grades = event.target.value;
        }      
    }

    handleUpdate() {
        
        updateUserEnrollment({
            userEnrollmentId: this.userEnrollmentId,
            notes: this.notes,
            grades: this.grades,
            status: true
        })
        .then(result => {
            console.log('Result : ' + result);
            this.showSubmitModal = false;
            this.dispatchEvent(new CustomEvent("submitstatuschange", {
                detail: this.showSubmitModal
            }));
            this.handleToastMessage("Success", "Evaluation details submitted successfully");
        })
        .catch(error => {
            console.log(error);
            this.handleToastMessage("Error", "Please enter valid values.");
        });
       
    }

    handleToastMessage(toastType, message) {
        var variant;
        toastType == "Success"
          ? (variant = "success")
          : toastType == "Error"
          ? (variant = "error")
          : (variant = "");
        const event = new ShowToastEvent({
          title: toastType,
          message: message,
          variant: variant,
          mode: "dismissable"
        });
        this.dispatchEvent(event);
    }

    closeModal() {
        this.showSubmitModal = false;
        this.dispatchEvent(new CustomEvent("submitstatuschange", {
            detail: this.showSubmitModal
        }));
    }

    
}