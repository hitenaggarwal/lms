import { LightningElement, track,api} from 'lwc';
import insertSuspendData from '@salesforce/apex/ScromDemoController.insertSuspendData';
import getSuspendData from '@salesforce/apex/ScromDemoController.getSuspendData';
import setModuleComplete from '@salesforce/apex/ScromDemoController.setModuleComplete';


import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class ScromDemo extends LightningElement {

    @api scromURL
    @api userModuleAssociateId
    showSpinner = false;
    completionStatuses = ['passed', 'completed'];
    lessonStatuskeys = [
        ['cmi.core.lesson_status'],
        ['cmi.completion_status', 'cmi.success_status']
    ];
    isModuleCompleted = false;
    loadScorm = false;
    key = 'cmi.suspend_data';
    currentInteractions = [];
    isModuleStarted = false;
    iframe;
    progress = 0;
    scormBaseUrl = 'https://lms-scorm-app.herokuapp.com/';
    @track suspendData = '';
    @track progressPercentage = 0;
    @track data = {
        module: "progress",
        suspendData:this.suspendData,
        isModuleCompleted: false
    }
  

    renderedCallback() {
        console.log('SCORM DEMO'+ this.scromURL);
        console.log('SCORM DEMO'+ this.userModuleAssociateId);
        this.iframe = this.template.querySelector('iframe');
        //this.sendSuspendData();
    }
    connectedCallback() {
        console.log('SCORM DEMO'+ this.scromURL);
        console.log('SCORM DEMO'+ this.userModuleAssociateId);
        this.handleGetSuspendData();
        
        this.setScormInfo();
        this.addMessageListener();
    }

    setScormInfo() {
        //this.showSpinner = true;
        if (this.data.module) {
            this.loadScorm = true;
        }
        this.isModuleCompleted = this.data.isModuleCompleted

    }

   

    

    handleSuspendedData() {
        this.showSpinner = true;
        console.log('adding suspended data');
        insertSuspendData({
            recordId: this.userModuleAssociateId,
            suspendData:this.suspendData
            })
            .then(data => {
                this.showSpinner = false;
            })
            .catch(error => {
                console.error(error);
                this.showToast('Error', error.body.message, 'error');
            })
            .finally(() => {
                this.showSpinner = false;
            });
    }

    handleGetSuspendData() {
        this.showSpinner = true;

        getSuspendData({
            recordId: this.userModuleAssociateId,
            })
            .then(data => {
                if(data){
                    this.data.suspendData=data;
                    
                }
            })
            .catch(error => {
                console.error(error);
                this.showToast('Error', error.body.message, 'error');
            })
            .finally(() => {
                this.showSpinner = false;
            });
    }

    showToast(title, message, variant) {
        this.dispatchEvent(new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        }));
    }

    // Listener for message transfer
    addMessageListener() {
        window.addEventListener(
            'message',
            (e) => {
                console.log('In Event');

                console.log(JSON.stringify(e.data.data));
                if (e.data) {
                    if(e.data.data){
                        if(e.data.data["cmi.suspend_data"]){
                            this.suspendData=e.data.data["cmi.suspend_data"];
                            console.log('suspendData-->' + this.suspendData);
                            this.handleSuspendedData();
                        }
                        if (e.data.data["cmi.progress_measure"]) {
                            this.progressPercentage = e.data.data["cmi.progress_measure"];
                            this.progressPercentage = this.progressPercentage * 100;
                            console.log('progressPercentage-->' + this.progressPercentage);
                        }
                    }

                    if (e.data.eventName === 'LMSSetValue' && e.data.data[this.key]) {
                        this.storeValues(e.data.data);
                    }
                    if (e.data.eventName === 'LMSInitialize') {
                        this.showSpinner = false;
                        this.isModuleStarted = true
                    }
                    if (e.data.eventName === 'MediatorInitiated') {
                        this.sendSuspendData();
                    }
                    this.hasModuleCompleted(e.data.data);
                }
            },
            false
        );
    }

    hasModuleCompleted(data) {
        if (data) {
            for (let key of this.lessonStatuskeys) {
                let i = 0;
                for (i; i < key.length; i++) {
                    if (!this.completionStatuses.includes(data[key[i]])) {
                        break;
                    }
                }
                if (i == key.length) {
                    this.isModuleCompleted = true;
                    this.showToast('Success', 'Module completed', 'success');
                    //update course user enrollment
                    setModuleComplete({userModuleId :this.userModuleAssociateId });
                    break;
                }
            }
        }
    }

    sendSuspendData() {
        this.sendMessage({
            eventName: 'LoadSCORM',
            data: {
                module: this.data.module,
                suspendData: this.data.suspendData
            }
        });
    }

    storeValues(data) {
        if (!this.data.isModuleCompleted) {
            this.currentInteractions.push(data);
        }
    }

    sendMessage(data) {
        // if (this.iframe && this.iframe.nativeElement && this.iframe.nativeElement.contentWindow) {
        //     this.iframe.nativeElement.contentWindow.postMessage(
        //         data,
        //         environment.scormBaseUrl
        //     );
        // }
   
        console.log('sendMessage data->', data);
        if (this.iframe && this.iframe.contentWindow ) {
            this.iframe.contentWindow.postMessage(
                data,
                this.scormBaseUrl
            );
        }
    }
}