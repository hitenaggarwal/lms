import { LightningElement,track } from 'lwc';
import getCourseDetails from '@salesforce/apex/lmsCourseManagementController.getCourseDetails';
import { NavigationMixin } from 'lightning/navigation';

export default class LmsCourseManagement  extends NavigationMixin(LightningElement)  {
    @track courses;
    connectedCallback(){
        getCourseDetails().then((data)=>{
            data.courseCatalog
            console.log('>>>>>data'+data);
            this.courses = data;
            console.log('>>>>>courses'+this.courses);

        }).catch((error)=>{
            console.log('>>>>>error'+error);
        });
    }
    manageCourse(event){

        console.log(JSON.stringify(event.target.dataset));
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: '/course-details-admin?courseId='+event.target.dataset.courseId
            }
        
        });
    }
    }