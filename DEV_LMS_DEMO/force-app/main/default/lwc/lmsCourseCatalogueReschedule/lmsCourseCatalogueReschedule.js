/*import { LightningElement, track, api, wire } from 'lwc';
import getCourseSchedule from '@salesforce/apex/LMS_CourseCatalogueReschedule.getCourseSchedule';
import getUserEnrollments from '@salesforce/apex/LMS_CourseCatalogueReschedule.getUserEnrollments';
import getCourseCatelouge from '@salesforce/apex/LMS_CourseCatalogueReschedule.getCourseCatelouge';
import saveEnrollment from '@salesforce/apex/LMS_CourseCatalogueReschedule.saveEnrollment';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getErrorMessage } from "c/pubsubMod";
export default class LmsCourseCatalogueReschedule extends LightningElement {

    @api courseCatalogueId = 'a004x0000035840AAA';
    @track courseScheduleList = [];
    @track enrollementList = [];
    @track courseCatelougeList = [];
    @track showTable = false;
    @track editEnrollementId;
    @track startDate = '';
    @track showModal = false;
    @track refreshEnrollemtId;
    @track showSpinner = false;

    connectedCallback() {
        this.fetchInitialData();
    }

    handleCourseChange(event) {
        if (event.target.value != '') {
            this.courseScheduleList = [];
            this.enrollementList = [];
            this.showTable = false;
            this.fetchData(event.target.value);
        } else {
            this.courseScheduleList = [];
            this.enrollementList = [];
            this.showTable = false;
        }
    }

    fetchInitialData() {
        getCourseCatelouge({
            
        })
            .then(result => {
                this.courseCatelougeList = [];
                this.courseCatelougeList = result;
            })
            .catch(error => {
                console.log("Error " + error);
            });
    }

    fetchData(id) {
        getCourseSchedule({
            id: id
        })
            .then(result => {
                this.courseScheduleList = [];
                this.courseScheduleList = result;
            })
            .catch(error => {
                console.log("Error " + error);
            });
    }

    handleCourseSchedule(event) {
        if (event.target.value == '') {
            this.showTable = false;
        } else {
            this.fetchEnrollementData(event.target.value);
            this.refreshEnrollemtId = event.target.value;
            this.showTable = true;
        }
    }

    fetchEnrollementData(eid) {
        this.showSpinner = true;
        getUserEnrollments({
            id: eid
        })
            .then(result => {
                this.enrollementList = [];
                this.showSpinner = false;
                if (result != '' && result != null && result != []) {
                    result.forEach(element => {
                        let _element = Object.assign({}, element);
                        _element.showEdit = true;
                        if (_element.Status__c == 'Not Started') {
                            _element.showEdit = false;
                        }
                        element = { ..._element };
                        this.enrollementList.push(element);
                    });
                    //this.enrollementList = result;
                }
            })
            .catch(error => {
                this.showSpinner = false;
                console.log("Error  " + error);
            });
    }

    openModal(event) {
        this.editEnrollementId = event.currentTarget.dataset.id;
        this.startDate = this.enrollementList[event.currentTarget.dataset.index].Started_Date__c;
        console.log('test1' + this.startDate);
        this.showModal = true;
    }

    handleCancel() {
        this.showModal = false;
        this.startDate = '';
    }

    handleDateChange(event) {
        this.startDate = event.target.value;
    }

    handleSave() {
        const allValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        console.log("all Vaild :" + allValid);
        if (allValid) {
            saveEnrollment({
                id: this.editEnrollementId,
                startDate: this.startDate
            })
                .then(result => {
                    if (result == 'SUCCESS') {
                        this.startDate = '';
                        this.showModal = false;
                        const evt = new ShowToastEvent({
                            title: "Success",
                            message: "Record Updated Successfully.",
                            variant: "success"
                        });
                        this.dispatchEvent(evt);
                        this.fetchEnrollementData(this.refreshEnrollemtId);
                    }
                })
                .catch(error => {
                    let erroMsg = getErrorMessage(error);
                    const evt = new ShowToastEvent({
                        title: "Error",
                        message: erroMsg,
                        variant: "error"
                    });
                    this.dispatchEvent(evt);
                    console.log("Error " + JSON.stringify(error));
                });
        }
    }
} */

import { LightningElement, track, api, wire } from "lwc";
import getCourseSchedule from "@salesforce/apex/LMS_CourseCatalogueReschedule.getCourseSchedule";
import saveEnrollment from "@salesforce/apex/LMS_CourseCatalogueReschedule.saveEnrollment";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getUserEnrollments1 from "@salesforce/apex/LMS_CourseCatalogueReschedule.getUserEnrollments1";
import deleteEnrolledUser from "@salesforce/apex/LMS_CourseCatalogueReschedule.deleteEnrolledUser";
import updateWaitListStatus from "@salesforce/apex/LMS_CourseCatalogueReschedule.updateWaitListStatus";
import updateEnrollmentRecord from "@salesforce/apex/LMS_CourseCatalogueReschedule.updateEnrollmentRecord";
import getReservedEnrolledUser from "@salesforce/apex/LMS_CourseCatalogueReschedule.getReservedEnrolledUser";
import enrolledUserOnReservedSeats from "@salesforce/apex/LMS_CourseCatalogueReschedule.enrolledUserOnReservedSeats";
import enrollLearner from "@salesforce/apex/LMS_CourseCatalogueReschedule.enrollLearner";
import getUserModuleAssociation from "@salesforce/apex/LMS_ManageTaskComponentController.getUserModuleAssociation";
import deleteUserModuleRecord from "@salesforce/apex/LMS_ManageTaskComponentController.deleteUserModuleRecord";
import updateUserModuleRecord from "@salesforce/apex/LMS_ManageTaskComponentController.updateUserModuleRecord";
import doInit from "@salesforce/apex/LMS_CourseEmailNotificationController.doInit";
import sendEmail from "@salesforce/apex/LMS_CourseEmailNotificationController.sendEmail";
import { NavigationMixin } from "lightning/navigation";
import { getErrorMessage } from "c/pubsubMod";

export default class LmsCourseCatalogueReschedule extends NavigationMixin(
    LightningElement
) {
    @api recordId;
    @api courseCatalogueId;
    @track courseScheduleList = [];
    @track enrollementList = [];
    @track courseCatelougeList = [];
    @track showTable = false;
    @track editEnrollementId;
    @track startDate = "";
    @track showModal = false;
    @track refreshEnrollemtId;
    showSpinner = false;

    selectedScheduleId;

    showSubmitModal = false;
    showViewModal = false;
    showDeleteModal = false;
    showEnrollUserModal = false;
    isShowReserveSeatModal = false;
    showErrorOnReserveSeatModal = false;
    isShowAssignedReservedSeatModal = false;
    showPrequisiteCourses = false;
    reservedEnrolledUserList = [];
    prerequisiteCourseList = [];
    reserveSeatAccountId = "";
    errorMessageReserveSeat = "";
    reserveSeatNumber;
    userEnrollId;
    recordIdForDelete;
    contactId;
    courseScheduleId;
    courseScheduleReserveSeatId;
    deleteItemMessage;
    userEnrollmentName;
    @track assignedFieldContactMap = {};
    @track tempAssignedFieldContactMap = {};

    @track showEmailModal = false;
    @track showSpinner = false;

    @track emailToAddresses = "";
    @track emailSubject = "";
    @track emailBody = "";

    @track showAttendeeStatusOptions = false;
    @track attendeeStatusOption = "All";
    attendeeStatusOptions = [
        { label: "All", value: "All" },
        { label: "Confirmed", value: "Confirmed" },
        { label: "Waitlist", value: "Waitlist" }
    ];

    allowedFormats = [
        "font",
        "size",
        "bold",
        "italic",
        "underline",
        "strike",
        "list",
        "indent",
        "align",
        "link",
        "clean",
        "table",
        "header",
        "color",
        "background"
    ];

    @track emailBodyValidity = true;

    enrollementList = [];
    userModuleAssociationList = [];
    showManageTaskButton = true;
    showManageTaskModal = false;
    isShowDeleteTaskModal = false;
    userModuleToDeleteId = "";
    manageTaskUserEnrollmentId = "";
    @track fieldChangeTrackMap = {};



    contactListNotEnrolled = [];

    connectedCallback() {
        this.courseCatalogueId = this.recordId;
        console.log("Course Catalogue Id >>> " + this.courseCatalogueId);
        this.fetchData(this.courseCatalogueId);
    }

    fetchData(id) {
        getCourseSchedule({
                id: id
            })
            .then((result) => {
                this.courseScheduleList = [];
                result.forEach((element) => {
                    let _element = Object.assign({}, element);
                    _element.enrollementList = [];
                    element = {..._element };
                    this.courseScheduleList.push(element);
                });
                this.getEnrollments();
            })
            .catch((error) => {
                console.log("Error " + error);
            });
    }

    getEnrollments() {
        let res = [];
        this.courseScheduleList.forEach((element1) => {
            res.push(element1.Id);
        });
        getUserEnrollments1({
                id: res
            })
            .then((result) => {
                if (result != "" && result != null && result != []) {
                    result.forEach((element) => {
                        let _element = Object.assign({}, element);
                        _element.showEdit = true;
                        if (_element.Status__c == "Not Started") {
                            _element.showEdit = false;
                        }
                        element = {..._element };
                        this.courseScheduleList.forEach((element1) => {
                            if (element1.Id == element.Course_Schedule__c) {
                                element1.enrollementList.push(element);
                            }
                        });
                    });
                }
                console.log("Result >>> " + JSON.stringify(this.enrollementList));
                console.log(JSON.stringify(this.courseScheduleList));
            })
            .catch((error) => {
                console.log("Error  " + error);
            });
    }

    openModal(event) {
        this.editEnrollementId = event.currentTarget.dataset.id;
        console.log(
            "test1" + this.courseScheduleList[event.currentTarget.dataset.pindex]
        );
        this.startDate = this.courseScheduleList[
            event.currentTarget.dataset.pindex
        ].enrollementList[event.currentTarget.dataset.index].Started_Date__c;
        console.log("test1" + this.startDate);
        this.showModal = true;
    }

    handleCancel() {
        this.showModal = false;
        this.startDate = "";
    }

    handleDateChange(event) {
        this.startDate = event.target.value;
    }

    handleSave() {
        const allValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        console.log("all Vaild :" + allValid);
        if (allValid) {
            saveEnrollment({
                    id: this.editEnrollementId,
                    startDate: this.startDate
                })
                .then((result) => {
                    if (result == "SUCCESS") {
                        this.startDate = "";
                        this.showModal = false;
                        const evt = new ShowToastEvent({
                            title: "Success",
                            message: "Record Updated Successfully.",
                            variant: "success"
                        });
                        this.dispatchEvent(evt);
                        this.fetchData(this.courseCatalogueId);
                    }
                })
                .catch((error) => {
                    let erroMsg = getErrorMessage(error);
                    const evt = new ShowToastEvent({
                        title: "Error",
                        message: erroMsg,
                        variant: "error"
                    });
                    this.dispatchEvent(evt);
                    console.log("Error " + JSON.stringify(error));
                });
        }
    }

    handleSubmitEvaluation(event) {
        this.showSubmitModal = true;
        this.userEnrollId = event.target.dataset.id;
    }
    handleViewEvaluation(event) {
        this.showViewModal = true;
        this.userEnrollId = event.target.dataset.id;
    }

    closeModal() {
        this.showSubmitModal = false;
        this.showViewModal = false;
        this.showEnrollUserModal = false;
    }

    handleSubmitStatusChange(event) {
        this.showSubmitModal = event.detail;
    }

    openDeleteEnrolledUserModal(event) {
        this.deleteItemMessage = "Are you sure you want to delete?";
        this.recordIdForDelete = event.target.dataset.id;
        console.log("Record Id >> " + this.recordIdForDelete);
        this.showDeleteModal = true;
    }
    handleCancelDeleteRecord() {
        this.showDeleteModal = false;
        this.recordIdForDelete = null;
        this.deleteItemMessage = "";
    }
    handleDeleteEnrolledUser(event) {
        console.log("Record Id in delete >> " + this.recordIdForDelete);
        if (
            this.recordIdForDelete != "" &&
            this.recordIdForDelete != null &&
            this.recordIdForDelete != undefined
        ) {
            console.log("Record Id in delete >> " + this.recordIdForDelete);
            this.showSpinner = true;
            deleteEnrolledUser({
                    recordIdForDelete: this.recordIdForDelete
                })
                .then((result) => {
                    this.fetchData(this.courseCatalogueId);
                    this.handleCancelDeleteRecord();
                    this.showSpinner = false;
                })
                .catch((error) => {
                    this.showSpinner = false;
                });
        }
    }
    moveToWaitlist(event) {
        const userEnrollId = event.target.dataset.enrollid;
        const courseScheduleId = event.target.dataset.csid;

        updateWaitListStatus({
                courseScheduleId: courseScheduleId,
                userEnrollId: userEnrollId
            })
            .then((result) => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: "Success",
                        message: "User moved to waitlist!!",
                        variant: "success"
                    })
                );
                this.fetchData(this.courseCatalogueId);
                this.showSpinner = false;
            })
            .catch((error) => console.log(JSON.stringify(error)));
    }

    handleContactChange(event) {
        console.log('LIST OF ID:' + JSON.stringify(event.detail));
        this.selectedContactList = event.detail;
        this.contactId = event.detail ? event.detail.Id : null;
        console.log("contactId >> " + this.contactId);
    }
    handleEnrollmentChange(event) {
        this.userEnrollmentName = event.target.value;
    }
    openEnrollLearnerModal(event) {
        this.courseScheduleId = event.target.dataset.csid;
        console.log("schedule id >> " + this.courseScheduleId);
        this.showEnrollUserModal = true;
    }
    handleEnrollUsers(event) {
        console.log('Handling Enroll');
        const toUpdate = this.selectedContactList.length;
        let updated = 0;
        for(var i in this.selectedContactList){
            const conId =  this.selectedContactLis[i];
            enrollLearner({
                courseId: this.courseCatalogueId,
                contact: conId,
                courseScheduleId: this.courseScheduleId
            })
            .then((result) => {
                updated += 1; 
                if(!result){
                    this.contactListNotEnrolled.push(conId);
                }else if (result.length <= 0) {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: "Success",
                            message: "Learner enrolled successfully!!",
                            variant: "success"
                        })
                    );
                    this.showEnrollUserModal = false;
                    this.fetchData(this.courseCatalogueId);
                    this.showSpinner = false;
                    this.userEnrollmentName = "";
                } else {
                    this.showPrequisiteCourses = true;
                    for (let key in result) {
                        console.log("key >> " + key);
                        this.prerequisiteCourseList.push({
                            name: result[key].Name,
                            key: key,
                            url: "https://dhs-lms.force.com/s/course-details-admin?courseId=" +
                                result[key].Id
                        });
                    }
                    console.log(JSON.stringify(this.prerequisiteCourseList));
                }
                console.log('toUpdate' + toUpdate);
                console.log('updated' + updated);
                if(toUpdate == updated){
                    console.log('updated all');
                }
            })
            .catch((error) => console.log(JSON.stringify(error)));

        }


        if (this.contactId != null) {
            enrollLearner({
                    courseId: this.courseCatalogueId,
                    contact: this.contactId,
                    courseScheduleId: this.courseScheduleId
                })
                .then((result) => {
                    console.log("Result prerequisite >> " + JSON.stringify(result));

                    if(!result){
                        contactListNotEnrolled.push(conId);


                    }

                    if (result.length <= 0) {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: "Success",
                                message: "Learner enrolled successfully!!",
                                variant: "success"
                            })
                        );
                        this.showEnrollUserModal = false;
                        this.fetchData(this.courseCatalogueId);
                        this.showSpinner = false;
                        this.userEnrollmentName = "";
                    } else {
                        this.showPrequisiteCourses = true;
                        for (let key in result) {
                            console.log("key >> " + key);
                            this.prerequisiteCourseList.push({
                                name: result[key].Name,
                                key: key,
                                url: "https://dhs-lms.force.com/s/course-details-admin?courseId=" +
                                    result[key].Id
                            });
                        }
                        console.log(JSON.stringify(this.prerequisiteCourseList));
                    }
                })
                .catch((error) => console.log(JSON.stringify(error)));
        }
    }

    handleReserveSeatButton(event) {
        this.courseScheduleReserveSeatId = event.target.dataset.csid;
        this.isShowReserveSeatModal = true;
    }
    handleAssignedSeatButton(event) {
        this.isShowAssignedReservedSeatModal = true;
        this.courseScheduleReserveSeatId = event.target.dataset.csid;
    }

    onCancelReserveSeatModal() {
        this.isShowReserveSeatModal = false;
    }

    onSaveReserveSeatModal() {
        this.showSpinner = true;
        let valid = false;
        valid = [...this.template.querySelectorAll("lightning-input")].reduce(
            (validSoFar, input) => {
                input.reportValidity();
                return validSoFar && input.checkValidity();
            },
            true
        );

        if (valid) {
            enrolledUserOnReservedSeats({
                    courseScheduleId: this.courseScheduleReserveSeatId,
                    accountId: this.reserveSeatAccountId,
                    seats: this.reserveSeatNumber
                })
                .then((data) => {
                    if (data != "Success") {
                        this.showErrorOnReserveSeatModal = true;
                        this.errorMessageReserveSeat = data;
                        this.showSpinner = false;
                    } else {
                        this.fetchData(this.courseCatalogueId);
                        this.handleToastMessage("Success", "Seats Assigned Successfully!!");
                        this.showErrorOnReserveSeatModal = false;
                        this.isShowReserveSeatModal = false;
                        this.showSpinner = false;
                        this.reserveSeatNumber = "";
                    }
                    console.log("Response Enroll:" + data);
                })
                .catch((err) => {
                    this.reserveSeatNumber = "";
                    this.showSpinner = false;
                    this.handleError(err);
                    console.log(err);
                });
        }
    }

    handleAccountChange(event) {
        this.reserveSeatAccountId = event.detail ? event.detail.Id : null;
        console.log("Account Id:" + this.reserveSeatAccountId);
    }

    handleReserveSeatChange(event) {
        this.reserveSeatNumber = event.detail.value;
        console.log("Seat Number:" + this.reserveSeatNumber);
    }

    handleCancelAssignedSeatModal() {
        this.assignedFieldContactMap = {};
        this.tempAssignedFieldContactMap = {};
        this.reservedEnrolledUserList = [];
        this.isShowAssignedReservedSeatModal = false;
    }

    handleSaveAssignedSeatModal() {
        this.showSpinner = true;
        updateEnrollmentRecord({ userEnrollMap: this.assignedFieldContactMap })
            .then((data) => {
                console.log("Success");
                this.fetchData(this.courseCatalogueId);
                this.showSpinner = false;
                this.handleToastMessage("Success", "Seats Assigned Successfully!!");
                this.assignedFieldContactMap = {};
                this.tempAssignedFieldContactMap = {};
                this.reservedEnrolledUserList = [];
            })
            .catch((err) => {
                this.assignedFieldContactMap = {};
                this.tempAssignedFieldContactMap = {};
                this.reservedEnrolledUserList = [];
                this.showSpinner = false;
                this.handleError(err);
                console.log(err);
            });
        this.isShowAssignedReservedSeatModal = false;
    }

    handleAccountChangeAssignedSeat(event) {
        this.reserveSeatAccountId = event.detail ? event.detail.Id : null;
        this.assignedFieldContactMap = {};
        this.tempAssignedFieldContactMap = {};
        if (this.reserveSeatAccountId) {
            getReservedEnrolledUser({
                    courseScheduleId: this.courseScheduleReserveSeatId,
                    accountId: this.reserveSeatAccountId
                })
                .then((data) => {
                    for (var i = 0; i < data.length; i++) {
                        this.assignedFieldContactMap[data[i].Id] = data[i];
                    }
                    this.tempAssignedFieldContactMap = JSON.parse(
                        JSON.stringify(this.assignedFieldContactMap)
                    );
                    this.reservedEnrolledUserList = data;
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    }
    handleContactChangeAssignedSeat(event) {
        const contactId = event.detail ? event.detail.Id : null;
        const enrollmentId = event.target.dataset.id;
        console.log("ContactId:" + contactId);
        if (contactId != null) {
            this.assignedFieldContactMap[enrollmentId].Contact__c = contactId;
        } else {
            this.assignedFieldContactMap[
                enrollmentId
            ].Contact__c = this.tempAssignedFieldContactMap[enrollmentId].Contact__c;
        }
    }

    handleToastMessage(toastType, message) {
        var variant;
        toastType == "Success" ?
            (variant = "success") :
            toastType == "Error" ?
            (variant = "error") :
            (variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
        });
        this.dispatchEvent(event);
    }

    handleError(error) {
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map((e) => e.message).join(", ");
        } else if (typeof error.body.message === "string") {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }

    handleManageTaskClick(event) {
        this.showSpinner = true;
        var Id = event.currentTarget.dataset.id;
        this.manageTaskUserEnrollmentId = Id;
        getUserModuleAssociation({ recordId: Id })
            .then((data) => {
                this.userModuleAssociationList = data;
                this.fieldChangeTrackMap = {};
                for (var i = 0; i < data.length; i++) {
                    this.fieldChangeTrackMap[data[i].Id] = data[i];
                }
                this.showSpinner = false;
            })
            .catch((err) => {
                this.showSpinner = false;
                console.log(err);
            });
        this.showManageTaskModal = true;
    }

    onManageTaskModelCancel() {
        this.showManageTaskModal = false;
    }

    onManageTaskModelSave() {
        this.showSpinner = true;
        this.showManageTaskModal = false;
        updateUserModuleRecord({ updatedMap: this.fieldChangeTrackMap })
            .then((data) => {
                this.handleToastMessage("Success", "Task Updated Successfully");
                this.showSpinner = false;
            })
            .catch((err) => {
                this.handleError(err);
                this.showSpinner = false;
            });
    }

    deleteTaskHandle(event) {
        this.userModuleToDeleteId = event.currentTarget.dataset.rid;
        this.isShowDeleteTaskModal = true;
    }

    handleSaveDeleteTaskModal() {
        this.showSpinner = true;
        deleteUserModuleRecord({ recordId: this.userModuleToDeleteId })
            .then((data) => {
                this.userModuleToDeleteId = "";
                this.isShowDeleteTaskModal = false;

                getUserModuleAssociation({ recordId: this.manageTaskUserEnrollmentId })
                    .then((data) => {
                        this.userModuleAssociationList = data;
                        this.fieldChangeTrackMap = {};
                        for (var i = 0; i < data.length; i++) {
                            this.fieldChangeTrackMap[data[i].Id] = data[i];
                        }
                        this.showSpinner = false;
                        this.handleToastMessage("Success", "Task Deleted Successfully!!");
                    })
                    .catch((err) => {
                        console.log(err);
                        this.showSpinner = false;
                    });
            })
            .catch((err) => {
                this.showSpinner = false;
                this.handleError(err);
                this.isShowDeleteTaskModal = false;
            });
    }
    handleCancelDeleteTaskModal() {
        this.isShowDeleteTaskModal = false;
    }

    handleFieldChange(event) {
        var Id = event.currentTarget.dataset.id;
        if (event.target.name == "dueDate") {
            this.fieldChangeTrackMap[Id].Due_Date__c = event.target.value;
        }
        if (event.target.name == "Sequence") {
            this.fieldChangeTrackMap[Id].Sequence__c = event.target.value;
        }
    }
    handleSendNotificationClick(event) {
        this.selectedScheduleId = event.target.dataset.csid;
        showEmailModal = true;
    }

    handleChange(event) {
        if (event.target.name == "emailToAddresses") {
            this.emailToAddresses = event.target.value;
        } else if (event.target.name == "emailSubject") {
            this.emailSubject = event.target.value;
        } else if (event.target.name == "emailBody") {
            this.emailBody = event.target.value;
        } else if (event.target.name == "attendeeStatusOption") {
            this.attendeeStatusOption = event.target.value;
            this.getToAddresses();
        }
    }

    getToAddresses() {
        this.showSpinner = true;
        doInit({
                selectedScheduleId: this.selectedScheduleId,
                attendeeStatusOption: this.attendeeStatusOption
            })
            .then((response) => {
                this.showAttendeeStatusOptions = response.showAttendeeStatusOptions;
                if (response.emails && response.emails.length > 0) {
                    this.emailToAddresses = response.emails.join(",");
                }
                this.showEmailModal = true;
            })
            .catch((error) => {
                console.log("error occurred: " + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            })
            .finally(() => {
                this.showSpinner = false;
            });
    }

    handleSendNotificationClick(event) {
        if (this.selectedScheduleId) {
            this.getToAddresses();
        } else {
            this.showEmailModal = true;
        }
    }

    handleSendEmailClick(event) {
        let allValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);

        allValid += [
            ...this.template.querySelectorAll("lightning-textarea")
        ].reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);

        this.emailBodyValidity = true;
        if (!this.emailBody || this.emailBody.trim().length == 0) {
            console.log("emailbody is blank");
            this.emailBodyValidity = false;
            allValid = false;
        }

        if (allValid) {
            let hasInvalidEmail = false;
            let emails = this.emailToAddresses.split(",");
            for (var i = 0; i < emails.length; i++) {
                if (!this.validateEmail(emails[i])) {
                    hasInvalidEmail = true;
                    break;
                }
            }

            if (hasInvalidEmail) {
                this.showToast("Error", "Please enter valid email address.", "error");
                return false;
            }

            this.showSpinner = true;
            sendEmail({
                    selectedScheduleId: this.selectedScheduleId,
                    emailAddresses: this.emailToAddresses,
                    emailSubject: this.emailSubject,
                    emailBody: this.emailBody
                })
                .then((response) => {
                    this.showToast("Success", "Email sent successfully.", "success");
                })
                .catch((error) => {
                    console.log("error occurred: " + JSON.stringify(error));
                    showAjaxErrorMessage(this, error);
                })
                .finally(() => {
                    this.showSpinner = false;
                });
        } else {
            this.showToast("Error", "Please fill all the require fields.", "error");
        }
    }

    validateEmail(email) {
        return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            email
        );
    }

    handleModalCancelClick(event) {
        this.showEmailModal = false;
        this.emailSubject = "";
        this.emailBody = "";
    }

    showToast(title, message, varient) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: varient
        });
        this.dispatchEvent(event);
    }
    navigateToReport() {
        this[NavigationMixin.Navigate]({
            type: "standard__objectPage",
            attributes: {
                objectApiName: "Report",
                actionName: "list"
            },
            state: {
                filterName: "everything"
            }
        });
    }
}