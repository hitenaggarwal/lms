import { LightningElement, api } from 'lwc';
import fetchHelpText from '@salesforce/apex/LMS_CourseDetailsController.fetchHelpText';


export default class LmsCourseDetailsHelpText extends LightningElement {
    @api courseId;
    
    helpText = '';
    

    connectedCallback() {
        this.loadHelpText();
    }

    loadHelpText() {
        console.log('Course Id : ' + this.courseId);
        fetchHelpText({
            courseId: this.courseId
        })
        .then(helpMessage => {
            console.log('Help Message : ' + helpMessage);
            if(helpMessage !== null) {
                this.helpText = helpMessage;
            }
        })
        .catch(error => { 
            console.log('Error Message : ' + JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        });
    } 
}

// SELECT Id, Number_of_Launches__c FROM User_Module_Association__c WHERE Course_Module__c IN (SELECT Id FROM Course_Module__c WHERE Course_Catalogue__c = 'a004x0000035840AAA')
// SELECT Id, Help_Text__c FROM Course_Catalogue__c WHERE Id = 'a004x0000035840AAA'