import { LightningElement, api, track } from "lwc";
import fetchCourse from "@salesforce/apex/LMS_CourseDetailsController.fetchCourseDetails";
import updateOverviewField from "@salesforce/apex/LMS_CourseDetailComponentController.updateOverviewField";
import updateObjectiveField from "@salesforce/apex/LMS_CourseDetailComponentController.updateObjectiveField";
import getAllModules from "@salesforce/apex/LMS_CourseDetailComponentController.getAllModules";
import getCourseEnrollment from "@salesforce/apex/LMS_CourseDetailComponentController.getCourseEnrollment";

export default class LmsCourseDetailsComponent extends LightningElement {
  @api recordId = "";

  @track course;

  overviewButtonText = "Edit";
  objectiveButtonText = "Edit";
  showSpinner = true;
  cmpLoaded = false;
  overviewEdit = false;
  objectiveEdit = false;
  overviewValue = "";
  objectiveValue = "";
  modules = [];
  enrollment = [];
  noModulesFound = false;
  noEnrollmentFound = false;
  modulesExist = false;
  @track showSubmitModal = false;
  showViewModal = false;
  userEnrollId;

  connectedCallback() {
    let sURL = window.location.href;
    let paramValue = sURL.split("courseId=")[1];
    console.log("paramValue" + paramValue);
    // const param = "courseId";
    // const paramValue1 = this.getUrlParamValue(window.location.href, param);
    // console.log("paramValue" + paramValue1);
    // var url = new URL(url_string);
    // var courseId = url.searchParams.get("courseId");
    this.recordId = paramValue;
    this.loadCourseDetails();
  }

  loadCourseDetails() {
    this.showSpinner = true;
    fetchCourse({
      courseId: this.recordId
    })
      .then((data) => {
        console.log(JSON.stringify(data));
        this.course = data;
        this.overviewValue = this.course.courseSummary;
        this.objectiveValue = this.course.courseObjective;
      })
      .catch((error) => {
        console.log("Error >>> " + JSON.stringify(error));
      })
      .finally(() => {
        this.showSpinner = false;
        this.cmpLoaded = true;
      });

    getCourseEnrollment({
      recordId: this.recordId
    })
      .then((data) => {
        console.log("Course Enrollment Data:" + JSON.stringify(data));
        this.enrollment = data;
        if (data.length > 0) {
          this.noEnrollmentFound = false;
          this.enrollmentExist = true;
        } else {
          this.noEnrollmentFound = true;
        }
      })
      .catch((err) => {
        console.log(err);
      });

    getAllModules({
      recordId: this.recordId
    })
      .then((data) => {
        console.log("DATAAAA:" + data);
        this.modules = data;
        if (data.length > 0) {
          this.noModulesFound = false;
          this.modulesExist = true;
        } else {
          this.noModulesFound = true;
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  onOverviewEdit() {
    if (this.overviewButtonText == "Edit") {
      this.overviewButtonText = "Save";
      this.overviewEdit = true;
    } else {
      this.overviewButtonText = "Edit";
      this.overviewEdit = false;

      updateOverviewField({
        recordId: this.recordId,
        summary: this.overviewValue
      })
        .then((data) => {
          console.log("Saved");
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  onObjectiveEdit() {
    if (this.objectiveButtonText == "Edit") {
      this.objectiveButtonText = "Save";
      this.objectiveEdit = true;
    } else {
      this.objectiveButtonText = "Edit";
      this.objectiveEdit = false;

      updateObjectiveField({
        recordId: this.recordId,
        objective: this.objectiveValue
      })
        .then((data) => {
          console.log("Saved");
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }
  handleOverviewChange(event) {
    var field = event.target.name;
    if (field === "overview") {
      this.overviewValue = event.target.value;
      console.log("overviewValue" + this.overviewValue);
    }
  }
  handleObjectiveChange(event) {
    var field = event.target.name;
    if (field === "objective") {
      this.objectiveValue = event.target.value;
      console.log("objectiveValue" + this.objectiveValue);
    }
  }

  // handleSubmitEvaluation(event) {
  //   this.showSubmitModal = true;
  //   this.userEnrollId = event.target.dataset.id;
  // }

  // handleViewEvaluation(event) {
  //   this.showViewModal = true;
  //   this.userEnrollId = event.target.dataset.id;
  // }

  // closeModal() {
  //   this.showSubmitModal = false;
  //   this.showViewModal = false;
  // }

  // handleSubmitStatusChange(event) {
  //   this.showSubmitModal = event.detail;
  // }
}