import { LightningElement, track, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import createCourseModuleRecord from "@salesforce/apex/LMSCourseModuleController.createCourseModuleRecord";
import createCourseModuleVersionRecord from "@salesforce/apex/LMSCourseModuleController.createCourseModuleVersionRecord";
import fetchCourseModuleRecords from "@salesforce/apex/LMSCourseModuleController.fetchCourseModuleRecords";
import fetchCourseModule from "@salesforce/apex/LMSCourseModuleController.fetchCourseModule";
import doDeleteRecord from "@salesforce/apex/LMSCourseModuleController.doDeleteRecord";

export default class LmsCourseModule extends LightningElement {
  @api recordId;
  @track showSpinner = false;
  @track componentLoaded = false;
  @track dataObj = {};
  @track isNoCMRecords = true;
  @track isShowRecordModal = false;
  @track isShowQuizModal = false;
  @track quizModalCourseModuleId;
  @track fields = {};

  courseModuleId;
  isModuleEditable = false;
  showVideoURL = false;
  showAudioURL = false;
  showPdfURL = false;
  showQuizField = false;
  showSCORMURL = false;
  loading = false;
  moduleType = "";

  @track isShowDeleteModal = false;
  @track deleteHeaderItemName = "";
  @track deleteItemMessage = "";
  @track recordIdForDelete = "";
  @track recordObjectForDelete = "";

  connectedCallback() {
    this.componentLoaded = false;
    this.handleLoadAllCourseModules();
  }
  handleLoadAllCourseModules() {
    this.showSpinner = true;
    fetchCourseModuleRecords({ courseCatalogRecordId: this.recordId })
      .then((result) => {
        if (result.cmWrapper != null && result.cmWrapper.length > 0) {
          this.dataObj = result.cmWrapper;
          this.isNoCMRecords = false;
        } else {
          this.isNoCMRecords = true;
        }
        this.showSpinner = false;
        this.componentLoaded = true;
      })
      .catch((error) => {
        this.handleError(error);
        this.showSpinner = false;
        this.componentLoaded = true;
      });
  }

  handleAddCM(event) {
    this.moduleType = null;
    this.showHideFieldsOnBasisOfModuleType();
    this.fields = {};
    this.isShowRecordModal = true;
  }

  handleCancel(event) {
    this.moduleType = null;
    this.showHideFieldsOnBasisOfModuleType();
    this.courseModuleId = null;
    this.isShowRecordModal = false;
  }

  handleToastMessage(toastType, message) {
    var variant;
    toastType == "Success"
      ? (variant = "success")
      : toastType == "Error"
      ? (variant = "error")
      : (variant = "");
    const event = new ShowToastEvent({
      title: toastType,
      message: message,
      variant: variant,
      mode: "dismissable"
    });
    this.dispatchEvent(event);
  }

  handleError(error) {
    var errorMessage;
    if (Array.isArray(error.body)) {
      errorMessage = error.body.map((e) => e.message).join(", ");
    } else if (typeof error.body.message === "string") {
      errorMessage = error.body.message;
    }
    this.handleToastMessage("Error", errorMessage);
  }

  showSuccessToast() {
    const event = new ShowToastEvent({
      title: "Success!!!",
      message: "Module successfully Added",
      variant: "success",
      mode: "dismissable"
    });
    this.dispatchEvent(event);
  }
  showErrorToast() {
    const event = new ShowToastEvent({
      title: "Error Occured!!",
      message: "Failed to Add Module",
      variant: "error",
      mode: "dismissable"
    });
    this.dispatchEvent(event);
  }

  showHideFieldsOnBasisOfModuleType() {
    if (this.moduleType == "Audio") {
      this.showVideoURL = false;
      this.fields.Video_URL__c = null;
      this.showAudioURL = true;
      this.showPdfURL = false;
      this.fields.PDF_URL__c = null;
      this.showQuizField = false;
      this.fields.Passing_Score__c = null;
      this.fields.Max_Attempts__c = null;
      this.showSCORMURL = false;
      this.fields.SCORM_URL__c = null;
    } else if (this.moduleType == "Video") {
      this.showVideoURL = true;
      this.showAudioURL = false;
      this.fields.Audio_URL__c = null;
      this.showPdfURL = false;
      this.fields.PDF_URL__c = null;
      this.showQuizField = false;
      this.fields.Passing_Score__c = null;
      this.fields.Max_Attempts__c = null;
      this.showSCORMURL = false;
      this.fields.SCORM_URL__c = null;
    } else if (this.moduleType == "Quiz") {
      this.showVideoURL = false;
      this.fields.Video_URL__c = null;
      this.showAudioURL = false;
      this.fields.Audio_URL__c = null;
      this.showPdfURL = false;
      this.fields.PDF_URL__c = null;
      this.showQuizField = true;
      this.showSCORMURL = false;
      this.fields.SCORM_URL__c = null;
    } else if (this.moduleType == "PDF") {
      this.showVideoURL = false;
      this.fields.Video_URL__c = null;
      this.showAudioURL = false;
      this.fields.Audio_URL__c = null;
      this.showPdfURL = true;
      this.showQuizField = false;
      this.fields.Passing_Score__c = null;
      this.fields.Max_Attempts__c = null;
      this.showSCORMURL = false;
      this.fields.SCORM_URL__c = null;
    } else if(this.moduleType == "SCORM"){
      this.showVideoURL = false;
      this.fields.Video_URL__c = null;
      this.showAudioURL = false;
      this.fields.Audio_URL__c = null;
      this.showPdfURL = false;
      this.fields.PDF_URL__c = null;
      this.showQuizField = false;
      this.fields.Passing_Score__c = null;
      this.fields.Max_Attempts__c = null;
      this.showSCORMURL = true;
    }else{
      this.showVideoURL = false;
      this.fields.Video_URL__c = null;
      this.showAudioURL = false;
      this.fields.Audio_URL__c = null;
      this.showPdfURL = false;
      this.fields.PDF_URL__c = null;
      this.showQuizField = false;
      this.fields.Passing_Score__c = null;
      this.fields.Max_Attempts__c = null;
      this.showSCORMURL = false;
      this.fields.SCORM_URL__c = null;
    }
  }

  //START - Course Module Record Modal Inputs
  handleChangeModuleName(event) {
    this.fields.Module_Name__c = event.target.value;
  }
  handleChangeStatus(event) {
    this.fields.Status__c = event.target.value;
  }
  handleChangeType(event) {
    this.fields.Type__c = event.target.value;
    this.moduleType = event.target.value;
    this.showHideFieldsOnBasisOfModuleType();
  }
  handleChangeVideoURL(event) {
    this.fields.Video_URL__c = event.target.value;
  }
  handleChangeAudioURL(event) {
    this.fields.Audio_URL__c = event.target.value;
  }
  handleChangePDFURL(event) {
    this.fields.PDF_URL__c = event.target.value;
  }
  handleChangeSCORMURL(event){
    this.fields.SCORM_URL__c = event.target.value;
  }
  handleChangePassingScore(event) {
    this.fields.Passing_Score__c = event.target.value;
  }
  handleChangeMaxAttempts(event) {
    this.fields.Max_Attempts__c = event.target.value;
  }
  //END - Course Schedule Record Modal Inputs

  @api handleCourseModuleSubmit(event) {}
  @api handleCourseModuleSuccess(event) {
    //===TO DO==Create Module Version Record===
    var moduleName = event.detail.fields.Name.value;
    createCourseModuleVersionRecord({ moduleName: moduleName })
      .then((result) => {
        console.log("Course Module Version Created");
      })
      .catch((error) => console.log(error));
    this.handleLoadAllCourseModules();
    //console.log(JSON.stringify(event.detail));
  }
  @api handleCourseModuleError(event) {
    //console.log(JSON.stringify(event.detail));
  }

  handleSave(event) {
    this.fields.Course_Catalogue__c = this.recordId;
    this.template.querySelector(".CourseModuleFormCls").submit(this.fields);
    this.moduleType = null;
    this.showHideFieldsOnBasisOfModuleType();
    this.courseModuleId = null;
    this.isShowRecordModal = false;
  }
  editModule(event) {
    var editRecordId = event.target.dataset.rid;
    this.courseModuleId = editRecordId;

    this.moduleType = event.target.dataset.rmoduletype;
    this.showHideFieldsOnBasisOfModuleType();

    this.fields = {};
    this.isShowRecordModal = true;
  }

  deleteModule(event) {
    this.deleteHeaderItemName = "Course Module";
    this.deleteItemMessage =
      "Are you sure you want to delete <b>" +
      event.target.dataset.rmodulename +
      "</b> Course Module?";
    this.recordIdForDelete = event.target.dataset.rid;
    this.recordObjectForDelete = "Course_Module__c";
    this.isShowDeleteModal = true;
  }
  handleCancelDeleteRecord(event) {
    this.isShowDeleteModal = false;
    this.deleteHeaderItemName = "";
    this.deleteItemMessage = "";
    this.recordIdForDelete = "";
    this.recordObjectForDelete = "";
  }
  handleDeleteRecord(event) {
    if (
      this.recordIdForDelete != "" &&
      this.recordIdForDelete != null &&
      this.recordIdForDelete != undefined &&
      this.recordObjectForDelete != "" &&
      this.recordObjectForDelete != null &&
      this.recordObjectForDelete != undefined
    ) {
      this.showSpinner = true;
      doDeleteRecord({
        recordIdForDelete: this.recordIdForDelete,
        recordObjectForDelete: this.recordObjectForDelete
      })
        .then((result) => {
          if (this.recordObjectForDelete == "Course_Module__c") {
            this.handleLoadAllCourseModules();
          }
          this.handleCancelDeleteRecord();
          this.showSpinner = false;
        })
        .catch((error) => {
          this.handleError(error);
          this.showSpinner = false;
        });
    }
  }

  handleShowQuizModal(event){
    this.quizModalCourseModuleId = event.target.dataset.rid;
    this.isShowQuizModal = true;
  }

  handleCloseQuizModal(event){
    this.quizModalCourseModuleId = null;
    this.isShowQuizModal = false;
  }
}