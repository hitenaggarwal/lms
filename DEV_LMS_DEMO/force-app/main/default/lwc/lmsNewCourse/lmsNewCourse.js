import { LightningElement, track, api } from "lwc";

import fetchCourseCatalogue from "@salesforce/apex/LMSCourseCatalogueController.fetchCourseCatalogue";
import createCourseCatalogueRecord from "@salesforce/apex/LMSCourseCatalogueController.createNewCourseRecord";
import fetchPicklist from "@salesforce/apex/LMS_FetchPicklistController.fetchPicklist";

import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class LmsNewCourse extends LightningElement {
  @api recordId;
  @track showSpinner = false;
  @track componentLoaded = false;
  @track dataObj = {};
  roles = [];
  competency = []
  isRecurringChecked = false;
  isOJTChecked = false;
  @track recurringDateOptions = [];
  @track recurringMonthOptions = [];
  @track recurringYearOptions = [];

  connectedCallback() {
    this.componentLoaded = false;
    this.showSpinner = true;
    fetchCourseCatalogue({ courseCatalogRecordId: this.recordId })
      .then((result) => {
        //console.log("Fetched Result >>> " + JSON.stringify(result));
        if (result != null && result != "" && result != undefined) {
          this.dataObj = result;
        }
        this.showSpinner = false;
        this.componentLoaded = true;
        if (this.dataObj.Recurring__c === true) {
          this.isRecurringChecked = true;
        }
        if(this.dataObj.On_Job_Training__c === true){
          this.isOJTChecked = true;
        }
        this.roles = this.dataObj.Roles__c.split(";");
        this.competency = this.dataObj.Competency__c.split(";");
      })
      .catch((error) => {
        this.handleError(error);
        this.showSpinner = false;
        this.componentLoaded = true;
      });
    this.fetchPicklistOptions();
  }

  get options() {
    return [
      { label: "Business Analyst", value: "Business Analyst" },
      { label: "Developer", value: "Developer" },
      { label: "HR", value: "HR" }
    ];
  }

  get competencyOptions() {
    return [
      { label: "Business Analyst", value: "Business Analyst" },
      { label: "Developer", value: "Developer" },
      { label: "HR", value: "HR" }
    ];
  }

  get selected() {
    return this.roles.length ? this.roles : "none";
  }

  handleInputs(event) {
    if (event.target.name === "Name") {
      this.dataObj.Name = event.target.value;
    } else if (event.target.name === "Description__c") {
      this.dataObj.Description__c = event.target.value;
    } else if (event.target.name === "Objective__c") {
      this.dataObj.Objective__c = event.target.value;
    } else if (event.target.name === "Recurring__c") {
      this.dataObj.Recurring__c = event.target.checked;
      if (event.target.checked === true) {
        this.isRecurringChecked = true;
      } else {
        this.isRecurringChecked = false;
        this.dataObj.Recurring_Duration_In_Days__c = "";
        this.dataObj.Reoccurence__c = null;
        this.dataObj.Recurring_Month__c = null;
        this.dataObj.Recurring_Year__c = null;
      }
    } else if (event.target.name === "Require_Supervisor_Approval__c") {
      this.dataObj.Require_Supervisor_Approval__c = event.target.checked;
    } else if (event.target.name === "Recurring_Duration_In_Days__c") {
      this.dataObj.Recurring_Duration_In_Days__c = event.target.value;
    } else if (event.target.name === "Reoccurence__c") {
      this.dataObj.Reoccurence__c = event.target.value;
    } else if (event.target.name === "Recurring_Month__c") {
      this.dataObj.Recurring_Month__c = event.target.value;
    } else if (event.target.name === "Recurring_Year__c") {
      this.dataObj.Recurring_Year__c = event.target.value;
    }else if (event.target.name === "On_Job_Training__c") {
      this.dataObj.On_Job_Training__c = event.target.checked;
      if (event.target.checked === true) {
        this.isOJTChecked = true;
      } else {
        this.isOJTChecked = false;
        this.dataObj.Competency__c = null;
       
      }
    } 
    //console.log(JSON.stringify(this.dataObj));
  }

  handleRoleChange(event) {
    this.roles = event.detail.value;
    //console.log(this.roles);
    var rolesString = this.roles.join();
    //console.log(rolesString);
    this.dataObj.Roles__c = rolesString.replaceAll(",", ";");
    //console.log(this.dataObj.Roles__c);
  }
  handleCompetencyChange(event) {
    this.competency = event.detail.value;
    //console.log(this.roles);
    var competencyString = this.competency.join();
    //console.log(rolesString);
    this.dataObj.Competency__c = competencyString.replaceAll(",", ";");
    //console.log(this.dataObj.Roles__c);
  }

  @api
  isValid(callback) {
    let valid = false;
    this.showSpinner = true;

    valid = [
      ...this.template.querySelectorAll("lightning-input"),
      ...this.template.querySelectorAll("lightning-textarea"),
      ...this.template.querySelectorAll("lightning-combobox"),
      ...this.template.querySelectorAll("lightning-radio-group"),
      ...this.template.querySelectorAll("lightning-dual-listbox")
    ].reduce((validSoFar, input) => {
      input.reportValidity();
      return validSoFar && input.checkValidity();
    }, true);

    if (valid) {
      createCourseCatalogueRecord({
        courseRecord: JSON.stringify(this.dataObj)
      })
        .then((result) => {
          let successMsg = "Course Created Successfully";
          if (this.recordId) {
            successMsg = "Course Updated Successfully";
          }
          this.handleToastMessage("Success", successMsg);

          this.showSpinner = false;
          callback({
            valid: true,
            data: result
          });
        })
        .catch((error) => {
          this.handleError(error);
          this.showSpinner = false;
        });
    } else {
      this.handleToastMessage("Error", "Please review the errors.");
      this.showSpinner = false;
      callback({ valid: false });
    }
  }

  handleToastMessage(toastType, message) {
    var variant;
    toastType == "Success"
      ? (variant = "success")
      : toastType == "Error"
      ? (variant = "error")
      : (variant = "");
    const event = new ShowToastEvent({
      title: toastType,
      message: message,
      variant: variant,
      mode: "dismissable"
    });
    this.dispatchEvent(event);
  }

  handleError(error) {
    var errorMessage;
    if (Array.isArray(error.body)) {
      errorMessage = error.body.map((e) => e.message).join(", ");
    } else if (typeof error.body.message === "string") {
      errorMessage = error.body.message;
    }
    this.handleToastMessage("Error", errorMessage);
  }

  fetchPicklistOptions() {
    fetchPicklist({
      objectName: "Course_Catalogue__c",
      fieldName: "Reoccurence__c"
    }).then((result) => {
      this.recurringDateOptions = result;
    });

    fetchPicklist({
      objectName: "Course_Catalogue__c",
      fieldName: "Recurring_Month__c"
    }).then((result) => {
      this.recurringMonthOptions = result;
    });

    fetchPicklist({
      objectName: "Course_Catalogue__c",
      fieldName: "Recurring_Year__c"
    }).then((result) => {
      this.recurringYearOptions = result;
    });
    fetchPicklist({
      objectName: "Course_Catalogue__c",
      fieldName: "Competency__c"
    }).then((result) => {
      console.log(JSON.stringify(result));
    });
  }
}