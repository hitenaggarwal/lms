import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import doInit from '@salesforce/apex/LMS_ManageAnnouncementsController.doInit';
import getAnnouncement from '@salesforce/apex/LMS_ManageAnnouncementsController.getAnnouncement';
import saveAnnouncement from '@salesforce/apex/LMS_ManageAnnouncementsController.saveAnnouncement';
import deleteAnnouncement from '@salesforce/apex/LMS_ManageAnnouncementsController.deleteAnnouncement';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { showAjaxErrorMessage } from 'c/lmsUtils';

export default class LmsManageAnnouncements extends LightningElement {

    @track showSpinner = false;
    @track showEditAnnouncementModal = false;
    @track showDeleteAnnouncementModal = false;

    @track editAnnouncementModalTitle = 'New Announcement';
    @track announcementRTValidiy = true;

    @track statusOption = '';
    @track statusOptions = [];

    @track typeOption = '';
    @track typeOptions = [];

    @track courseCatalogueOption = '';
    @track courseCatalogueOptions = [];

    @track announcements = [];

    @track announcement = {};
    @track announcementId = '';

    get showCourseCatalogueDropdown(){
        return this.announcement.Type__c == "Course";
    }

    connectedCallback(){
        this.loadAnnouncements();
    }

    loadAnnouncements(){
        this.showSpinner = true;
        doInit({
        }).then( response => {
            this.typeOptions = response.typeOptions;
            this.statusOptions = response.statusOptions;
            this.courseCatalogueOptions = response.courseCatalogueOptions;

            this.announcements = JSON.parse( JSON.stringify(response.announcements) ); 
            this.announcements.forEach(announcement => {
                announcement.menu_edit_index = 'edit__'+announcement.Id;
                announcement.menu_delete_index = 'delete__'+announcement.Id;
            });          
        }).catch(error=>{
            console.log('error occurred: '+JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        }).finally(()=>{
            this.showSpinner = false;
        });
    }

    handleChange(event){
        if( event.target.name == "announcementTitle" ){
            this.announcement.Title__c = event.target.value;
        }else if( event.target.name == "announcementRichText" ){
            this.announcement.Announcement__c = event.target.value;
        }else if( event.target.name == "announcementDate" ){
            this.announcement.Date__c = event.target.value;
        }else if( event.target.name == "announcementExpiryDate" ){
            this.announcement.Expiry_Date__c = event.target.value;
        }else if( event.target.name == "announcementStatus" ){
            this.announcement.Status__c = event.target.value;
        }else if( event.target.name == "announcementType" ){
            this.announcement.Type__c = event.target.value;
            if( event.target.value == "General" ){
                this.announcement.Course_Catalogue__c = null;
            }
        }else if( event.target.name == "announcementCourseCatalogue" ){
            this.announcement.Course_Catalogue__c = event.target.value;
        }
    }

    handleNewAnnouncementClick(event){
        this.announcement = {};
        this.announcementRTValidiy = true;
        this.editAnnouncementModalTitle = 'New Announcement';
        this.showEditAnnouncementModal = true;
    }

    handleAnnouncementAction( event ){
        const selectedValue = event.detail.value;
        let arr = selectedValue.split("__");
        if( arr[0] == 'edit' ){
            //this.announcementId = arr[1];
            //this.announcement = this.announcements.find( announcement => announcement.Id == arr[1] );
            this.showSpinner = true;
            getAnnouncement({
                announcementId : arr[1]
            }).then( response => { 
                this.announcement = response;
                this.announcementRTValidiy = true;
                this.showEditAnnouncementModal = true;
                this.editAnnouncementModalTitle = 'Edit Announcement';         
            }).catch(error=>{
                console.log('error occurred: '+JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            }).finally(()=>{
                this.showSpinner = false;
            });
            
        }else if( arr[0] == 'delete' ){
            //this.announcementId = arr[1];
            this.announcement = this.announcements.find( announcement => announcement.Id == arr[1] );
            this.showDeleteAnnouncementModal = true;
        }
    }

    handleEditAnnouncementModalSave(event){
        let allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        allValid += [...this.template.querySelectorAll('lightning-textarea')]
        .reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);

        allValid += [...this.template.querySelectorAll('lightning-combobox')]
        .reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);

        if( allValid ){
            this.showSpinner = true;
            saveAnnouncement({
                announcementJSON : JSON.stringify( this.announcement )
            }).then( response => { 
                this.showToast("Success", "Announcement \""+this.announcement.Title__c+"\" was saved.", "success");
                this.announcement = {};
                this.showEditAnnouncementModal = false;
                this.announcements = JSON.parse( JSON.stringify(response.announcements) ); 
                this.announcements.forEach(announcement => {
                    announcement.menu_edit_index = 'edit__'+announcement.Id;
                    announcement.menu_delete_index = 'delete__'+announcement.Id;
                });  
            }).catch(error=>{
                console.log('error occurred: '+JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            }).finally(()=>{
                this.showSpinner = false;
            });
        }else{
            this.showToast( 'Error', 'Please fill all the required fields.', 'error' );
        }
    }

    handleEditAnnouncementModalCancel(event){
        this.showEditAnnouncementModal = false;
        this.announcement = {};
    }

    handleDeleteAnnouncementModalDelete(event){
        this.showSpinner = true;
        deleteAnnouncement({
            announcementId : this.announcement.Id
        }).then( response => {
            this.announcements = JSON.parse( JSON.stringify(response.announcements) ); 
            this.announcements.forEach(announcement => {
                announcement.menu_edit_index = 'edit__'+announcement.Id;
                announcement.menu_delete_index = 'delete__'+announcement.Id;
            });  
            this.showDeleteAnnouncementModal = false;  
            this.showToast("Success", "Announcement \""+this.announcement.Title__c+"\" was deleted.", "success");
            this.announcement = {};   
        }).catch(error=>{
            console.log('error occurred: '+JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        }).finally(()=>{
            this.showSpinner = false;
        });
    }

    handleDeleteAnnouncementModalCancel(event){
        this.showDeleteAnnouncementModal = false;
        this.announcement = {};
    }

    showToast(title, message, varient) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: varient
        });
        this.dispatchEvent(event);
    }
}