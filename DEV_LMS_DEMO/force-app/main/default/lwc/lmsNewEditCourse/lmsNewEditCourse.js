import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import LMS_CommunityURL from "@salesforce/label/c.LMS_CommunityURL";

export default class LmsNewEditCourse extends LightningElement {

    LMS_CommunityURL = LMS_CommunityURL;
    @track showSpinner = false;
    @track componentLoaded = false;
    @api recordId;
    @track newEditCourseHeading = 'New Course';
    @track flowSteps = [
        {stepNumber: 1, className: 'active', component: 'c-lms-new-course'}, 
        {stepNumber: 2, className: '', component: ''}, 
        {stepNumber: 3, className: '', component: ''},
        {stepNumber: 4, className: '', component: ''}];
    @track isShowStep1 = false;
    @track isShowStep2 = false;
    @track isShowStep3 = false;
    @track isShowStep4 = false;
    @track currentStep = 1;

    hideAllSteps(){
        this.isShowStep1 = false;
        this.isShowStep2 = false;
        this.isShowStep3 = false;
        this.isShowStep4 = false;
    }
    stepsCalculation(){
        this.hideAllSteps();
        let cStep = this.currentStep;
        let fSteps = this.flowSteps;
        for(var i=1; fSteps.length >= i; i++){
            var flowIndex = i-1;
            if(cStep == i){
                fSteps[flowIndex].className = 'active';
            }else if(cStep > i){
                fSteps[flowIndex].className = 'completed';
            }else if(cStep < i){
                fSteps[flowIndex].className = '';
            }
        }
        this.flowSteps = fSteps;
        if(cStep == 1){
            this.isShowStep1 = true;
        }else if(cStep == 2){
            this.isShowStep2 = true;
        }else if(cStep == 3){
            this.isShowStep3 = true;
        }else if(cStep == 4){
            this.isShowStep4 = true;
        }
    }

    connectedCallback() {
        this.currentStep = 1;
        this.stepsCalculation();
        if(this.recordId){
            this.newEditCourseHeading = 'Edit Course';
        }else{
            this.newEditCourseHeading = 'New Course';
        }
        this.componentLoaded = true;
    }

    handleCancel(event){
        window.location = LMS_CommunityURL + '/s/course-management';
    }

    handleBack(event){
        this.currentStep = this.currentStep - 1;
        this.stepsCalculation();
        window.scrollTo(0, 0);
    }

    handleNext(event){
        let currentFlowStep;
        let currentFlowStepCmp;
        currentFlowStep = (this.flowSteps)[this.currentStep - 1];
        if(currentFlowStep.component != null && currentFlowStep.component != '' && currentFlowStep.component != undefined){
            currentFlowStepCmp = this.template.querySelector(currentFlowStep.component);
            currentFlowStepCmp.isValid(response => {
                if (response.valid) {
                    if (response.data) {
                        if(this.currentStep == 1){
                            this.recordId = response.data;
                            this.currentStep = this.currentStep + 1;
                            this.stepsCalculation();
                        }
                    }
                }
            });
        }else{
            this.currentStep = this.currentStep + 1;
            this.stepsCalculation();
        }
        window.scrollTo(0, 0);
    }

}