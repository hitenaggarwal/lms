import { LightningElement, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import fetchEventsWrapper from '@salesforce/apex/LMS_MyDashboardEventsCtrl.fetchEventsWrapper';

export default class LmsMyDashboardEvents extends LightningElement {
    
    @track showSpinner = false;
    @track componentLoaded = false;
    @track dataObj = {};

    connectedCallback() {
        this.componentLoaded = false;
        this.showSpinner = true;
        fetchEventsWrapper().then(result => {
            if(result != null && result != '' && result != undefined){
                this.dataObj = result;
            }
            this.showSpinner = false;
            this.componentLoaded = true;
        }).catch(error => {
            this.handleError(error);
            this.showSpinner = false;
            this.componentLoaded = true;
        });
    }

    handleEventNavigation(event){
        let navURL = event.currentTarget.dataset.navurl;
        window.location = navURL;
    }

    handleToastMessage(toastType, message){
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error": variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
          });
        this.dispatchEvent(event);
    }

    handleError(error){
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }

}