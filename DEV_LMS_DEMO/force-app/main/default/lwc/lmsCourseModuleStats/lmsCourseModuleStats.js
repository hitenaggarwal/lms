import { LightningElement ,track} from 'lwc';
import getCourseCountDetails from '@salesforce/apex/lmsCourseModuleStatsController.getCourseCountDetails';
import { NavigationMixin } from 'lightning/navigation';
export default class LmsCourseModuleStats extends NavigationMixin(LightningElement) {
    @track courses='';
    
    connectedCallback(){
        getCourseCountDetails().then((data)=>{
            
            console.log('>>>>>data'+JSON.stringify(data));
            this.courses = (data);
            console.log('>>>>>courses'+this.courses);

        }).catch((error)=>{
            console.log('>>>>>error'+JSON.stringify(error));
        });
    }

    navigateToLmsCourseCatalogueAdminDetailsDraft() {

        console.log('>>>>in navigation');
       
       
        var urlToNavigate = 'https://dhs-lms.force.com/s/course-catalogue-admin-details?status='+'Draft';
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            
            type: 'standard__webPage',
            attributes: {
                url: urlToNavigate
            },
        });
    }
    navigateToLmsCourseCatalogueAdminDetailsPublished() {

        console.log('>>>>in navigation');
       
       
        var urlToNavigate = 'https://dhs-lms.force.com/s/course-catalogue-admin-details?status='+'Published';
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            
            type: 'standard__webPage',
            attributes: {
                url: urlToNavigate
            },
        });
    }
    navigateToLmsCourseCatalogueAdminDetailsArchived() {

        console.log('>>>>in navigation');
       
       
        var urlToNavigate = 'https://dhs-lms.force.com/s/course-catalogue-admin-details?status='+'Archived';
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            
            type: 'standard__webPage',
            attributes: {
                url: urlToNavigate
            },
        });
    }
    navigateToLmsCourseModuleAdminDetailsDraft() {

        console.log('>>>>in navigation');
       
       
        var urlToNavigate = 'https://dhs-lms.force.com/s/course-module-admin-details?status='+'Draft';
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            
            type: 'standard__webPage',
            attributes: {
                url: urlToNavigate
            },
        });
    }
    navigateToLmsCourseModuleAdminDetailsPublished() {

        console.log('>>>>in navigation');
       
       
        var urlToNavigate = 'https://dhs-lms.force.com/s/course-module-admin-details?status='+'Published';
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            
            type: 'standard__webPage',
            attributes: {
                url: urlToNavigate
            },
        });
    }
    navigateToLmsUserModuleAdminDetailsCompleted() {

        console.log('>>>>in navigation');
       
       
        var urlToNavigate = 'https://dhs-lms.force.com/s/user-module-admin-details?status='+'Completed';
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            
            type: 'standard__webPage',
            attributes: {
                url: urlToNavigate
            },
        });
    }
    navigateToLmsUserModuleAdminDetailsIncomplete() {

        console.log('>>>>in navigation');
       
       
        var urlToNavigate = 'https://dhs-lms.force.com/s/user-module-admin-details?status='+'Incomplete';
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            
            type: 'standard__webPage',
            attributes: {
                url: urlToNavigate
            },
        });
    }
    navigateToLmsUserModuleAdminDetailsOverdue() {

        console.log('>>>>in navigation');
       
       
        var urlToNavigate = 'https://dhs-lms.force.com/s/user-module-admin-details?status='+'Overdue';
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
            
            type: 'standard__webPage',
            attributes: {
                url: urlToNavigate
            },
        });
    }
    
    
}