import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import fetchCourseRelations from '@salesforce/apex/LMS_CoursePreRequisitesViewController.fetchCourseRelations';

export default class LmsCoursePreRequisitesView extends LightningElement {
    @api recordId;
    @track showSpinner = false;
    @track componentLoaded = false;

    @track isNoCPRRecords = true;
    @track dataObj = {};

    connectedCallback() {
        this.componentLoaded = false;
        this.getData();
    }

    getData() {
        this.showSpinner = true;
        fetchCourseRelations({
            courseId: this.recordId
        })
        .then(result => {
            this.courseRequisitesList = [];
            if(result.crWrapper != null && result.crWrapper.length > 0){
                this.dataObj = result.crWrapper;
                this.isNoCPRRecords = false;
            }else{
                this.isNoCPRRecords = true;
            }
            this.showSpinner = false;
            this.componentLoaded = true;
        })
        .catch(error => {
            this.handleError(error);
            this.showSpinner = false;
            this.componentLoaded = true;
        });
    }

    handleToastMessage(toastType, message){
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error": variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
          });
        this.dispatchEvent(event);
    }

    handleError(error){
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }
}