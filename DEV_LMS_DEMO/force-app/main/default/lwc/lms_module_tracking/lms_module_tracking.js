import { LightningElement, track } from "lwc";

export default class Lms_module_tracking extends LightningElement {
  @track
  mapData = [];
  connectedCallback() {
    this.mapData.push({ value: '42', key: 'Planned' });
    this.mapData.push({ value: '14', key: 'In Progress' });
    this.mapData.push({ value: '44', key: 'Complete' });
  }
}