import { LightningElement, api } from 'lwc';

export default class LmsLearnerEvaluation extends LightningElement {
  @api
  courseId;
  grade="98.21 %";
  evaluationNotes = "Some Notes from the instructor";

}