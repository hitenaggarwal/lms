import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getCourseCatalogues from '@salesforce/apex/LMS_AttendanceManagerController.getCourseCatalogues';
import getCourseSchedules from '@salesforce/apex/LMS_AttendanceManagerController.getCourseSchedules';
import getCourseEnrollments from '@salesforce/apex/LMS_AttendanceManagerController.getCourseEnrollments';
import saveAttendance from '@salesforce/apex/LMS_AttendanceManagerController.saveAttendance';


import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { showAjaxErrorMessage } from 'c/lmsUtils';

export default class LmsAttendanceManager extends LightningElement {

    @track showSpinner = false;

    @track courseCatalogueOption = '';
    @track courseCatalogueOptions = [];

    @track courseScheduleOption = '';
    @track courseScheduleOptions = [];

    @track courseEnrollments = [];

    @track noCourseEnrollments = false;
    
    get showScheduleOptions(){
        return this.courseCatalogueOption.trim().length > 0;
    }

    get showEnrollments(){
        return this.courseEnrollments.length > 0 || this.noCourseEnrollments ;
    }

    attendanceOptions = []

    connectedCallback(){
        this.fetchCourseCatalogues();
    }

    handleChange( event ){
        if( event.target.name == 'courseCatalogueOption' ){
            this.courseCatalogueOption = event.target.value;
            this.courseScheduleOption = '';
            this.courseEnrollments = [];
            this.noCourseEnrollments = false;
            this.fetchCourseSchedules( this.courseCatalogueOption );
        }else if( event.target.name == 'courseScheduleOption' ){
            this.courseScheduleOption = event.target.value;
            this.courseEnrollments = [];
            this.noCourseEnrollments = false;
            this.fetchCourseEnrollments( this.courseScheduleOption );
        }else if( event.target.name == 'attendance' || event.target.dataset.type == "attendance-radio-group" ){
            let index = event.target.dataset.index;
            this.courseEnrollments[index].userAttendance['Attendance__c'] = event.target.value;
        }
    }

    fetchCourseCatalogues(){
        this.showSpinner = true;
        getCourseCatalogues({
        }).then(
            response => {
                this.courseCatalogueOptions = response.courseCatalogueOptions;
                this.attendanceOptions = response.attendanceOptions;
            }
        ).catch(error=>{
            console.log('error occurred: '+JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        }).finally(()=>{
            this.showSpinner = false;
        });
    }

    fetchCourseSchedules( courseCatalogueId ){
        this.showSpinner = true;
        getCourseSchedules({ 
            courseCatalogueId : courseCatalogueId
        }).then(
            response => {
                this.courseScheduleOptions = response.courseScheduleOptions;
            }
        ).catch(error=>{
            console.log('error occurred: '+JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        }).finally(()=>{
            this.showSpinner = false;
        });
    }

    fetchCourseEnrollments( courseScheduleId ){
        this.showSpinner = true;
        getCourseEnrollments({ 
            courseScheduleId : courseScheduleId
        }).then(
            response => {
                this.noCourseEnrollments = false;
                this.courseEnrollments = response.courseEnrollments;
                if( this.courseEnrollments.length == 0 ){
                    this.noCourseEnrollments = true;
                }
            }
        ).catch(error=>{
            console.log('error occurred: '+JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        }).finally(()=>{
            this.showSpinner = false;
        });
    }

    handleSaveAttendanceClick( event ){
        let allValid = [...this.template.querySelectorAll('lightning-radio-group')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if( allValid && !this.noCourseEnrollments ){
            this.showSpinner = true;
            saveAttendance({ 
                jsonUserEnrollments : JSON.stringify( this.courseEnrollments )
            }).then(
                response => {
                    this.courseEnrollments = [];
                    this.courseEnrollments = response.courseEnrollments;
                    this.showToast( 'Success', 'Attendance saved successfully.', 'success' );
                }
            ).catch(error=>{
                console.log('error occurred: '+JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            }).finally(()=>{
                this.showSpinner = false;
            });
        }else{
            this.showToast( 'Error', 'Please mark attendance for all users.', 'error' );
        }
    }

    showToast(title, message, varient) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: varient
        });
        this.dispatchEvent(event);
    }
}