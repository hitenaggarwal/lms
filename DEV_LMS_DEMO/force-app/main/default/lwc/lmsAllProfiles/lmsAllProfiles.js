import { LightningElement, track, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getContact from "@salesforce/apex/LMS_AllProfileController.getContact";
import getProfileInfo from "@salesforce/apex/LMS_AllProfileController.getProfileInfo";
import getCertificationDetail from "@salesforce/apex/LMS_UserCertificationController.getCertificationDetail";
import getCertificationsList from "@salesforce/apex/LMS_AllProfileController.getCertificationsList";
import getUserComplianceList from "@salesforce/apex/LMS_AllProfileController.getUserCompliance";
import getUserComplianceDetails from "@salesforce/apex/LMS_AllProfileController.getUserComplianceDetails"
import upsertUserCompliance from "@salesforce/apex/LMS_AllProfileController.upsertUserCompliance"
import upsertCertification from "@salesforce/apex/LMS_AllProfileController.upsertCertification";
import updateAboutMe from "@salesforce/apex/LMS_UserCertificationController.updateAboutMe";
import getAllProfiles from "@salesforce/apex/LMS_AllProfileController.getAllProfiles";
import getAllEvents from "@salesforce/apex/LMS_AllProfileController.getAllEvents"
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import FullCalendarJS from '@salesforce/resourceUrl/FullCalendarJS';
import deleteUserCompliance from '@salesforce/apex/LMS_AllProfileController.deleteUserCompliance'
import deleteCertification from '@salesforce/apex/LMS_AllProfileController.deleteCertification'
import getUserEnrollmentList from '@salesforce/apex/LMS_AllProfileController.getUserEnrollmentList'
export default class LmsAllProfile extends LightningElement {
  contactId;
  imgUrl;
  uploadedImageName;
  aboutMe = "";
  editOrSaveLabel = "Edit";
  isModalOpen = false;
  isComplianceModalOpen = false;
  isEditable = false;
  isCertificateEditable = false;
  isComplianceEditable = false;
  isImageUploaded = false;

  isCalendarVisible=false;
  fullCalendarJsInitialised = false;
  eventArray = [];
  
  @track obj = {};
  @api recordId;
  @track showSpinner = false;
  @track showTotalHours = false;
  @track certificationObj = {};
  @track ComplianceObj = {};
  @track certificationsList = [];
  @track userComplianceList = [];
  @track userEnrollmentList = [];
  @track profileOption = '';
  @track profileOptions = [];
  
  get options() {
        return [
            { label: 'None', value: 'None' },
            { label: 'Approved', value: 'Approved' },
            { label: 'Pending', value: 'Pending' },
            { label: 'Rejected', value: 'Rejected' },
        ];
    }
  callCalendar() {
    // Executes all loadScript and loadStyle promises
    // and only resolves them once all promises are done
    Promise.all([
      loadScript(this, FullCalendarJS + "/FullCalendarJS/jquery.min.js"),
      loadScript(this, FullCalendarJS + "/FullCalendarJS/moment.min.js"),
      loadScript(this, FullCalendarJS + "/FullCalendarJS/fullcalendar.min.js"),
      loadStyle(this, FullCalendarJS + "/FullCalendarJS/fullcalendar.min.css"),
    ])
      .then(() => {
        
        //this.initialiseFullCalendarJs();
        // Initialise the calendar configuration

      })
      .catch(error => {
        // eslint-disable-next-line no-console
        console.error({
          message: 'Error occured on FullCalendarJS',
          error
        });
      })
      this.fetchEvents();
  }

  /**
   * @description Initialise the calendar configuration
   *              This is where we configure the available options for the calendar.
   *              This is also where we load the Events data.
   */
  initialiseFullCalendarJs() {
    console.log("Event Array >> ",this.eventArray);
    var ele = this.template.querySelector('div.fullcalendarjs');
    console.log("ELE BEfore >>>",ele);
    // eslint-disable-next-line no-undef
    $(ele).fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      displayEventTime: true,
      defaultDate: new Date(),
      // defaultDate: new Date(), // default day is today
      navLinks: true, // can click day/week names to navigate views
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      events: this.eventArray,
    });
    console.log("ELE After >>>",ele);
  }
  //Fetch All events of Contact Course Schedule for calender
  fetchEvents(){
    this.eventArray=[];
    getAllEvents({contactId : this.contactId}).then((data)=>{
        data.map(course=>{
           this.eventArray.push({title:course.courseScheduleName,start : course.courseScheduleStartDate,end:course.courseScheduleEndDate ,color:'#696969',url:course.courseScheduleId});
         })
        this.initialiseFullCalendarJs();
    }).catch(error => {
        console.log(' Error Occured ', error);
    })
  }


  connectedCallback() {
    this.fetchAllProfiles(); 
  }
  fetchAllProfiles(){
    getAllProfiles({}).then(response => {
            this.profileOptions = response.profileOptions; 
        }
    ).catch(error=>{
        console.log('error occurred: '+JSON.stringify(error));
    })
  }
  handleChange(event){
    this.contactId = event.target.value;
    console.log("Contact Id :>>>",this.contactId);
    this.getContact();
    this.fetchCertifications();  
    this.callCalendar();
    this.fetchUserCompliance();
    this.fetchUserEnrollment();
  }
  handleInputChange(event) {
    this.obj[event.target.name] = event.detail.value;
    this.certificationObj[event.target.name] = event.detail.value;
    this.ComplianceObj[event.target.name] = event.detail.value;
  }
  handleSave() {
    const event = new ShowToastEvent({
      title: "Success!",
      message: "Changes have been saved!!",
      variant: "Success"
    });
    this.dispatchEvent(event);
    window.location.reload();
  }

  getContact() {
    getContact({contactId: this.contactId})
      .then((result) => {
        this.obj = JSON.parse(JSON.stringify(result));
        if(this.obj.trainingDurationInHours !=null && this.obj.trainingDurationInHours !=undefined && this.obj.trainingDurationInHours !== "0mins"){
            this.showTotalHours = true;
        }
        else{
            this.showTotalHours = false;
        }
        this.fetchProfileInfo();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }
  /* -- ACCEPTED IMAGE FORMATS
  -- */
  get acceptedFormats() {
    return [".jpg", ".jpeg", ".png"];
  }
  /* -- HANDLE PROFILE IMAGE AND ABOUT ME CONTENT DISPLAY/EDIT
  -- */
  fetchProfileInfo() {
    getProfileInfo({ contactId: this.contactId })
      .then((result) => {
        this.aboutMe = result.aboutMe;
        this.imgUrl =
          "/sfc/servlet.shepherd/version/download/" + result.contentVersionId;
        console.log("About Me" + JSON.stringify(result));
        this.uploadedImageName = result.fileName;
      })
      .catch((error) => {
        console.log('getProfileInfo Error',JSON.stringify(error));
      });
  }

  handleProfileEdit() {
    this.isEditable = !this.isEditable;
    this.editOrSaveLabel = this.isEditable === true ? "Save" : "Edit";

    if (this.isEditable === false) {
      updateAboutMe({ contactId: this.contactId, aboutMe: this.aboutMe })
        .then((result) => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Success",
              message: "Updated Successfully!!",
              variant: "success"
            })
          );
        window.location.reload();
        })
        .catch((error) => {
          console.log("Error: " + JSON.stringify(error.message));
        });
    }
  }

  handleCancelEdit(event) {
    this.isEditable = false;
    this.editOrSaveLabel = "Edit";
  }

  handleAboutMeChange(event) {
    console.log("About Me >>> " + event.target.value);
    this.aboutMe = event.target.value;
  }

  handleUploadFinished(event) {
    console.log("Image Uploaded");
    var file = event.detail.files;
    this.uploadedImageName = file[0].name;
    this.isImageUploaded = true;
  }
  /* -- HANDLE CERTIFICATION CONTENT DISPLAY/EDIT
  -- */
  fetchCertifications() {
    getCertificationsList({contactId:this.contactId})
      .then((result) => {
        this.certificationsList = result;
        console.log("CERTIFICATIONS >>> " + JSON.stringify(result));
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }
  /* -- HANDLE User Compliance Status CONTENT DISPLAY/EDIT
  -- */
  fetchUserCompliance() {
    getUserComplianceList({contactId:this.contactId})
      .then((result) => {
        this.userComplianceList = result;
        console.log("userComplianceList Result >>> " + JSON.stringify(result));
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }
  openAddCertificationModal(event) {
    this.isModalOpen = true;
  }
  openAddComplianceModal(event) {
    this.isComplianceModalOpen = true;
  }
  handleAddCertification(event) {
    upsertCertification({
      certificationString: JSON.stringify(this.certificationObj),
      isUpdate : false,
      contactId:this.contactId,
    })
      .then((result) => {
        this.isModalOpen = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Success",
            message: "New certification added!!",
            variant: "success"
          })
        );
        window.location.reload();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }

  handleAddCompliance(event) {
    upsertUserCompliance({
        complianceString: JSON.stringify(this.ComplianceObj),
        isUpdate: false,
        contactId: this.contactId,
    })
      .then((result) => {
         
        this.isComplianceModalOpen = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Success",
            message: "New User Compliance added!!",
            variant: "success"
          })
        );
        window.location.reload();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }
  closeAddCertificationModal(event) {
    this.isModalOpen = false;
    this.isCertificateEditable = false;
  }
  closeAddUserComplianceModal(event) {
    this.isComplianceModalOpen = false;
    this.isComplianceEditable = false;
  }
  openCertificationEditModal(event) {
    const certificationId = event.target.dataset.id;
    getCertificationDetail({ certificateId: certificationId })
      .then((result) => {
        this.isModalOpen = true;
        this.isCertificateEditable = true;
        this.certificationObj = result;
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }

  openComplianceEditModal(event) {
    const userComplianceId = event.target.dataset.id;
    console.log('openComplianceEditModal userComplianceId>>>',userComplianceId);
    getUserComplianceDetails({ userComplianceId: userComplianceId })
      .then((result) => {
        this.isComplianceModalOpen = true;
        this.isComplianceEditable = true;
        this.ComplianceObj = result;
      })
      .catch((error) => {
        console.log(error);
      });
  }


  handleEditCertification(event) {
    upsertCertification({
      certificationString: JSON.stringify(this.certificationObj),
      isUpdate: true,
      contactId : this.contactId,
    })
      .then((result) => {
        this.isModalOpen = false;
        this.isCertificateEditable = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Success",
            message: "Certification updated!!",
            variant: "success"
          })
        );
       window.location.reload();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }

  handleEditCompliance(event) {
    upsertUserCompliance({
      complianceString: JSON.stringify(this.ComplianceObj),
      isUpdate: true,
      contactId: this.contactId,
    })
      .then((result) => {
        this.isComplianceModalOpen = false;
        this.isComplianceEditable = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Success",
            message: "User Compliance updated!!",
            variant: "success"
          })
        );
       window.location.reload();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }

  deleteCertifiaction(event){
    const userComplianceId = event.target.dataset.id;
    deleteUserCompliance({ userComplianceId: userComplianceId 
      })
        .then((result) => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Deleted",
              message: "User Compliance Deleted!!",
              variant: "success"
            })
          );
         window.location.reload();
        })
        .catch((error) => {
          console.log(error);
        });
  }


  deleteCertification(event){
    const certificationId = event.target.dataset.id;
    deleteCertification({ certificationId: certificationId 
      })
        .then((result) => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Deleted",
              message: "Certifiacate Deleted!!",
              variant: "success"
            })
          );
        window.location.reload();
        })
        .catch((error) => {
          console.log(error);
        });
  }
  fetchUserEnrollment(){
    getUserEnrollmentList({contactId: this.contactId }
        ).then((result) => {
            this.userEnrollmentList = result;
          })
          .catch((error) => {
            console.log(error);
          });
  }
}