import { LightningElement,track } from 'lwc';
import fetchCourseRelations from '@salesforce/apex/LMS_CoursePreRequisitesViewController.fetchCourseRelations';
export default class LmsCourseModulePreRequisites extends LightningElement {
    
    @track showSpinner = false;
    @track componentLoaded = false;

    @track isNoCPRRecords = true;
    @track dataObj = {};

    connectedCallback() {
        let sURL = window.location.href;
     this.recordid = sURL.split("courseId=")[1];
        this.componentLoaded = false;
        this.getData();
    }

    getData() {
        this.showSpinner = true;
        fetchCourseRelations({
            courseId: this.recordid
        })
        .then(result => {
            this.courseRequisitesList = [];
            if(result.crWrapper != null && result.crWrapper.length > 0){
                this.dataObj = result.crWrapper;
                this.isNoCPRRecords = false;
            }else{
                this.isNoCPRRecords = true;
            }
            this.showSpinner = false;
            this.componentLoaded = true;
        })
        .catch(error => {
            this.handleError(error);
            this.showSpinner = false;
            this.componentLoaded = true;
        });
    }

}