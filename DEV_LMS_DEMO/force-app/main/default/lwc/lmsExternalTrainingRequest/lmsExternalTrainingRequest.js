import { LightningElement, track } from "lwc";
import { NavigationMixin } from "lightning/navigation";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getTrainingRequests from "@salesforce/apex/LMS_ExternalTrainingRequestController.getTrainingRequests";
import updateExternalTrainingRequest from "@salesforce/apex/LMS_ExternalTrainingRequestController.updateExternalTrainingRequest";
import createExternalTrainingRequest from "@salesforce/apex/LMS_ExternalTrainingRequestController.createExternalTrainingRequest";
import deleteExternalTrainingRequest from "@salesforce/apex/LMS_ExternalTrainingRequestController.deleteExternalTrainingRequest";

export default class LmsExternalTrainingRequest extends NavigationMixin(
  LightningElement
) {
  showTrainingRequestForm = false;
  isSubmitDisabled = true;
  isFormUploaded = false;
  recordId;
  uploadedFormId;
  uploadedFileName;
  @track trainingRequests;
  @track trainingRequestObj = {};

  /* -- 
    Show External Request Form
   */
  openTrainingRequestForm(event) {
    this.showTrainingRequestForm = true;
    createExternalTrainingRequest()
      .then((result) => {
        this.recordId = result;
        console.log(result);
      })
      .catch((error) => {
        console.log("Error >> " + error);
      });
  }

  connectedCallback() {
    this.trainingRequestObj.status = "Draft";
    this.fetchTrainingRequests();
  }
  handleInputChange(event) {
    this.trainingRequestObj.recordId = this.recordId;
    this.trainingRequestObj[event.target.name] = event.target.value;
  }
  /* -- 
    Fetching Records to display in table
  --*/
  fetchTrainingRequests() {
    getTrainingRequests()
      .then((result) => {
        this.trainingRequests = result;
      })
      .catch((error) => {
        console.log("Error >> " + error);
      });
  }
  /* -- 
    Navigate to External Request Detail Page
  --*/
  navigateToTrainingRequestPage(event) {
    let recordId = event.currentTarget.dataset.id;

    this[NavigationMixin.Navigate]({
      type: "standard__recordPage",
      attributes: {
        recordId: recordId,
        objectApiName: "External_Training_Request__c",
        actionName: "view"
      }
    });
  }
  /* -- FORM UPLOAD FUNCTIONS ----
   */
  get acceptedFormats() {
    return [".pdf"];
  }

  handleUploadFinished(event) {
    var file = event.detail.files;
    this.uploadedFileName = file[0].name;
    this.uploadedFormId = file[0].documentId;
    this.isFormUploaded = true;
    this.isSubmitDisabled = false;
  }
  /* -- 
    Navigate to Download Form-186
   */
  downloadForm() {
    this[NavigationMixin.Navigate]({
      type: "standard__webPage",
      attributes: {
        url: "https://www.opm.gov/forms/pdf_fill/sf182.pdf"
      }
    });
  }
  /* -- 
    Submit External Training Request
   */
  createTrainingRequest() {
    updateExternalTrainingRequest({
      trainingRequest: JSON.stringify(this.trainingRequestObj)
    })
      .then((result) => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Success",
            message: "Your External Training Request Submitted",
            variant: "success"
          })
        );
        this.showTrainingRequestForm = false;
        window.location.reload();
      })
      .catch((error) => {
        console.log("Error>>" + error.getMessage());
      });
  }
  /* -- 
    Cancel Submitting External Training Request
   */
  cancelTrainingRequest() {
    console.log("here >>> " + this.recordId);
    deleteExternalTrainingRequest({ trainingReqId: this.recordId })
      .then((result) => {
        console.log("Record deleted");
        this.showTrainingRequestForm = false;
      })
      .catch((error) => {
        console.log("Error >> " + error);
      });
  }
}