import { LightningElement, api, track, wire } from 'lwc';
import fetchCourse from '@salesforce/apex/LMS_CourseDetailsController.fetchCourseDetails';
import fetchLaunchDetails from '@salesforce/apex/LMS_CourseDetailsController.fetchLaunchDetails';
import { showAjaxErrorMessage } from 'c/lmsUtils';
import { NavigationMixin } from 'lightning/navigation';
import { getUrlParam } from 'c/lmsUtils';

export default class LmsCourseDetails extends NavigationMixin(LightningElement) {

    @api recordId;

    @track course;

    showSpinner = false;
    cmpLoaded = false;
    launchStatus = false;

    connectedCallback() {
        this.loadCourseDetails();
    }

    loadCourseDetails() {
        this.showSpinner = true;
        fetchCourse({
            courseId: this.recordId
        })
            .then(data => {
                this.course = data;
            })
            .catch(error => {
                showAjaxErrorMessage(this, error);
            })
            .finally(() => {
                this.showSpinner = false;
                this.cmpLoaded = true;
            });

        fetchLaunchDetails({
            courseId: this.recordId
        })
        .then(launchCount => {
            console.log('Launch Count : ' + launchCount);
            if(launchCount === 0) {
                this.launchStatus = true;
            }
        })
        .catch(error => {
            showAjaxErrorMessage(this, error);
        });
    }
}