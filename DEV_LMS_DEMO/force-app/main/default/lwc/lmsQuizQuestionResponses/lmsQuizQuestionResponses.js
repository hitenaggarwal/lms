import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import fetchQuizQuestionResponseRecords from '@salesforce/apex/LMSQuizQuestionResponsesController.fetchQuizQuestionResponseRecords';
import doDeleteRecord from '@salesforce/apex/LMSQuizQuestionResponsesController.doDeleteRecord';

export default class LmsQuizQuestionResponses extends LightningElement {

    @api questionId;
    @track showSpinner = false;
    @track componentLoaded = false;
    @track dataObj = {};
    @track isNoQuestionResponseRecords = true;

    @track fields = {};
    @track isShowQuestionResponseRecordModal = false;
    @track questionResponseRecordId;

    @track isShowDeleteModal = false;
    @track deleteHeaderItemName = '';
    @track deleteItemMessage = '';
    @track recordIdForDelete = '';
    @track recordObjectForDelete = '';

    connectedCallback() {
        this.componentLoaded = false;
        this.handleLoadQuestionResponses();
    }

    handleLoadQuestionResponses(){
        this.showSpinner = true;
        fetchQuizQuestionResponseRecords({ questionId: this.questionId })
        .then((result) => {
            if(result.quesResWrapper != null && result.quesResWrapper.length > 0){
                this.dataObj = result.quesResWrapper;
                this.isNoQuestionResponseRecords = false;
            }else{
                this.isNoQuestionResponseRecords = true;
            }
            this.showSpinner = false;
            this.componentLoaded = true;
        })
        .catch((error) => {
            this.handleError(error);
            this.showSpinner = false;
            this.componentLoaded = true;
        });
    }

    handleQuestionResponseRecordEdit(event){
        this.fields = {};
        this.questionResponseRecordId = event.target.dataset.rid;
        this.isShowQuestionResponseRecordModal = true;
    }

    handleAddQuestionResponse(event){
        this.fields = {};
        this.questionResponseRecordId = null;
        this.isShowQuestionResponseRecordModal = true;
    }
    handleQuestionResponseCancelModal(event){
        this.questionResponseRecordId = null;
        this.isShowQuestionResponseRecordModal = false;
    }

    //START - Question Response Record Modal Inputs
    handleChangeSequence(event){
        this.fields.Sequence__c = event.target.value;
    }
    handleChangeIsCorrect(event){
        this.fields.Is_Correct__c = event.target.value;
    }
    handleChangeDescription(event){
        this.fields.Description__c = event.target.value;
    }
    //END - Question Response Record Modal Inputs


    @api handleQuestionResponseSubmit(event) {
        
    }
    @api handleQuestionResponseSuccess(event) {
        this.questionResponseRecordId = null;
        this.isShowQuestionResponseRecordModal = false;
        this.handleLoadQuestionResponses();
        //console.log(JSON.stringify(event.detail));
    }
    @api handleQuestionResponseError(event) {
        //console.log(JSON.stringify(event.detail));
        var errors = event.detail;
        this.handleToastMessage("Error", errors.detail);
    }


    handleQuestionResponseRecordDelete(event){
        this.deleteHeaderItemName = 'Question Response';
        this.deleteItemMessage = 'Are you sure you want to delete this Question Response?';
        this.recordIdForDelete = event.target.dataset.rid;
        this.recordObjectForDelete = 'Question_Response__c';
        this.isShowDeleteModal = true;
    }
    handleCancelDeleteRecord(event){
        this.isShowDeleteModal = false;
        this.deleteHeaderItemName = '';
        this.deleteItemMessage = '';
        this.recordIdForDelete = '';
        this.recordObjectForDelete = '';
    }
    handleDeleteRecord(event){
        if(this.recordIdForDelete != '' && this.recordIdForDelete != null && this.recordIdForDelete != undefined && 
        this.recordObjectForDelete != '' && this.recordObjectForDelete != null && this.recordObjectForDelete != undefined){
            this.showSpinner = true;
            doDeleteRecord({
                recordIdForDelete : this.recordIdForDelete, 
                recordObjectForDelete : this.recordObjectForDelete
            })
            .then(result => {
                this.handleLoadQuestionResponses();
                this.handleCancelDeleteRecord();
                this.showSpinner = false;
            }).catch(error => {
                this.handleError(error);
                this.showSpinner = false;
            });
        }
    }

    handleQuestionResponseSaveModal(event){
        this.fields.Question__c = this.questionId;
        this.template.querySelector('.QuestionResponseFormCls').submit(this.fields);
    }

    handleToastMessage(toastType, message){
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error": variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
          });
        this.dispatchEvent(event);
    }

    handleError(error){
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }
}