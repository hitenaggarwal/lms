import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import doCourseEnroll from '@salesforce/apex/LMS_CourseEnrollBannerController.doCourseEnroll';

export default class LmsCourseEnrollBanner extends LightningElement {
    @api courseId;
    @track showSpinner = false;

    handleEnroll(event) {
        //alert('Enroll to course: '+this.courseId);
        this.showSpinner = true;
        doCourseEnroll({ courseId: this.courseId })
        .then((result) => {
            window.location.reload();
        })
        .catch((error) => {
            this.handleError(error);
            this.showSpinner = false;
        });
    }

    handleToastMessage(toastType, message){
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error": variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
          });
        this.dispatchEvent(event);
    }

    handleError(error){
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }
}