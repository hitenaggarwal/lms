import { LightningElement,api ,track} from 'lwc';
import getCourseDetails from '@salesforce/apex/LmsModuleFacilitieController.getCourseDetails';

export default class LmsCourseModuleFacilitiesDetails extends LightningElement {
   
    @track courses='';
    
    



    connectedCallback(){
        
        getCourseDetails().then((data)=>{
           
           
            this.courses = data;
            

        }).catch((error)=>{
            console.log('>>>>>error'+JSON.stringify(error));
        });
    }
}