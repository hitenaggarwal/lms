import { LightningElement,track } from 'lwc';
import getTrainingPlans from "@salesforce/apex/LMSTrainingPlanController.fetchTrainingPlanWrapper";
export default class LmsTrainingPlans extends LightningElement {
    @track trainingRequests;
    connectedCallback() {
        this.fetchTrainingPlans();
      }
      fetchTrainingPlans() {
        getTrainingPlans()
          .then((result) => {
            this.trainingRequests = result;
            console.log(JSON.stringify(this.trainingRequests));
          })
          .catch((error) => {
            console.log("Error >> " + error);
          });
      }
}