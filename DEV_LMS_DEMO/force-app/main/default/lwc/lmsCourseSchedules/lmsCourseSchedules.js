import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import fetchCourseScheduleRecords from '@salesforce/apex/LMSCourseSchedulesController.fetchCourseScheduleRecords';
import doDeleteRecord from '@salesforce/apex/LMSCourseSchedulesController.doDeleteRecord';

export default class LmsCourseSchedules extends LightningElement {

    @api recordId;
    @track showSpinner = false;
    @track componentLoaded = false;
    @track dataObj = {};
    @track isNoCSRecords = true;

    @track fields = {};
    @track courseScheduleRecordId;
    @track isShowCourseScheduleRecordModal = false;

    @track isShowCourseRoleModal = false;
    @track courseSchduleIdForRoles;

    @track isShowDeleteModal = false;
    @track deleteHeaderItemName = '';
    @track deleteItemMessage = '';
    @track recordIdForDelete = '';
    @track recordObjectForDelete = '';
    
    connectedCallback() {
        this.componentLoaded = false;
        this.handleLoadAllCourseSchedules();
    }

    handleLoadAllCourseSchedules(){
        this.showSpinner = true;
        fetchCourseScheduleRecords({ courseCatalogRecordId: this.recordId })
        .then((result) => {
            if(result.csWrapper != null && result.csWrapper.length > 0){
                this.dataObj = result.csWrapper;
                this.isNoCSRecords = false;
            }else{
                this.isNoCSRecords = true;
            }
            this.showSpinner = false;
            this.componentLoaded = true;
        })
        .catch((error) => {
            this.handleError(error);
            this.showSpinner = false;
            this.componentLoaded = true;
        });
    }

    handleAddCS(event){
        this.fields = {};
        this.courseScheduleRecordId = null;
        this.isShowCourseScheduleRecordModal = true;
    }

    handleCourseSchduleRecordEdit(event){
        this.fields = {};
        let instructorId = event.target.dataset.insid;
        if(instructorId != null && instructorId != '' && instructorId != undefined){
            this.fields.Instructor__c = instructorId;
        }
        this.courseScheduleRecordId = event.target.dataset.rid;
        this.isShowCourseScheduleRecordModal = true;
    }

    handleCourseScheduleCancelModal(event){
        this.courseScheduleRecordId = null;
        this.isShowCourseScheduleRecordModal = false;
    }

    handleCourseScheduleSaveModal(event){
        var isValid = true;
        if(this.fields.Instructor__c == null || this.fields.Instructor__c == '' || this.fields.Instructor__c == undefined){
            isValid = false;
        }
        if(isValid){
            this.fields.Course_Catalogue__c = this.recordId;
            this.template.querySelector('.CourseScheduleFormCls').submit(this.fields);
        }else{
            this.handleToastMessage("Error", "Please complete all required fields.");
        }
    }

    //START - Course Schedule Record Modal Inputs
    handleChangeSessionType(event){
        this.fields.Session_Type__c = event.target.value;
    }
    handleChangeAvailableSeats(event){
        this.fields.Available_Seats__c = event.target.value;
    }
    handleChangeStartDate(event){
        this.fields.Start_Date__c = event.target.value;
    }
    handleChangeEndDate(event){
        this.fields.End_Date__c = event.target.value;
    }
    handleChangeLocation(event){
        this.fields.Location__c = event.target.value;
    }
    handleChangeCanUserDropCourse(event){
        this.fields.Can_User_Drop_Course__c = event.target.value;
    }
    handleChangeInstructor(event){
        this.fields.Instructor__c = event.target.value;
    }
    handleChangeAutoEnroll(event){
        this.fields.Auto_Enroll__c = event.target.value;
    }
    handleChangeStatus(event){
        this.fields.Status__c = event.target.value;
    }
    //END - Course Schedule Record Modal Inputs


    @api handleCourseScheduleSubmit(event) {
        
    }
    @api handleCourseScheduleSuccess(event) {
        this.courseScheduleRecordId = null;
        this.isShowCourseScheduleRecordModal = false;
        this.handleLoadAllCourseSchedules();
        console.log(JSON.stringify(event.detail));
    }
    @api handleCourseScheduleError(event) {
        console.log(JSON.stringify(event.detail));
        var errors = event.detail;
        this.handleToastMessage("Error", errors.detail);
    }


    //START - Course Role Modal
    handleShowCourseRoleModal(event){
        this.courseSchduleIdForRoles = event.target.dataset.rid;
        this.isShowCourseRoleModal = true;
    }

    handleCourseRoleCancelModal(event){
        this.courseSchduleIdForRoles = null;
        this.isShowCourseRoleModal = false;
    }
    //END - Course Role Modal



    handleCourseSchduleRecordDelete(event){
        this.deleteHeaderItemName = 'Course Schedule';
        this.deleteItemMessage = 'Are you sure you want to delete this Course Schedule?';
        this.recordIdForDelete = event.target.dataset.rid;
        this.recordObjectForDelete = 'Course_Schedule__c';
        this.isShowDeleteModal = true;
    }
    handleCancelDeleteRecord(event){
        this.isShowDeleteModal = false;
        this.deleteHeaderItemName = '';
        this.deleteItemMessage = '';
        this.recordIdForDelete = '';
        this.recordObjectForDelete = '';
    }
    handleDeleteRecord(event){
        if(this.recordIdForDelete != '' && this.recordIdForDelete != null && this.recordIdForDelete != undefined && 
        this.recordObjectForDelete != '' && this.recordObjectForDelete != null && this.recordObjectForDelete != undefined){
            this.showSpinner = true;
            doDeleteRecord({
                recordIdForDelete : this.recordIdForDelete, 
                recordObjectForDelete : this.recordObjectForDelete
            })
            .then(result => {
                if(this.recordObjectForDelete == 'Course_Schedule__c'){
                    this.handleLoadAllCourseSchedules();
                }
                this.handleCancelDeleteRecord();
                this.showSpinner = false;
            }).catch(error => {
                this.handleError(error);
                this.showSpinner = false;
            });
        }
    }



    handleToastMessage(toastType, message){
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error": variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
          });
        this.dispatchEvent(event);
    }

    handleError(error){
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }

}