import { LightningElement } from "lwc";
import { NavigationMixin } from "lightning/navigation";
export default class LmsCertificationTemplates extends NavigationMixin(
  LightningElement
) {
  navigateToCertificateTemplate(event) {
    this[NavigationMixin.GenerateUrl]({
      type: "standard__webPage",
      attributes: {
        url: "/apex/LMS_CertificateTemplate"
      }
    }).then((generatedUrl) => {
      window.open(generatedUrl);
    });
  }
}