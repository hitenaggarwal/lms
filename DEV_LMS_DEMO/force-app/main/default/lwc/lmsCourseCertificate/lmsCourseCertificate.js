import { LightningElement, track, api } from 'lwc';
import lmsModule from '@salesforce/resourceUrl/ModuleIcon';
// import allcourseCompleted from '@salesforce/apex/LMS_Controller.allcourseCompleted';
// import validateCertitficateGenrated from '@salesforce/apex/LMS_Controller.validateCertitficateGenrated';
// import generatCertificate from '@salesforce/apex/LMS_Controller.generatCertificate';
export default class LmsCourseCertificate extends LightningElement {
    onlineinc = lmsModule + '/certificatebackground.jpg';
    @api course;

    // @track _userEnroll;
    // @track certificate;
    // @track overview;
    // @track error; // to show error message from apex controller.
    // @track success; // to show succes message in ui.
    // @track validateButton;
    // @track validateCertificate;
    // @track genratedCertificate;
    // @api courseid;
    // @api scheduledid;
    // @track userEnroll;


    // @api 
    // set userEnrollment(value){
    //     this.userEnroll = value;
    //     this.validateCertitifcation();
    // }

    // get userEnrollment(){
    //     return{};
    // }

    
    download(){       
        let navgateURL = '/apex/LMS_CertificateDownload?enrollmentId='+this.course.userEnrollmentId;
        window.open(navgateURL, '_blank');
    }

    // validateCertitifcation() {
    //     validateCertitficateGenrated({userEnroll : this.userEnroll})
    //         .then(result => {
    //             this.certificate = result;
    //             if(this.certificate.isCertificateGenrated === true){
    //                 this.validateButton = false;
    //             }else{
    //                 this.validateModules();
    //             }
    //             //console.log('result : ' + JSON.stringify(result));
    //             this.error = undefined;
    //         })
    //         .catch(error => {
    //             this.error = error;
    //             this.certificate = undefined;
    //         });
    // } 

    // validateModules () {
    //     allcourseCompleted({userEnroll : this.userEnroll})
    //         .then(result => {
    //             this.validateButton = result;
    //             this.error = undefined;
    //         })
    //         .catch(error => {
    //             this.error = error;
    //             this.certificate = undefined;
    //         });
    // }

    // genrateNewCertitifcate(){
    //     generatCertificate({userEnroll : this.userEnroll})
    //         .then(result => {
    //             this.certificate = result;
    //             this.error = undefined;
    //             this.validateButton = false;
    //         })
    //         .catch(error => {
    //             this.error = error;
    //             this.certificate = undefined;
    //         });
    // }
}