import { LightningElement, track } from 'lwc';
import getApprovals from '@salesforce/apex/lmsApprovalsController.getApprovals';
import approveRejectApprovals from '@salesforce/apex/lmsApprovalsController.approveRejectApprovals';
import getCourseRoles from '@salesforce/apex/LMS_CourseRoleApproval.getCourseRoles';
import saveCourseRoleData from '@salesforce/apex/LMS_CourseRoleApproval.saveCourseRoleData';

import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class LmsApprovals extends LightningElement {
    @track isLoading = false;
    @track internalPendingApprovals = [];
    @track externalPendingApprovals = [];
    @track internalPastApprovals = [];
    @track externalPastApprovals = [];
    @track courseRolePastApprovals = [];
    @track courseRolePendingApprovals = [];
    internalApprovals = [];
    externalApprovals = [];
    courseRoleApprovals = [];

    connectedCallback(){
        this.getInitialLoadData();
        this.getCourseRelatdData();
    }

    getInitialLoadData(){
        getApprovals()
        .then(result => {
            if(result){
                if(result.internalApprovalsList.length > 0){
                    result.internalApprovalsList.forEach(element => {
                        if(element.Status__c == 'Pending'){
                            this.internalPendingApprovals.push(element);
                        }
                        else{
                            this.internalPastApprovals.push(element);
                        }
                    });
                }
                if(result.externalApprovalsList.length > 0){
                    result.externalApprovalsList.forEach(element => {
                        if(element.Status__c == 'Pending'){
                            this.externalPendingApprovals.push(element);
                        }
                        else{
                            this.externalPastApprovals.push(element);
                        }
                    }); 
                }
            }
        })
        .catch(error => {
            this.isLoading = false;
            console.log('error-->' + JSON.stringify(error));
        });
    }

    updateApprovals(event){
        let action= event.target.dataset.name;
        let _isApprove = false;
        if(action ==='Approve'){
            _isApprove = true;
        }

        if( this.internalApprovals.length == 0 && this.externalApprovals.length == 0 && this.courseRoleApprovals.length == 0){
            this.dispatchEvent(
                new ShowToastEvent({
                    title: "",
                    variant: "error",
                    message: "Please select atleast one approval"
                })
            );
        }
        if(this.internalApprovals.length != 0 && this.externalApprovals.length != 0){
            approveRejectApprovals({
                internalApprovals : this.internalApprovals,
                externalApprovals : this.externalApprovals,
                isApprove : _isApprove
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: "",
                        variant: "success",
                        message: _isApprove?"Selected Approvals Approved Successfully":"Selected Approvals Rejected Successfully"
                    })
                );
                this.internalPendingApprovals = [];
                this.externalPendingApprovals = [];
                this.internalPastApprovals = [];
                this.externalPastApprovals = [];
                this.internalApprovals = [];
                this.externalApprovals = [];
                this.getInitialLoadData();
            })
            .catch(error => {
                this.isLoading = false;
                console.log('error-->' + JSON.stringify(error));
            });
        } 
        if(this.courseRoleApprovals.length != 0) {
            console.log(JSON.stringify(this.courseRoleApprovals));
            let status = '';
            if(action == 'Reject') {
                status = 'Rejected';
            } else {
                status= 'Approved';
            }
            saveCourseRoleData({
                ids : this.courseRoleApprovals,
                status : status
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: "",
                        variant: "success",
                        message: status == 'Approved'?"Selected Approvals Approved Successfully":"Selected Approvals Rejected Successfully"
                    })
                );
                this.getCourseRelatdData();
                this.courseRoleApprovals = [];
            })
            .catch(error => {
                this.isLoading = false;
                console.log('error-->' + JSON.stringify(error));
            });
        }
    }

    handleInputChange(event){
        let action= event.target.dataset.name;
        let _recordId = event.target.dataset.id;

        if(action === 'internal'){
            if(event.target.checked){
                this.internalApprovals.push(_recordId);
            }
            else{
                let index = this.internalApprovals.indexOf(_recordId);
                if (index > -1) {
                    this.internalApprovals.splice(index, 1);
                }
            }
        }
        else{
            if(event.target.checked){
                this.externalApprovals.push(_recordId);
            }
            else{
                let index = this.externalApprovals.indexOf(_recordId);
                if (index > -1) {
                    this.externalApprovals.splice(index, 1);
                }
            }
        }

        if(action === 'courseRole'){
            if(event.target.checked){
                this.courseRoleApprovals.push(_recordId);
            }
            else{
                let index = this.courseRoleApprovals.indexOf(_recordId);
                if (index > -1) {
                    this.courseRoleApprovals.splice(index, 1);
                }
            }
        }

    }

    navigateToDetails(event){
        let action= event.target.dataset.name;
        let _recordId = event.target.dataset.id;

        if(action === 'internal'){
            window.open('/s/user-compliance-status/'+_recordId);
        }
        else{
            window.open('/s/external-training-request/'+_recordId);
        }
    }


    getCourseRelatdData(){
        getCourseRoles()
        .then(result => {
            if(result){
                this.courseRolePendingApprovals = [];
                this.courseRolePastApprovals = [];
                result.forEach(element => {
                    if(element.Status__c == 'Pending') {
                      this.courseRolePendingApprovals.push(element);  
                    } else {
                        this.courseRolePastApprovals.push(element); 
                    }
                });
                console.log(JSON.stringify(this.courseRolePendingApprovals));
                console.log(JSON.stringify(this.courseRolePastApprovals));
            }
        })
        .catch(error => {
            this.isLoading = false;
            console.log('error1-->' + error);
        });
    }


}