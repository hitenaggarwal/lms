import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import fetchCourseRoleRecords from '@salesforce/apex/LMSCourseModuleRolesController.fetchCourseRoleRecords';
import doDeleteRecord from '@salesforce/apex/LMSCourseModuleRolesController.doDeleteRecord';

export default class LmsCourseModuleRoles extends LightningElement {

    @api recordId;
    @api courseScheduleId;
    @track showSpinner = false;
    @track componentLoaded = false;
    @track dataObj = {};
    @track isNoCRRecords = true;

    @track fields = {};
    @track isShowCourseRoleRecordModal = false;
    @track courseRoleRecordId;
    @track courseRoleRecordIdForDelete;

    @track isShowDeleteModal = false;
    @track deleteHeaderItemName = '';
    @track deleteItemMessage = '';
    @track recordIdForDelete = '';
    @track recordObjectForDelete = '';
    
    connectedCallback() {
        this.componentLoaded = false;
        this.handleLoadAllCourseRoles();
    }

    handleLoadAllCourseRoles(){
        this.showSpinner = true;
        fetchCourseRoleRecords({ courseScheduleRecordId: this.courseScheduleId })
        .then((result) => {
            if(result.crWrapper != null && result.crWrapper.length > 0){
                this.dataObj = result.crWrapper;
                this.isNoCRRecords = false;
            }else{
                this.isNoCRRecords = true;
            }
            this.showSpinner = false;
            this.componentLoaded = true;
        })
        .catch((error) => {
            this.handleError(error);
            this.showSpinner = false;
            this.componentLoaded = true;
        });
    }

    handleAddCR(event){
        this.fields = {};
        this.courseRoleRecordId = null;
        this.isShowCourseRoleRecordModal = true;
    }

    handleCourseRoleRecordEdit(event){
        this.fields = {};
        this.courseRoleRecordId = event.target.dataset.rid;
        this.isShowCourseRoleRecordModal = true;
    }

    handleCourseRoleCancelModal(event){
        this.courseRoleRecordId = null;
        this.isShowCourseRoleRecordModal = false;
    }

    handleCourseRoleSaveModal(event){
        this.fields.Course_Schedule__c = this.courseScheduleId;
        this.template.querySelector('.CourseRoleFormCls').submit(this.fields);
        this.courseRoleRecordId = null;
        this.isShowCourseRoleRecordModal = false;
    }

    //START - Course Role Record Modal Inputs
    handleChangeName(event){
        this.fields.Name = event.target.value;
    }
    handleChangeRole(event){
        this.fields.Role__c = event.target.value;
    }
    handleChangeContact(event){
        this.fields.Contact__c = event.target.value;
    }
    handleChangeShowAllData(event){
        this.fields.Show_All_Data__c = event.target.value;
    }
    handleChangeViewExamResult(event){
        this.fields.View_Exam_Result__c = event.target.value;
    }
    handleChangeStatus(event){
        this.fields.Status__c = event.target.value;
    }
    //END - Course Role Record Modal Inputs


    @api handleCourseRoleSubmit(event) {
        
    }
    @api handleCourseRoleSuccess(event) {
        this.handleLoadAllCourseRoles();
        //console.log(JSON.stringify(event.detail));
    }
    @api handleCourseRoleError(event) {
        //console.log(JSON.stringify(event.detail));
    }

    handleCourseRoleRecordDelete(event){
        this.deleteHeaderItemName = 'Course Role';
        this.deleteItemMessage = 'Are you sure you want to delete this Course Role?';
        this.recordIdForDelete = event.target.dataset.rid;
        this.recordObjectForDelete = 'Course_Module_Role__c';
        this.isShowDeleteModal = true;
    }
    handleCancelDeleteRecord(event){
        this.isShowDeleteModal = false;
        this.deleteHeaderItemName = '';
        this.deleteItemMessage = '';
        this.recordIdForDelete = '';
        this.recordObjectForDelete = '';
    }
    handleDeleteRecord(event){
        if(this.recordIdForDelete != '' && this.recordIdForDelete != null && this.recordIdForDelete != undefined && 
        this.recordObjectForDelete != '' && this.recordObjectForDelete != null && this.recordObjectForDelete != undefined){
            this.showSpinner = true;
            doDeleteRecord({
                recordIdForDelete : this.recordIdForDelete, 
                recordObjectForDelete : this.recordObjectForDelete
            })
            .then(result => {
                if(this.recordObjectForDelete == 'Course_Module_Role__c'){
                    this.handleLoadAllCourseRoles();
                }
                this.handleCancelDeleteRecord();
                this.showSpinner = false;
            }).catch(error => {
                this.handleError(error);
                this.showSpinner = false;
            });
        }
    }



    handleToastMessage(toastType, message){
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error": variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
          });
        this.dispatchEvent(event);
    }

    handleError(error){
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }

}