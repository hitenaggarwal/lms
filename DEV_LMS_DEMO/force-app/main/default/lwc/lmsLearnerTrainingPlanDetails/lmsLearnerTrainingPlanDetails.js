import { LightningElement, api, track } from 'lwc';
import getTrainingPlanCourses from '@salesforce/apex/LMSTrainingPlanDetailController.getTrainingPlanCourses';
import getTrainingPlanDetails from '@salesforce/apex/LMSTrainingPlanDetailController.getTrainingPlanDetails';
import createTrainingPlanEnrollemnt from '@salesforce/apex/LMSTrainingPlanDetailController.createTrainingPlanEnrollemnt';
import createTrainingPlanEnrollemntFeedback from '@salesforce/apex/LMSTrainingPlanDetailController.createTrainingPlanEnrollemntFeedback';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import Id from "@salesforce/user/Id";



export default class LmsLearnerTrainingPlanDetails extends NavigationMixin(LightningElement) {

    @api
    recordId
    @track
    noCoursesFound
    @track
    courseRecordsFound

    @track dataObj
    @track trainingPlan
    isShowRequestModal = false;
    isShowSubmitFeedbackModal = false;
    isShowDateField = false;
    requestType = '';
    requestComment = '';
    requestDate = '';
    feedbackComment = '';
    userId = Id;
    loading = false;


    connectedCallback() {
        console.log('Record ID' + this.recordId);
        getTrainingPlanDetails({ recordId: this.recordId }).then((data) => {
            this.trainingPlan = data;
        })
        getTrainingPlanCourses({ recordId: this.recordId }).then((data) => {
            if (data && data.length > 0) {
                this.dataObj = data;
                this.courseRecordsFound = true;

                console.log('records found');
            } else {
                this.noCoursesFound = true;
                this.courseRecordsFound = false;
                console.log('no records found')


            }
        })

    }
    backToTrainingPlans() {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'training-plans' // pageName must be lower case
            }
        });
    }
    navigateToCourse(event) {

        let courseId = event.target.dataset.courseId;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: courseId,
                objectApiName: 'Course_Catalogue__c',
                actionName: 'view'
            }

        });



    }

    get options() {
        return [
            { label: 'Extension', value: 'Extension' },
            { label: 'Exemption', value: 'Exemption' },
            { label: 'Waivers', value: 'Waivers' },
            { label: 'Propose Changes', value: 'proposechanges' },
        ];
    }

    resetValues() {
        this.requestDate = '';
        this.requestComment = '';
        this.requestType = null;
        this.feedbackComment = '';
    }

    handleRequestTypeChange(event) {
        this.requestType = event.target.value;
        if (this.requestType == 'Extension') {
            this.isShowDateField = true;
        } else {
            this.isShowDateField = false;
        }
    }

    requestSubmit() {
        this.isShowRequestModal = true;
    }

    handleRequestCancel() {
        this.isShowRequestModal = false;
        this.resetValues();
    }

    handleRequestSend() {
        let valid = false;
        valid = [
            ...this.template.querySelectorAll("lightning-textarea"),
            ...this.template.querySelectorAll("lightning-combobox"),
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        if (valid) {
            this.loading = true;
            this.isShowRequestModal = false;
            console.log('Type:' + this.requestType);
            console.log('Comment:' + this.requestComment);
            console.log('Date:' + this.requestDate);
            let trainingEnrollment = { 'sobjectType': 'Training_Plan_Enrol__c' }
            trainingEnrollment.Training_Plan__c = this.recordId;
            trainingEnrollment.Type__c = this.requestType;
            trainingEnrollment.Comment__c = this.requestComment;
            trainingEnrollment.Proposed_Changes__c = '';
            if (this.requestType == 'proposechanges') {
                trainingEnrollment.Proposed_Changes__c =this.requestComment;
                trainingEnrollment.Type__c = null;

            }
            if (this.requestType == 'Extension') {
                trainingEnrollment.New_Date__c = this.requestDate;
            } else {
                trainingEnrollment.New_Date__c = '';
            }
            createTrainingPlanEnrollemnt({ newRecord: trainingEnrollment, userId: this.userId }).then(data => {
                console.log(data);
                this.resetValues();
                this.loading = false;
                this.handleToastMessage('Success', 'Request Saved!!')
            }).catch(err => {
                console.log(err);
                this.resetValues();
                this.loading = false;
                this.handleError(err);
            })
        }
    }


    submitFeedback() {
        this.isShowSubmitFeedbackModal = true;
    }

    handleFeedbackCancel() {
        this.isShowSubmitFeedbackModal = false;
        this.resetValues();
    }
    handleFeedbackSave() {
        let valid = false;
        valid = [
            ...this.template.querySelectorAll("lightning-textarea")
        ].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        if (valid) {
            this.loading = true;
            this.isShowSubmitFeedbackModal = false;
            let trainingEnrollment = { 'sobjectType': 'Training_Plan_Enrol__c' }
            trainingEnrollment.Training_Plan__c = this.recordId;
            trainingEnrollment.Proposed_Changes__c = this.feedbackComment;
            createTrainingPlanEnrollemntFeedback({ newRecord: trainingEnrollment, userId: this.userId }).then(data => {
                console.log(data);
                this.resetValues();
                this.loading = false;
                this.handleToastMessage('Success', 'Feedback Saved!!');

            }).catch(err => {
                console.log(err);
                this.loading = false;
                this.resetValues();
                this.handleError(err);
            })
        }
    }

    handleRequestFieldChange(event) {
        var field = event.target.name;
        if (field === "Comment") {
            this.requestComment = event.target.value;
        } else if (field === 'Date') {
            this.requestDate = event.target.value;
        }
    }
    handleFeedbackFieldChange(event) {
        var field = event.target.name;
        if (field === "Comment") {
            this.feedbackComment = event.target.value;
        }
    }

    handleToastMessage(toastType, message) {
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error" : variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
        });
        this.dispatchEvent(event);
    }

    handleError(error) {
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }

}