import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import fetchQuizQuestionRecords from '@salesforce/apex/LMSQuizQuestionsController.fetchQuizQuestionRecords';
import doDeleteRecord from '@salesforce/apex/LMSQuizQuestionsController.doDeleteRecord';

export default class LmsQuizQuestions extends LightningElement {

    @api courseModuleId;
    @track showSpinner = false;
    @track componentLoaded = false;
    @track dataObj = {};
    @track isNoQuestionRecords = true;

    @track isShowQuestionResponsesModal = false;
    @track questionIdForQuestionResponses;

    @track fields = {};
    @track isShowQuestionRecordModal = false;
    @track questionRecordId;

    @track isShowDeleteModal = false;
    @track deleteHeaderItemName = '';
    @track deleteItemMessage = '';
    @track recordIdForDelete = '';
    @track recordObjectForDelete = '';

    connectedCallback() {
        this.componentLoaded = false;
        this.handleLoadQuestions();
    }

    handleLoadQuestions(){
        this.showSpinner = true;
        fetchQuizQuestionRecords({ courseModuleId: this.courseModuleId })
        .then((result) => {
            if(result.quesWrapper != null && result.quesWrapper.length > 0){
                this.dataObj = result.quesWrapper;
                this.isNoQuestionRecords = false;
            }else{
                this.isNoQuestionRecords = true;
            }
            this.showSpinner = false;
            this.componentLoaded = true;
        })
        .catch((error) => {
            this.handleError(error);
            this.showSpinner = false;
            this.componentLoaded = true;
        });
    }

    handleQuestionRecordEdit(event){
        this.fields = {};
        this.questionRecordId = event.target.dataset.rid;
        this.isShowQuestionRecordModal = true;
    }

    handleAddQuestion(event){
        this.fields = {};
        this.questionRecordId = null;
        this.isShowQuestionRecordModal = true;
    }
    handleQuestionCancelModal(event){
        this.isShowQuestionRecordModal = false;
        this.questionRecordId = null;
    }

    //START - Question Record Modal Inputs
    handleChangeCategory(event){
        this.fields.Category__c = event.target.value;
    }
    handleChangeQuestionType(event){
        this.fields.Question_Type__c = event.target.value;
    }
    handleChangeSequence(event){
        this.fields.Sequence__c = event.target.value;
    }
    handleChangeScore(event){
        this.fields.Score__c = event.target.value;
    }
    handleChangeDescription(event){
        this.fields.Description__c = event.target.value;
    }
    handleChangeIsActive(event){
        this.fields.Is_Active__c = event.target.value;
    }
    //END - Question Record Modal Inputs


    @api handleQuestionSubmit(event) {
        
    }
    @api handleQuestionSuccess(event) {
        this.questionRecordId = null;
        this.isShowQuestionRecordModal = false;
        this.handleLoadQuestions();
        //console.log(JSON.stringify(event.detail));
    }
    @api handleQuestionError(event) {
        //console.log(JSON.stringify(event.detail));
        var errors = event.detail;
        this.handleToastMessage("Error", errors.detail);
    }


    handleQuestionRecordDelete(event){
        this.deleteHeaderItemName = 'Question';
        this.deleteItemMessage = 'Are you sure you want to delete this Question?';
        this.recordIdForDelete = event.target.dataset.rid;
        this.recordObjectForDelete = 'Question__c';
        this.isShowDeleteModal = true;
    }
    handleCancelDeleteRecord(event){
        this.isShowDeleteModal = false;
        this.deleteHeaderItemName = '';
        this.deleteItemMessage = '';
        this.recordIdForDelete = '';
        this.recordObjectForDelete = '';
    }
    handleDeleteRecord(event){
        if(this.recordIdForDelete != '' && this.recordIdForDelete != null && this.recordIdForDelete != undefined && 
        this.recordObjectForDelete != '' && this.recordObjectForDelete != null && this.recordObjectForDelete != undefined){
            this.showSpinner = true;
            doDeleteRecord({
                recordIdForDelete : this.recordIdForDelete, 
                recordObjectForDelete : this.recordObjectForDelete
            })
            .then(result => {
                this.handleLoadQuestions();
                this.handleCancelDeleteRecord();
                this.showSpinner = false;
            }).catch(error => {
                this.handleError(error);
                this.showSpinner = false;
            });
        }
    }

    handleQuestionSaveModal(event){
        this.fields.Course_Module__c = this.courseModuleId;
        this.template.querySelector('.QuestionFormCls').submit(this.fields);
    }

    //START - Question Responses Modal
    handleShowQuestionResponsesModal(event){
        this.questionIdForQuestionResponses = event.target.dataset.rid;
        this.isShowQuestionResponsesModal = true;
    }
    handleQuestionResponsesCancelModal(event){
        this.isShowQuestionResponsesModal = false;
        this.questionIdForQuestionResponses = null;
    }
    //END - Question Responses Modal

    handleToastMessage(toastType, message){
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error": variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
          });
        this.dispatchEvent(event);
    }

    handleError(error){
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }
}