import { LightningElement, api } from 'lwc';

export default class LmsCompletedCourseCard extends LightningElement {
    @api course;
}