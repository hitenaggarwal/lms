import { LightningElement,api ,track} from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getCourseDetails from '@salesforce/apex/lmsCourseFacilityController.getCourseFaciltyDetails';
import doDeleteRecord from '@salesforce/apex/lmsCourseFacilityController.doDeleteRecord';
import getRemainingFacility from '@salesforce/apex/lmsCourseFacilityController.getRemainingFacility';
import addCourseFacility from '@salesforce/apex/lmsCourseFacilityController.addCourseFacility';


export default class LmsCourseModuleFacility extends LightningElement {
    
    @track isNoCFRecords = true;
    @track isShowDeleteModal = false;
    @track deleteHeaderItemName = "";
    @track deleteItemMessage = "";
    @track recordIdForDelete = "";
    @track recordObjectForDelete = "";
    @track isModalOpen = false;

    @track isNoCFRecords= true;

  
  @track remainingFacility = [];
  @track facilityAddId = [];
  @track recordid=''


    @api courses =[]
    connectedCallback(){
        let sURL = window.location.href;
     this.recordid = sURL.split("courseId=")[1];
    
     this.showFacilityDetails();
    }

    showFacilityDetails(){
        getCourseDetails({recordId: this.recordid}).then((data)=>{
            if (data != null && data.length > 0) {
              
                this.isNoCFRecords = false;
            } else {
                this.isNoCFRecords = true;
            } 
            console.log('>>>>>data facilitytty'+JSON.stringify(data));
            this.courses = data;
            console.log('>>>>>courses'+this.courses);
    
        }).catch((error)=>{
            this.isNoCFRecords = true;
            console.log('>>>>>error'+JSON.stringify(error));
        });

    }

    openAddCourseModal(event) {
        this.isModalOpen = true;
        getRemainingFacility({ recordId : this.recordid })
          .then((result) => {
            console.log("Remaining Facility >> " + JSON.stringify(result));
            this.remainingFacility = result;
          })
          .catch((error) => {
            console.log("Error: " + JSON.stringify(error.message));
          });
      }

      closeAddCourseModal(event) {
        this.isModalOpen = false;
      }
      handleCourseSelect(event) {
        console.log("here" + event.target.checked);
        if (event.target.checked === true) {
          this.facilityAddId.push(event.target.dataset.id);
        } else {
          const index = this.facilityAddId.indexOf(event.target.dataset.id);
          if (index > -1) {
            this.facilityAddId.splice(index, 1);
          }
        }
        console.log("facilityAddId >> " + this.facilityAddId);
      }

      handleAddCourses(event) {
        addCourseFacility({
            recordId: this.recordid,
          facilityString: JSON.stringify(this.facilityAddId)
        })
          .then((result) => {
            this.dispatchEvent(
              new ShowToastEvent({
                title: "Success",
                message: "Facilty added to Course Successfully!",
                variant: "success"
              })
            );
            this.showFacilityDetails();
            this.facilityAddId =[];
            this.isModalOpen = false;
            
          })
          .catch((error) => {
            console.log("Error: " + JSON.stringify(error.message));
          });
      }

    deleteModule(event) {
        this.deleteHeaderItemName = "Course Module";
        this.deleteItemMessage =
          "Are you sure you want to delete <b>" +
          event.target.dataset.rmodulename +
          "</b> of Course ";
        this.recordIdForDelete = event.target.dataset.rid;
        this.recordObjectForDelete = "Course_Facility__c";
        this.isShowDeleteModal = true;
      }


      handleCancelDeleteRecord(event) {
        this.isShowDeleteModal = false;
        this.deleteHeaderItemName = "";
        this.deleteItemMessage = "";
        this.recordIdForDelete = "";
        this.recordObjectForDelete = "";
      }

      handleDeleteRecord(event) {
        if (
          this.recordIdForDelete != "" &&
          this.recordIdForDelete != null &&
          this.recordIdForDelete != undefined &&
          this.recordObjectForDelete != "" &&
          this.recordObjectForDelete != null &&
          this.recordObjectForDelete != undefined
        ) {
          
          doDeleteRecord({
            recordIdForDelete: this.recordIdForDelete,
            recordObjectForDelete: this.recordObjectForDelete
          })
            .then((result) => {
              if (this.recordObjectForDelete == "Course_Facility__c") {
                this.showFacilityDetails();
              }
              this.handleCancelDeleteRecord();
              
            })
            .catch((error) => {
              this.handleError(error);
              
            });
        }
      }


}