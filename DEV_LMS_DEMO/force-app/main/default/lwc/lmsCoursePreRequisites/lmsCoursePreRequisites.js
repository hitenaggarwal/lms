import { LightningElement, track, api, wire } from 'lwc';
import fetchCourseRelations from '@salesforce/apex/LMS_CoursePreRequisites.fetchCourseRelations';
import deleteCourseRelations from '@salesforce/apex/LMS_CoursePreRequisites.deleteCourseRelations';

import { ShowToastEvent } from "lightning/platformShowToastEvent"

export default class LmsCoursePreRequisites extends LightningElement {
    @api recordId;
    @track courseRequisitesList = [];
    @track norecords = false;
    @track isShowRecordModal = false;
    @track isShowDeleteModal = false;
    @track cousreRequisiteId = '';
    @track deleteRecordId = '';
    @track fields = {
        'Name': '',
        'Type__c': '',
        'Course_Catalogue__c': '',
        'Related_Course__c' : ''
    };

    connectedCallback() {
        this.getData();
    }

    getData() {
        fetchCourseRelations({
            courseId: this.recordId
        })
            .then(result => {
                console.log(result);
                this.courseRequisitesList = [];
                this.courseRequisitesList = result;
                console.log(JSON.stringify(this.courseRequisitesList));
            })
            .catch(error => {
                console.log("Error " + error);
            });
    }

    handleAdd(event) {
        this.cousreRequisiteId = '';
        this.fields = {
            'Name': '',
            'Type__c': '',
            'Course_Catalogue__c': '',
            'Related_Course__c' : ''
        };
        this.isShowRecordModal = true;
    }

    handleSuccess() {
        const evt = new ShowToastEvent({
            title: "Success",
            message: "",
            variant: "success"
        });
        this.dispatchEvent(evt);
        this.isShowRecordModal = false;
        this.getData();
    }
    handleCancel(event) {
        this.cousreRequisiteId = '';
        this.isShowRecordModal = false;
    }

    handleChangeName(event) {
        this.fields.Name = event.target.value;
    }
    handleType(event) {
        this.fields.Type__c = event.target.value;
    }
    handleCourseChange(event) {
        this.fields.Related_Course__c = event.target.value;
    }

    handleSave(event) {
        this.fields.Course_Catalogue__c = this.recordId;
        this.template.querySelector(".CourseModuleFormCls").submit(this.fields);
        this.isShowRecordModal = false;
    }

    editModule(event) {
        this.cousreRequisiteId = event.currentTarget.dataset.id;
        this.isShowRecordModal = true;
    }

    deleteModule(event) {
        this.isShowDeleteModal = true;
        this.deleteRecordId = event.currentTarget.dataset.id
    }

    handleCancelDeleteRecord(event) {
        this.isShowDeleteModal = false;
    }

    handleDeleteRecord(event) {
        deleteCourseRelations({
            id: this.deleteRecordId
        })
            .then(result => {
                const evt = new ShowToastEvent({
                    title: "Success",
                    message: "Record deleted Successfully.",
                    variant: "success"
                });
                this.dispatchEvent(evt);
                this.isShowDeleteModal = false;
                this.deleteRecordId = '';
                this.getData();
            })
            .catch(error => {
                console.log("Error " + error);
            });
    }
}