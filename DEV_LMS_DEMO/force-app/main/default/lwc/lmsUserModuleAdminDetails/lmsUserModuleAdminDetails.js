import { LightningElement,track } from 'lwc';
import getCourseDetails from '@salesforce/apex/lmsUserModuleAdminDetailsController.getCourseDetails';
export default class LmsUserModuleAdminDetails extends LightningElement {
    @track courses;
    
    



    connectedCallback(){
        var url_string = window.location.href;
        var url = new URL(url_string);
        var statusValue = url.searchParams.get("status");
        console.log('statussssss>>>>>'+statusValue);
        console.log('>>>>>in connected callback');
        getCourseDetails({status: statusValue}).then((data)=>{
           
            console.log('>>>>>data'+data);
            this.courses = data;
            console.log('>>>>>courses'+this.courses);

        }).catch((error)=>{
            console.log('>>>>>error'+JSON.stringify(error));
        });
    }
    
}