import { LightningElement, track } from "lwc";
import getTrainingPlans from "@salesforce/apex/LMSTrainingPlanController.getTrainingPlans";
import getLearners from "@salesforce/apex/LMSTrainingPlanController.getLearners";
import getTrainingPlanEnrollments from "@salesforce/apex/LMSTrainingPlanController.getTrainingPlanEnrollments";
import getEnrolledLearners from "@salesforce/apex/LMSTrainingPlanController.getEnrolledLearners";
import enrollLeaners from "@salesforce/apex/LMS_TrainingPlanEnrollment.enrollLeaners";
import updateEnrollers from "@salesforce/apex/LMS_TrainingPlanEnrollment.updateEnrollers";


import { NavigationMixin } from "lightning/navigation";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class LmsTrainingPlan extends NavigationMixin(LightningElement) {
  isTrainingPlanModalOpen = false;
  @track trainingPlanList = [];
  @track unregisterdLearnerList = [];
  @track trainingPlanEnrollList = [];
  @track isRegisterModal = false;
  @track isRegisteredModal = false;
  @track isProposedChangesModal = false;
  @track isLoading = false;
  @track fieldsForTrainingPlanEditForm = [
    {name : "Name"},
    {name : "Approver_Supervisor__c"},
    {name : "Skill_Level__c"},
    {name : "Status__c"},
    {name : "Duration_In_Days__c"},
    {name : "Is_Active__c"},
    {name : "Description__c"},
  ]
  @track learnerModifiedList = [];
  @track recordsUodated = false;
 
  selectedTrainingPlanId = "";
  selectedLearners = [];

  connectedCallback() {
    this.getInitialLoadData();
  }

  getInitialLoadData() {
    this.isLoading = true;
    getTrainingPlans()
      .then((result) => {
        this.trainingPlanList = result;
        this.isLoading = false;
      })
      .catch((error) => {
        this.isLoading = false;
        //this.error = error;
        console.log("error-->" + JSON.stringify(error));
      });
  }
  handleHourChange(event){
    this.learnerModifiedList.forEach(element => {
      if( event.target.dataset.id == element.Learner__c){
        element[event.target.name] = event.target.value;
      }
  });
  }

  handleContactChange(event) {
    console.log('LIST OF ID:' + JSON.stringify(event.detail));
    console.log('event.target.name ' + event.target.name);
    console.log('event.target.dataset.id ' + event.target.dataset.id);
    this.learnerModifiedList.forEach(element => {
        if( event.target.dataset.id == element.Learner__c){
          if(event.detail != null){
            element[event.target.name] = event.detail.Id;
          }else{
            element[event.target.name] = event.detail;
          }
        }
    });
  }

  handleContactChangeRegistered(event){
    console.log('LIST OF ID:' + JSON.stringify(event.detail));
    console.log('event.target.name ' + event.target.name);
    console.log('event.target.dataset.id ' + event.target.dataset.id);
    this.registeredLearnersList.forEach(element => {
        if( event.target.dataset.id == element.Id){
            if(event.detail != null){
              element[event.target.name] = event.detail.Id;
            }else{
              element[event.target.name] = event.detail;
            }
            this.recordsUodated = true;
        }
    });
  }

  isValid(){
    let isAllValid = false;
    isAllValid = [...this.template.querySelectorAll('lightning-combobox, lightning-dual-listbox, lightning-textarea, lightning-radio-group, lightning-input')]
    .reduce((validSoFar, inputCmp) => {
        inputCmp.reportValidity();
        return validSoFar && inputCmp.checkValidity();
    }, true);
    
    if(isAllValid === false){
          return isAllValid;
    }
    return isAllValid;
  }

  enrollLearners(event) {
    let _recordId = event.target.dataset.id;
    this.selectedTrainingPlanId = _recordId;
    this.isLoading = true;
    getLearners({ trainingPlanId: _recordId, isRegisterd: false })
      .then((result) => {
        if (result.length > 0) {
          this.isRegisterModal = true;
          this.unregisterdLearnerList = result;
          this.updateLearners(this.unregisterdLearnerList);
          this.isLoading = false;
        } else {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "",
              variant: "error",
              message: "All learners are already registered for this training"
            })
          );
          this.isLoading = false;
        }
      })
      .catch((error) => {
        this.isLoading = false;
        //this.error = error;
        console.log("error-->" + JSON.stringify(error));
      });
  }

  updateLearners(learnerList){
    learnerList.forEach(element => {
      let temp = {};
      temp.Learner__c = element.Id;
      temp.Name = element.name;
      temp.Certifying_Official__c = "";
      temp.Supervisor__c = "";
      temp.Training_Administrator__c = "";
      temp.Coach__c = "";
      temp.isSelected = false;
      this.learnerModifiedList.push(temp);
    });
  }

  showRegisteredLearners(event) {
    let _recordId = event.target.dataset.id;
    this.selectedTrainingPlanId = _recordId;
    this.isLoading = true;
    getEnrolledLearners({ trainingPlanId: _recordId })
      .then((result) => {
        if (result.length > 0) {
          this.isRegisteredModal = true;
          this.registeredLearnersList = result;
          this.isLoading = false;
        } else {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "",
              variant: "error",
              message: "No learner is registered yet for this training"
            })
          );
          this.isLoading = false;
        }
      })
      .catch((error) => {
        this.isLoading = false;
        //this.error = error;
        console.log("error-->" + JSON.stringify(error));
      });
    // getLearners({ trainingPlanId: _recordId, isRegisterd: true })
    //   .then((result) => {
    //     if (result.length > 0) {
    //       this.isRegisteredModal = true;
    //       this.registeredLearnersList = result;
    //       this.isLoading = false;
    //     } else {
    //       this.dispatchEvent(
    //         new ShowToastEvent({
    //           title: "",
    //           variant: "error",
    //           message: "No learner is registered yet for this training"
    //         })
    //       );
    //       this.isLoading = false;
    //     }
    //   })
    //   .catch((error) => {
    //     this.isLoading = false;
    //     //this.error = error;
    //     console.log("error-->" + JSON.stringify(error));
    //   });
  }

  showProposedChanges(event) {
    let _recordId = event.target.dataset.id;
    this.isLoading = true;
    getTrainingPlanEnrollments({ trainingPlanId: _recordId })
      .then((result) => {
        if (result.length > 0) {
          this.isProposedChangesModal = true;
          this.trainingPlanEnrollList = result;
          this.isLoading = false;
        } else {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "",
              variant: "error",
              message: "No changes are proposed by any learners"
            })
          );
          this.isLoading = false;
        }
      })
      .catch((error) => {
        //this.error = error;
        console.log("error-->" + JSON.stringify(error));
      });
  }

  handleInputChange(event) {
    let _recordId = event.target.dataset.id;
    this.learnerModifiedList.forEach(element => {
      if(_recordId == element.Learner__c){
        if (event.target.checked) {
          element.isSelected = true;
        }else{
          element.isSelected = false;
        }
      }
    });
  /*
    if (event.target.checked) {
      this.selectedLearners.push(_recordId);
    } else {
      let index = this.selectedLearners.indexOf(_recordId);
      if (index > -1) {
        this.selectedLearners.splice(index, 1);
      }
    }
    */
  }

  submitChanges(){
    if(this.recordsUodated){
      updateEnrollers({ enrollmentList: this.registeredLearnersList }).then((e) => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "success",
            message: "Learner successfully enrolled for training"
            })
          );
          this.closeModal();
      }).catch((e)=>{
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "error",
            message: "Something Wnet Wrong !"
          }));
        this.closeModal();
      });
    }else{
      this.closeModal();
    }
  }
  

  closeModal() {
    this.isRegisterModal = false;
    this.isProposedChangesModal = false;
    this.isRegisteredModal = false;
    this.learnerModifiedList = [];
  }

  submitRegisterationDetails() {
    console.log('selectedLearners' + JSON.stringify(this.selectedLearners));
    console.log('Training Plan Id' + JSON.stringify(this.selectedTrainingPlanId));
    console.log('this.learnerModifiedList' + JSON.stringify(this.learnerModifiedList));
    let enrollmentList = [];
    this.learnerModifiedList.forEach(element => {
        if(element.isSelected){
          enrollmentList.push(element);
        }
    }); 
    if(this.isValid()){
      if (enrollmentList.length > 0) {
        enrollLeaners({ trainingPlanId: this.selectedTrainingPlanId, enrollmentList: enrollmentList }).then((e) => {
          this.isRegisterModal = false;
          this.dispatchEvent(
            new ShowToastEvent({
              title: "",
              variant: "success",
              message: "Learner successfully enrolled for training"
            }));
        }).catch((e)=>{
          this.dispatchEvent(
            new ShowToastEvent({
              title: "",
              variant: "error",
              message: "Something Wnet Wrong !"
            })
          );
        });
      }else{
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "error",
            message: "Please select atleast one learner to register"
          })
        );
      }
    }else{
      this.dispatchEvent(
        new ShowToastEvent({
          title: "",
          variant: "error",
          message: "Please review all the error on the page"
        })
      );
    }
    
    /*
    if (this.selectedLearners.length > 0) {
      this.isRegisterModal = false;
      enrollLeaners({ trainingPlanId: this.selectedTrainingPlanId, contactIdList: this.selectedLearners }).then((e) => {
        this.isRegisterModal = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "success",
            message: "Learner successfully enrolled for training"
          }));
      }).catch((e)=>{
        this.dispatchEvent(
          new ShowToastEvent({
            title: "",
            variant: "error",
            message: "Something Wnet Wrong !"
          })
        );
      });
    } else {
      this.dispatchEvent(
        new ShowToastEvent({
          title: "",
          variant: "error",
          message: "Please select atleast one learner to register"
        })
      );
    }
    */
  }

  navigateToDetails(event) {
    let _recordId = event.target.dataset.id;
    // this[NavigationMixin.Navigate](
    //   {
    //     type: "standard__webPage",
    //     attributes: {
    //       url: "/training-plan-details?id=" + _recordId
    //     }
    //   },
    //   true
    // );

    this[NavigationMixin.Navigate]({
      type: "standard__recordPage",
      attributes: {
        recordId: _recordId,
        objectApiName: "Training_Plan__c",
        actionName: "view"
      }
    });
  }

  openTrainingPlanModal(event) {
    this.isTrainingPlanModalOpen = true;
  }
  closeTrainingPlanModal(event) {
    this.isTrainingPlanModalOpen = false;
  }
  handleTrainingPlanAdd(event) {
    this.getInitialLoadData();
    this.dispatchEvent(
      new ShowToastEvent({
        title: "Success",
        variant: "success",
        message: "Training Plan Added!"
      })
    );
    this.isTrainingPlanModalOpen = false;
    this.isLoading = false;
    //window.location.reload();
  }
}