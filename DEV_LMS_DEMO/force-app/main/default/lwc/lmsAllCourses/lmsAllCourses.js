import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import loadAllCourses from '@salesforce/apex/LMS_AllCoursesCtrl.loadAllCourses';

export default class LmsAllCourses extends LightningElement {
    
    @track showSpinner = false;
    @track componentLoaded = false;
    @track dataObj = {};
    @track searchedValue = '';
    @track courseRecordsFound = false;
    @track noCourseRecordFound = false;
    @track noCourseRecordFoundAfterSearch = false;

    connectedCallback() {
        this.componentLoaded = false;
        this.handleLoadAllCourses(false);
    }

    handleLoadAllCourses(isFromSearch){
        this.showSpinner = true;
        this.noCourseRecordFound = false;
        this.noCourseRecordFoundAfterSearch = false;
        loadAllCourses({ isFromSearch: isFromSearch, searchedValue: this.searchedValue }).then(result => {
            if(result != null && result != '' && result != undefined){
                this.dataObj = result;
                this.courseRecordsFound = true;
            }else{
                this.courseRecordsFound = false;
                if(isFromSearch){
                    this.noCourseRecordFoundAfterSearch = true;
                }else{
                    this.noCourseRecordFound = true;
                }
            }
            this.showSpinner = false;
            this.componentLoaded = true;
        }).catch(error => {
            this.handleError(error);
            this.showSpinner = false;
            this.componentLoaded = true;
        });
    }

    handleInput(event){
        if (event.target.name === "searchedValue") {
            this.searchedValue = event.target.value.trim();
            if(this.searchedValue == '' || this.searchedValue == null || this.searchedValue == undefined){
                this.handleSearch();
            }
        }
    }

    handleSearch(event){
        this.handleLoadAllCourses(true);
    }

    handleToastMessage(toastType, message){
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error": variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
          });
        this.dispatchEvent(event);
    }

    handleError(error){
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }
}