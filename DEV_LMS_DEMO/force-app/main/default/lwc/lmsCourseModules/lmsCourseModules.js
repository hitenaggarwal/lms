import { LightningElement, api, track } from 'lwc';
import resetQuiz from '@salesforce/apex/LMS_QuizRevisit_Controller.resetQuiz';
import handleNumberOfLaunches from '@salesforce/apex/LMS_QuizRevisit_Controller.handleNumberOfLaunches';
import completeCourse from '@salesforce/apex/LMS_QuizRevisit_Controller.completeCourse';

import doSubmitForApproval from '@salesforce/apex/LMS_CourseDetailsController.doSubmitForApproval';
import { showAjaxErrorMessage } from 'c/lmsUtils';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class LmsCourseModules extends LightningElement {
    @api courseId;
    @api modules;
    @api isShowSubmitForApprovalButton;
    @track
    isModalQuizOpen = false;
    selectedUserModuleId;
    userModuleId;
    @track
    isModalVideoOpen = false;
    @track isModalScormOpen = false;
    @track statusOfModule= '';

    handleLaunch(event) {
        
        let moduleType = event.target.dataset.moduleType;
        this.selectedUserModuleId = event.target.dataset.umaId;
        this.userModuleId = event.target.dataset.umaId;

        handleNumberOfLaunches({
            courseModuleId : event.target.dataset.moduleId
        })
        .then(status => {
            this.statusOfModule = status;
            console.log('Status : ' + status);
            
            if(this.statusOfModule == 'Number of Launches Updated'){
                this.statusOfModule = '';
                if(moduleType == 'Quiz'){
                    
                    console.log( this.courseModuleId);
                    this.isModalQuizOpen = true;
                }else if(moduleType == 'Video'){
                    
                    this.isModalVideoOpen = true;
                }
                else if(moduleType == 'SCORM'){
                    
                    this.isModalScormOpen = true;   
                    
                }
                
            }else{
                console.log('>>>>>>>>not completed>>>>>>>>>>'+this.statusOfModule);
                this.statusOfModule = '';
                var message = 'Please complete previous modules first';
                this.handleToastMessage("Note", message);
            }
        })
        .catch(error => {
            console.log('Error : ' + JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        })
        
        this.courseModuleId = event.target.dataset.moduleId;
        this.fullUrl = event.target.dataset.moduleVideoUrl;
        this.scormURL = event.target.dataset.scormUrl;
        
    }
    connectedCallback(){
        console.log(JSON.stringify(this.modules,null,2));
        console.log(   this.modules.length);
     
    }

    handleRevisit(event) {
        let moduleType = event.target.dataset.moduleType;

        handleNumberOfLaunches({
            courseModuleId : event.target.dataset.moduleId
        })
        .then(status => {
            console.log('Status : ' + status);
        })
        .catch(error => {
            console.log('Error : ' + JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        })

        if(moduleType == 'Quiz'){
            this.courseModuleId = event.target.dataset.moduleId;
            console.log('Course Module Id : ' + this.courseModuleId);
            resetQuiz({
                courseModuleId : this.courseModuleId
            })
            .then(status => {
                console.log('Status : ' + status);
                this.isModalQuizOpen = true;
            })
            .catch(error => {
                console.log('Error : ' + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            })

        }
    }

    get modulesExist(){
        return this.modules && this.modules.length > 0;
    }

    get noModulesFound() {
        return !this.modules || this.modules.length === 0;
    }
    closeModal(){
        this.isModalQuizOpen = false;
        this.isModalVideoOpen = false;
        this.isModalScormOpen = false;
    }

    handleSubmitForApproval(event){
        this.showSpinner = true;
        doSubmitForApproval({ courseId: this.courseId })
        .then((result) => {
            
        })
        .catch((error) => {
            this.handleError(error);
            this.showSpinner = false;
        });
    }

    handleToastMessage(toastType, message){
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error": variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
          });
        this.dispatchEvent(event);
    }

    handleError(error){
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }
    completeModule(){
        completeCourse({cumaId :this.selectedUserModuleId });
        this.isModalVideoOpen = false;
        window.location.reload;
       
    }
}