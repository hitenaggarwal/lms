import { LightningElement, track, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getTrainingPlan from "@salesforce/apex/LMS_TrainingPlanDetailsController.getTrainingPlan";
import getTrainingPlanCourses from "@salesforce/apex/LMS_TrainingPlanDetailsController.getTrainingPlanCourses";
import getRemainingCourses from "@salesforce/apex/LMS_TrainingPlanDetailsController.getRemainingCourses";
import updateActiveStatus from "@salesforce/apex/LMS_TrainingPlanDetailsController.updateActiveStatus";
import addCoursesToTrainingPlan from "@salesforce/apex/LMS_TrainingPlanDetailsController.addCoursesToTrainingPlan";
import deleteRecord from "@salesforce/apex/lmsUtility.deleteRecord";

export default class LmsTrainingPlanDetails extends LightningElement {
  //recordId = "a0K4x000001ZcAoEAK";
  @api recordId;
  isEditable = false;
  isModalOpen = false;
  isEditOrSave = "Edit";
  activeStatus;
  @track trainingPlan = {};
  @track trainingPlanCourses = [];
  @track remainingCourses = [];
  @track courseToAddIds = [];

  @track isShowDeleteModal = false;
    @track deleteHeaderItemName = "";
    @track deleteItemMessage = "";
    @track recordIdForDelete = "";
    

  connectedCallback() {
    // let sURL = window.location.href;
    // let paramValue = sURL.split("id=")[1];
    // console.log("paramValue" + paramValue);
    // const param = "id";
    // const paramValue = this.getUrlParamValue(window.location.href, param);
    // console.log("paramValue" + paramValue);
    //this.recordId = paramValue;
    this.fetchTrainingPlan();
    this.fetchTrainingPlanCourses();
  }
  handleActiveStatus(event) {
    this.activeStatus = event.target.checked;
  }
  editTrainingPlan(event) {
    this.isEditable = !this.isEditable;
    this.isEditOrSave = this.isEditOrSave === "Edit" ? "Save" : "Edit";

    if (this.isEditable === false) {
      updateActiveStatus({
        trainingPlanId: this.recordId,
        activeStatus: this.activeStatus
      })
        .then((result) => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Success",
              message: "Updated",
              variant: "success"
            })
          );
          window.location.reload();
        })
        .catch((error) => {
          console.log("Error: " + JSON.stringify(error.message));
        });
    }
  }

  fetchTrainingPlan() {
    getTrainingPlan({ trainingId: this.recordId })
      .then((result) => {
        this.trainingPlan = result[0];
        console.log("Result" + JSON.stringify(result));
      })
      .catch((error) => {
        console.log("Error: " + JSON.stringify(error.message));
      });
  }

  fetchTrainingPlanCourses() {
    getTrainingPlanCourses({ trainingId: this.recordId })
      .then((result) => {
        for (let data of result) {
          console.log(
            "trainingPlanCourses >> " + JSON.stringify(data.Course_Catalogue__r)
          );
          this.trainingPlanCourses.push(data.Course_Catalogue__r);
        }
      })
      .catch((error) => {
        console.log("Error: " + JSON.stringify(error.message));
      });
  }

  openAddCourseModal(event) {
    this.isModalOpen = true;
    getRemainingCourses({ trainingPlanId: this.recordId })
      .then((result) => {
        console.log("Remaining Courses >> " + JSON.stringify(result));
        this.remainingCourses = result;
      })
      .catch((error) => {
        console.log("Error: " + JSON.stringify(error.message));
      });
  }

  closeAddCourseModal(event) {
    this.isModalOpen = false;
  }

  handleCourseSelect(event) {
    console.log("here" + event.target.checked);
    if (event.target.checked === true) {
      this.courseToAddIds.push(event.target.dataset.id);
    } else {
      const index = this.courseToAddIds.indexOf(event.target.dataset.id);
      if (index > -1) {
        this.courseToAddIds.splice(index, 1);
      }
    }
    console.log("courseToAddIds >> " + this.courseToAddIds);
  }
  handleAddCourses(event) {
    addCoursesToTrainingPlan({
      trainingPlanId: this.recordId,
      coursesString: JSON.stringify(this.courseToAddIds)
    })
      .then((result) => {
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Success",
            message: "Course added to Training Plan Successfully!",
            variant: "success"
          })
        );
        this.isModalOpen = false;
        window.location.reload();
      })
      .catch((error) => {
        console.log("Error: " + JSON.stringify(error.message));
      });
  }


  deleteModule(event) {
    this.deleteHeaderItemName = "Course Module";
    this.deleteItemMessage =
      "Are you sure you want to delete <b>" +
      event.target.dataset.rmodulename +
      "</b> of Course ";
    this.recordIdForDelete = event.target.dataset.rid;
    
    this.isShowDeleteModal = true;
  }


  handleCancelDeleteRecord(event) {
    this.isShowDeleteModal = false;
    this.deleteHeaderItemName = "";
    this.deleteItemMessage = "";
    this.recordIdForDelete = "";
    
  }

  handleDeleteRecord(event) {
    if (
      this.recordIdForDelete != "" &&
      this.recordIdForDelete != null &&
      this.recordIdForDelete != undefined 
      
    ) {
      
      deleteRecord({
        recordId: this.recordIdForDelete,
        
      })
        .then((result) => {
          
          
          window.location.reload();
          this.handleCancelDeleteRecord();
          
        })
        .catch((error) => {
          this.handleError(JSON.stringify(error));
          
        });
    }
  }
}