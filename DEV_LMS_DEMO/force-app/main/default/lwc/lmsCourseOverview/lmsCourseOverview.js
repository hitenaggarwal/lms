import { LightningElement, api } from 'lwc';

export default class LmsCourseOverview extends LightningElement {
    @api course;
}