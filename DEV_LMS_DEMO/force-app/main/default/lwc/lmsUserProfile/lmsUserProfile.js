import { LightningElement, track, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getContactId from "@salesforce/apex/LMS_UserCertificationController.getContactId";
import getContact from "@salesforce/apex/LMS_ProfileController.getContact";
import getProfileInfo from "@salesforce/apex/LMS_UserCertificationController.getProfileInfo";
import getCertificationDetail from "@salesforce/apex/LMS_UserCertificationController.getCertificationDetail";
import getCertificationsList from "@salesforce/apex/LMS_UserCertificationController.getCertificationsList";
import upsertCertification from "@salesforce/apex/LMS_UserCertificationController.upsertCertification";
import updateAboutMe from "@salesforce/apex/LMS_UserCertificationController.updateAboutMe";
import deleteRecord from "@salesforce/apex/lmsUtility.deleteRecord";
import getUserEnrollmentList from '@salesforce/apex/LMS_AllProfileController.getUserEnrollmentList'

export default class LmsUserProfile extends LightningElement {
  contactId;
  imgUrl;
  uploadedImageName;
  aboutMe = "";
  editOrSaveLabel = "Edit";
  isModalOpen = false;
  isEditable = false;
  
  isCertificateEditable = false;
  isImageUploaded = false;

  @track obj = {};
  @api recordId;
  @track showSpinner = false;
  @track showTotalHours = false;
  @track certificationObj = {};
  @track certificationsList = [];
  @track userEnrollmentList = [];

  @track isShowDeleteModal = false;
  @track deleteHeaderItemName = "";
  @track deleteItemMessage = "";
  @track recordIdForDelete = "";

  connectedCallback() {
    this.fetchContactId();
    this.getContact();
    this.fetchCertifications();
  }
  handleInputChange(event) {
    this.obj[event.target.name] = event.detail.value;
    this.certificationObj[event.target.name] = event.detail.value;
    //console.log(JSON.stringify(this.obj));
    //console.log(JSON.stringify(this.certificationObj));
  }
  handleSave() {
    const event = new ShowToastEvent({
      title: "Success!",
      message: "Changes have been saved!!",
      variant: "Success"
    });
    this.dispatchEvent(event);
  }
  /* -- FETCH CONTACT ID FOR IMAGE UPLOAD TO CONTACT
  -- */
  fetchContactId() {
    getContactId({})
      .then((result) => {
        this.contactId = result;
        console.log("ContactId >>> " + this.contactId);
        this.fetchProfileInfo();
        this.fetchUserEnrollment();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }

  getContact() {
    getContact({})
      .then((result) => {
        this.obj = JSON.parse(JSON.stringify(result));
        if(this.obj.trainingDurationInHours !=null && this.obj.trainingDurationInHours !=undefined && this.obj.trainingDurationInHours !== "0mins"){
          this.showTotalHours = true;
      }
      else{
          this.showTotalHours = false;
      }
        console.log(this.obj);
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }
  /* -- ACCEPTED IMAGE FORMATS
  -- */
  get acceptedFormats() {
    return [".jpg", ".jpeg", ".png"];
  }
  /* -- HANDLE PROFILE IMAGE AND ABOUT ME CONTENT DISPLAY/EDIT
  -- */
  fetchProfileInfo() {
    getProfileInfo({ contactId: this.contactId })
      .then((result) => {
        this.aboutMe = result.aboutMe;
        this.imgUrl =
          "/sfc/servlet.shepherd/version/download/" + result.contentVersionId;
        console.log("About Me" + JSON.stringify(result));
        this.uploadedImageName = result.fileName;
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }

  handleProfileEdit() {
    this.isEditable = !this.isEditable;
    this.editOrSaveLabel = this.isEditable === true ? "Save" : "Edit";

    if (this.isEditable === false) {
      updateAboutMe({ contactId: this.contactId, aboutMe: this.aboutMe })
        .then((result) => {
          this.dispatchEvent(
            new ShowToastEvent({
              title: "Success",
              message: "Updated Successfully!!",
              variant: "success"
            })
          );
          window.location.reload();
        })
        .catch((error) => {
          console.log("Error: " + JSON.stringify(error.message));
        });
    }
  }

  handleCancelEdit(event) {
    this.isEditable = false;
    this.editOrSaveLabel = "Edit";
  }

  handleAboutMeChange(event) {
    console.log("About Me >>> " + event.target.value);
    this.aboutMe = event.target.value;
  }

  handleUploadFinished(event) {
    console.log("Image Uploaded");
    var file = event.detail.files;
    this.uploadedImageName = file[0].name;
    this.isImageUploaded = true;
  }
  /* -- HANDLE CERTIFICATION CONTENT DISPLAY/EDIT
  -- */
  fetchCertifications() {
    getCertificationsList()
      .then((result) => {
        this.certificationsList = result;
        console.log("CERTIFICATIONS >>> " + JSON.stringify(result));
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }
  openAddCertificationModal(event) {
    this.isModalOpen = true;
  }
  handleAddCertification(event) {
    upsertCertification({
      certificationString: JSON.stringify(this.certificationObj)
    })
      .then((result) => {
        this.isModalOpen = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Success",
            message: "New certification added!!",
            variant: "success"
          })
        );
        window.location.reload();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }
  closeAddCertificationModal(event) {
    this.isModalOpen = false;
    this.isCertificateEditable = false;
  }
  openCertificationEditModal(event) {
    const certificationId = event.target.dataset.id;
    getCertificationDetail({ certificateId: certificationId })
      .then((result) => {
        this.isModalOpen = true;
        this.isCertificateEditable = true;
        this.certificationObj = result;
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }
  handleEditCertification(event) {
    upsertCertification({
      certificationString: JSON.stringify(this.certificationObj),
      isUpdate: true
    })
      .then((result) => {
        this.isModalOpen = false;
        this.isCertificateEditable = false;
        this.dispatchEvent(
          new ShowToastEvent({
            title: "Success",
            message: "Certification updated!!",
            variant: "success"
          })
        );
        window.location.reload();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
      });
  }
  deleteModule(event) {
    this.deleteHeaderItemName = "Course Module";
    this.deleteItemMessage =
      "Are you sure you want to delete <b>" +
      event.target.dataset.rmodulename +
      "</b> of Course ";
    this.recordIdForDelete = event.target.dataset.rid;
    
    this.isShowDeleteModal = true;
  }


  handleCancelDeleteRecord(event) {
    this.isShowDeleteModal = false;
    this.deleteHeaderItemName = "";
    this.deleteItemMessage = "";
    this.recordIdForDelete = "";
    
  }

  handleDeleteRecord(event) {
    if (
      this.recordIdForDelete != "" &&
      this.recordIdForDelete != null &&
      this.recordIdForDelete != undefined 
      
    ) {
      
      deleteRecord({
        recordId: this.recordIdForDelete,
        
      })
        .then((result) => {
          
          
          this.fetchCertifications();
          this.handleCancelDeleteRecord();
          
        })
        .catch((error) => {
          this.handleError(JSON.stringify(error));
          
        });
    }
  }

  fetchUserEnrollment(){
    getUserEnrollmentList({contactId: this.contactId }
        ).then((result) => {
            this.userEnrollmentList = result;
          })
          .catch((error) => {
            console.log(error);
          });
  }
}