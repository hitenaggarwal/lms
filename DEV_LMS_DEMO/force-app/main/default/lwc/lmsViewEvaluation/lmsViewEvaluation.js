import { LightningElement, api } from 'lwc';

export default class LmsViewEvaluation extends LightningElement {
    
    @api userEnrollmentId;

    connectedCallback() {
        console.log('User Enrollment Id : ' + this.userEnrollmentId);
    }
}