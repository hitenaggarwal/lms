import { LightningElement, api, track } from 'lwc';
import getAllUserEnrollment from '@salesforce/apex/LMS_ManageTaskComponentController.getAllUserEnrollment';
import getUserModuleAssociation from '@salesforce/apex/LMS_ManageTaskComponentController.getUserModuleAssociation';
import deleteUserModuleRecord from '@salesforce/apex/LMS_ManageTaskComponentController.deleteUserModuleRecord';
import updateUserModuleRecord from '@salesforce/apex/LMS_ManageTaskComponentController.updateUserModuleRecord';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class LmsManageTaskComponent extends LightningElement {

    @api courseScheduleId = 'a014x00000956kaAAA';
    loading = false;
    enrollementList = []
    userModuleAssociationList = []
    showManageTaskButton = true;
    showManageTaskModal = false;
    isShowDeleteTaskModal = false;
    userModuleToDeleteId = '';
    manageTaskUserEnrollmentId = '';
    @track fieldChangeTrackMap = {};

    connectedCallback() {
        getAllUserEnrollment({ recordId: this.courseScheduleId }).then(data => {
            this.enrollementList = data;
            if (this.enrollementList[0].Course_Schedule__r.Session_Type__c == 'On Job Training') {
                this.showManageTaskButton = false;
            } else {
                this.showManageTaskButton = true;
            }
        }).catch(err => {
            console.log(err);
        })
    }

    handleToastMessage(toastType, message) {
        var variant;
        toastType == "Success" ? variant = "success" : (toastType == "Error" ? variant = "error" : variant = "");
        const event = new ShowToastEvent({
            title: toastType,
            message: message,
            variant: variant,
            mode: "dismissable"
        });
        this.dispatchEvent(event);
    }

    handleError(error) {
        var errorMessage;
        if (Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if (typeof error.body.message === 'string') {
            errorMessage = error.body.message;
        }
        this.handleToastMessage("Error", errorMessage);
    }

    handleManageTaskClick(event) {
        this.loading = true;
        var Id = event.currentTarget.dataset.id;
        this.manageTaskUserEnrollmentId = Id;
        getUserModuleAssociation({ recordId: Id }).then(data => {
            this.userModuleAssociationList = data;
            this.fieldChangeTrackMap = {};
            for (var i = 0; i < data.length; i++) {
                this.fieldChangeTrackMap[data[i].Id] = data[i];
            }
            this.loading = false;
        }).catch(err => {
            this.loading = false;
            console.log(err);
        })
        this.showManageTaskModal = true;
    }

    onManageTaskModelCancel() {
        this.showManageTaskModal = false;
    }

    onManageTaskModelSave() {
        this.loading = true;
        this.showManageTaskModal = false;
        updateUserModuleRecord({ updatedMap: this.fieldChangeTrackMap }).then(data => {
            this.handleToastMessage('Success', 'Task Updated Successfully');
            this.loading = false;
        }).catch(err => {
            this.handleError(err);
            this.loading = false;
        })
    }

    deleteTaskHandle(event) {
        this.userModuleToDeleteId = event.currentTarget.dataset.rid;
        this.isShowDeleteTaskModal = true;
    }

    handleSaveDeleteTaskModal() {
        this.loading = true;
        deleteUserModuleRecord({ recordId: this.userModuleToDeleteId }).then(data => {
            this.userModuleToDeleteId = '';
            this.isShowDeleteTaskModal = false;

            getUserModuleAssociation({ recordId: this.manageTaskUserEnrollmentId }).then(data => {
                this.userModuleAssociationList = data;
                this.fieldChangeTrackMap = {};
                for (var i = 0; i < data.length; i++) {
                    this.fieldChangeTrackMap[data[i].Id] = data[i];
                }
                this.loading = false;
                this.handleToastMessage('Success', 'Task Deleted Successfully!!');
            }).catch(err => {
                console.log(err);
                this.loading = false;
            })
        }).catch(err => {
            this.loading = false;
            this.handleError(err);
            this.isShowDeleteTaskModal = false;
        })
    }
    handleCancelDeleteTaskModal() {
        this.isShowDeleteTaskModal = false;
    }

    handleFieldChange(event) {
        var Id = event.currentTarget.dataset.id;
        if (event.target.name == 'dueDate') {
            this.fieldChangeTrackMap[Id].Due_Date__c = event.target.value;
        }
        if (event.target.name == 'Sequence') {
            this.fieldChangeTrackMap[Id].Sequence__c = event.target.value;
        }

    }


}