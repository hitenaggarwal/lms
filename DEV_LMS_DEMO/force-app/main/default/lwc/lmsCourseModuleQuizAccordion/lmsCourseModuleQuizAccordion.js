import { LightningElement, api, track } from "lwc";
import courseModuleQuiz from "@salesforce/apex/lmsCourseModuleQuizAccordionController.courseModuleQuiz";

export default class LmsCourseModuleQuizAccordion extends LightningElement {
  @api recordId = "";
  //@api recordid = "a004x0000035H2lAAE";
  moduleList = [];
  wrapperData = [];
  hasQuizTaken = false;
  @track mapData = [];

  connectedCallback() {
    console.log("Record Id >> " + this.recordId);
    courseModuleQuiz({ recordId: this.recordId })
      .then((data) => {
        console.log("Data >>> " + JSON.stringify(data));
        if (data && data.length > 0) {
          this.hasQuizTaken = true;
          this.wrapperData = data;
        } else {
          this.hasQuizTaken = false;
        }
      })
      .catch((err) => {
        console.log("ERROR" + JSON.stringify(err));
      });
  }
}