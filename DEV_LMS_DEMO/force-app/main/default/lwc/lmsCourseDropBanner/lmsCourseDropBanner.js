import { LightningElement, api } from 'lwc';
import deleteUserModuleAndEnrollment from '@salesforce/apex/lmsCourseDropBannerController.deleteUserModuleAndEnrollment';

import Id from "@salesforce/user/Id";

export default class LmsCourseDropBanner extends LightningElement {

    @api
    courseId;
    userId = Id;


    handleDrop() {
        console.log('I am dropping course');
        console.log('CourseId:' + this.courseId);
        deleteUserModuleAndEnrollment({ userId: this.userId, courseId: this.courseId }).then(data => {
            console.log('Success');
            window.location.reload();
        }).catch(err => {
            console.log(err);
        })
    }

}