import { api, LightningElement, track } from 'lwc';

import doInit from '@salesforce/apex/LMS_CourseEmailNotificationController.doInit';
import sendEmail from '@salesforce/apex/LMS_CourseEmailNotificationController.sendEmail';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { showAjaxErrorMessage } from 'c/lmsUtils';

export default class LmsCourseEmailNotification extends LightningElement {
    @track showEmailModal = false;
    @track showSpinner = false;

    @track emailToAddresses = '';
    @track emailSubject = '';
    @track emailBody = '';

    @track showAttendeeStatusOptions = false;
    @track attendeeStatusOption = "All";
    attendeeStatusOptions = [
        { label : "All", value : "All" },
        { label : "Confirmed", value : "Confirmed" },
        { label : "Waitlist", value : "Waitlist"}
    ];

    allowedFormats =  ['font', 'size', 'bold', 'italic', 'underline', 'strike',
    'list', 'indent', 'align', 'link', 'clean', 'table', 'header', 'color',
    'background'];

    @track emailBodyValidity = true;

    @api recordId = 'a014x000007grchAAA';

    connectedCallback(){
        //this.getToAddresses();
    }

    handleChange( event ){
        if( event.target.name == 'emailToAddresses'){
            this.emailToAddresses = event.target.value;
        }else if( event.target.name == 'emailSubject'){
            this.emailSubject = event.target.value;
        }else if( event.target.name == 'emailBody'){
            this.emailBody = event.target.value;
        }else if( event.target.name == 'attendeeStatusOption' ){
            this.attendeeStatusOption = event.target.value;
            this.getToAddresses();
        }
    }

    getToAddresses(){
        this.showSpinner = true;
        doInit({
            recordId : this.recordId,
            attendeeStatusOption : this.attendeeStatusOption
        }).then(response=>{
            this.showAttendeeStatusOptions = response.showAttendeeStatusOptions;
            if( response.emails && response.emails.length > 0 ){
                this.emailToAddresses = response.emails.join(',');
            }
            this.showEmailModal = true;
        }).catch(error=>{
            console.log('error occurred: '+JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        }).finally(()=>{
            this.showSpinner = false;
        });
    }

    handleSendNotificationClick(event){
        if( this.recordId ){
            this.getToAddresses();
        }else{
            this.showEmailModal = true;
        }
    }

    handleSendEmailClick(event){
        let allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        allValid += [...this.template.querySelectorAll('lightning-textarea')]
        .reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);

        this.emailBodyValidity = true;
        if( !this.emailBody || this.emailBody.trim().length == 0 ){
            console.log( 'emailbody is blank' );
            this.emailBodyValidity = false;
            allValid = false;
        }

        if( allValid ){
            let hasInvalidEmail = false;
            let emails = this.emailToAddresses.split(',');
            for( var i = 0; i < emails.length; i++ ){
                if( !this.validateEmail( emails[i] ) ){
                    hasInvalidEmail = true;
                    break;
                }
            }

            if( hasInvalidEmail ){
                this.showToast( 'Error', 'Please enter valid email address.', 'error' );
                return false;
            }

            this.showSpinner = true;
            sendEmail({
                recordId : this.recordId,
                emailAddresses : this.emailToAddresses,
                emailSubject : this.emailSubject,
                emailBody : this.emailBody
            }).then(response=>{
                this.showToast( 'Success', 'Email sent successfully.', 'success' );
            }).catch(error=>{
                console.log('error occurred: '+JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            }).finally(()=>{
                this.showSpinner = false;
            });

        }else{
            this.showToast( 'Error', 'Please fill all the require fields.', 'error' );
        }
            
    }

    validateEmail(email){
        return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }

    handleModalCancelClick( event ){
        this.showEmailModal = false;
        this.emailSubject = '';
        this.emailBody = '';
    }

    showToast(title, message, varient) {
        const event = new ShowToastEvent({
            title: title,
            message: message,
            variant: varient
        });
        this.dispatchEvent(event);
    }
}