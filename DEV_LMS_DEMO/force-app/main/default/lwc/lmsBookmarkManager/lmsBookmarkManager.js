import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import fetchBookmarks from '@salesforce/apex/LMS_BookmarkManagerController.fetchBookmarks';
import toggleCourseBookmark from '@salesforce/apex/LMS_BookmarkManagerController.toggleCourseBookmark';
import { showAjaxErrorMessage } from 'c/lmsUtils';

export default class LmsBookmarkManager extends NavigationMixin(LightningElement) {

    objectName = '/course-catalogue/';
    @track bookmarked = false;
    @api recordId;
    @track bookmarks = [{
            Course_Catalogue__r : {
                Name : 'You don\'t have any favorites yet',
            },
            Id : null
        }];
    @track showBookmarkManager = false;
    @track disableAddBookmark = true;
    @track currentCourseBookmarked = false;
    @track pathPrefix = '';
    connectedCallback() {
        console.log('RecordId : '+this.recordId);
        if( this.recordId == '' ){
            this.recordId = this.getRecordIdFromURL();
            var courseURL = document.location.href; 
            if( courseURL.indexOf(this.objectName) != -1 ){
                this.recordId = courseURL.split(this.objectName)[1].split('/')[0];
                console.log('URL recordId : '+this.recordId);
            }      
            /*var url = new URL(url_string);         
            var courseId = url.searchParams.get("courseId");         
            console.log(courseId)*/
        }
        if( this.recordId && this.recordId != '' ){
            this.runIntervalOnRecord = false;
        }
        this.loadBookmarks();
        this.activateSetInterval();
    }

    @track runIntervalOnRecord = true;
    activateSetInterval(){
        var intervalID = setInterval(function (){
            console.log('inside setInterval...');
            var courseURL = document.location.href; 

            if(courseURL.indexOf(this.objectName) != -1){
                if( this.recordId != courseURL.split(this.objectName)[1].split('/')[0] ){
                    this.recordId = courseURL.split(this.objectName)[1].split('/')[0];
                    this.runIntervalOnRecord = true;
                }
            }

            if( courseURL.indexOf(this.objectName) == -1 ){
                this.disableAddBookmark = true;
                this.bookmarked = false;
                this.runIntervalOnRecord = true;
            }else if( this.runIntervalOnRecord ){
                this.runIntervalOnRecord = false;
                this.loadBookmarks();
            }
        }.bind(this),2000);
    }

    getRecordIdFromURL(){

    }

    loadBookmarks(){
        fetchBookmarks({ recordId : this.recordId })
        .then(response => {
            this.disableAddBookmark = response.disableAddBookmark;
            this.bookmarked = response.currentCourseBookmarked;
            this.pathPrefix = response.pathPrefix;
            if( response.bookmarks.length > 0 ){
                this.bookmarks = response.bookmarks;
            }
            this.showBookmarkManager = true;
        })
        .catch(error => {
            console.log('error occurred: '+JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        })
        .finally(() => {
        });
    }

    handleBookmarkClick(){
        if( this.recordId && this.recordId != '' ){
            toggleCourseBookmark({ recordId : this.recordId, bookmarkedState : this.bookmarked })
            .then(response => {
                this.bookmarked = !this.bookmarked;
                if( response.bookmarks.length > 0 ){
                    this.bookmarks = response.bookmarks;
                }
            })
            .catch(error => {
                console.log('error occurred: '+JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            })
            .finally(() => {
            });
        }
    }

    handleOnCourseClick(event){
        var selectedItemValue = event.detail.value;
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: this.pathPrefix+'/course-catalogue/'+selectedItemValue
            }
        });
    }
}