import { LightningElement, api, track } from 'lwc';
import getQuizResponses from '@salesforce/apex/LMS_Quiz_Controller.getQuizResponses'
import submitResponse from '@salesforce/apex/LMS_Quiz_Controller.submitResponse'
import validateQuiz from '@salesforce/apex/LMS_Quiz_Controller.validateQuiz'
import initiateQuiz from '@salesforce/apex/LMS_Quiz_Controller.initiateQuiz'
import getScore from '@salesforce/apex/LMS_Quiz_Controller.getScore'


export default class LMS_Quiz extends LightningElement {

  @api contentUserId
  @api userModuleId

  @api
  courseModuleId;
  @api moduleOnlineInstructions ;
  @track
  allQuizResponses = []
  currentQuestion;
  currentQuestionIndex = 0;
  loaded = false;
  @track
  initiatedQuiz = false;
  showError = false;
  @track
  userScore;
  @track
  quizScore;
  showQuizInstructions = false;
  showValidationError = false;
  submitError = false;

  connectedCallback(){
    console.log('quiz cmponent');
    if(this.courseModuleId){
      validateQuiz({courseModuleId:this.courseModuleId}).then((data)=>{
        console.log('Validate : ' + data);
        if(data){
          this.showQuizInstructions = true;
          initiateQuiz({courseModuleId: this.courseModuleId}).then(()=>{
            this.loaded = true;
          }).catch((e)=>{
            this.loaded = true;
          })
        }else{
          this.showError=true;
          this.loaded = true;
        }
      }).catch((e)=>{
        this.loaded = true;
      })
    }
  }
  reviewQuiz(){
    this.loaded = false;
    this.initiatedQuiz = false;
    getScore({courseModuleId:this.courseModuleId}).then((data)=>{
      this.userScore = data.UserScore;
      this.quizScore = data.QuizScore;
    })

    getQuizResponses({courseModuleId: this.courseModuleId,review:true}).then(
      (data)=>{
        this.loaded = true;
        this.showError = false;
        this.allQuizResponses =[];
        this.allQuizResponses = data;
        this.showReview = true;
      }
    ).catch(e=>{
      this.loaded = true;
    })
  }

  startQuiz(){
    this.initiatedQuiz = true;
    this.showQuizInstructions = false;
    this.loaded = false;
    getQuizResponses({courseModuleId: this.courseModuleId}).then(
      (data)=>{
        this.loaded = true;
        this.allQuizResponses = JSON.parse(JSON.stringify(data));
        this.currentQuestion = this.allQuizResponses[this.currentQuestionIndex];
      }
    )
  }

  @api
  submitResponse() {
    this.loaded = false;
    return new Promise((resolve,reject)=>{
      if( this.allQuizResponses.length !== 0 && this.allQuizResponses.every((q)=>{return q.listOfResponses.some((r)=>{return r.isSelected})})){
        this.submitError = false;
        submitResponse({ QuizResponse: this.allQuizResponses,courseModuleId: this.courseModuleId }).then(() => {
          console.log('submitted');
            this.loaded = true;
            this.reviewQuiz();
            resolve('Success');
        }).catch((e) => {
          this.loaded = true;
          reject('Error'+e);
        })
      }else{
        this.loaded = true
        this.submitError = true;
        reject('Error');
      }
    })
  }

  radioChangeHandler(event) {
    if (this.currentQuestion.isSingleChoice) {
      this.currentQuestion.listOfResponses.map((r) => {
        if (r.answerId === event.currentTarget.value) {
          r.isSelected = true;
        } else {
          r.isSelected = false;
        }
        return null;
      });
    } else {
      if (event.currentTarget.checked) {
        this.currentQuestion.listOfResponses.map((r) => {
          if (r.answerId === event.currentTarget.value) {
            r.isSelected = true;
          }
          return null;
        });
      } else {
        this.currentQuestion.listOfResponses.map((r) => {
          if (r.answerId === event.currentTarget.value) {
            r.isSelected = false;
          }
          return null;
        });
      }
    }
    if(this.currentQuestion.listOfResponses.some((r)=>{return r.isSelected})){
      this.currentQuestion.isAnswered = true;
    }else{
      this.currentQuestion.isAnswered = false;

    }

  }

  goNext() {
    this.showValidationError = false
    this.currentQuestion = this.allQuizResponses[this.currentQuestionIndex];
    if(this.currentQuestion.listOfResponses.some((r)=>{return r.isSelected})){
      this.showValidationError = false;
      this.currentQuestionIndex++;
      this.currentQuestion = this.allQuizResponses[this.currentQuestionIndex];
    }else{
      this.showValidationError = true;
    }
  }

  hideSubmitError(){
    this.submitError = false;
  }
  handleNavigate(event){
    this.currentQuestionIndex = parseInt(event.currentTarget.label,10) -1;
    this.currentQuestion = this.allQuizResponses[this.currentQuestionIndex];
  }

  goPrevious() {
    this.showValidationError = false;
    this.currentQuestionIndex--;
    this.currentQuestion = this.allQuizResponses[this.currentQuestionIndex];
  }
  get nextDisabled() {
    return this.currentQuestionIndex >= this.allQuizResponses.length - 1;
  }
  get previousDisabled() {
    return this.currentQuestionIndex <= 0;
  }
  get currentQuestionNoText() {
    return this.currentQuestionIndex + 1;
  }

  get totalQuestions(){
    return this.allQuizResponses.length;
  }
  closeModal(){
    this.dispatchEvent(new CustomEvent('cancel'));
  }
  completedQuiz(){

    this.submitResponse().then(()=>{
      this.dispatchEvent(new CustomEvent('complete',{detail:{contentUserId  : this.contentUserId, userModuleId : this.userModuleId}}));
    }).catch(()=>{})



  }

}